from enum import Enum

class ParameterName(Enum):
    HeartRate        = 'HeartRate'
    OxygenSaturation = 'OxygenSaturation'
    BodyTemperature  = 'BodyTemperature'
    RoomHumidity     = 'RoomHumidity'
    RoomTemperature  = 'RoomTemperature'
    RoomAirQuality   = 'RoomAirQuality'
