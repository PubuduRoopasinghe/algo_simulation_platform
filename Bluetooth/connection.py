# from enum  import Enum
#
# class BLE_STATE(Enum):
#     NO_SCAN_RESULTS     = 0x00 # BabyIO devices are not available with in range
#     NO_CHARGING_DEVICES = 0x01 # none of the detected devices are on the base station
#     BAND_DETECTED       = 0x02 # device setup sequence found new device and registered
#     BAND_UNAVAILABLE    = 0x03 # registered device is not available for connecting
#     BAND_CONNECTED      = 0x04
#     BAND_DISCONNECTED   = 0x05
#
#
# class Connection:
#
#     def __init__(self,state=BLE_STATE.BAND_UNAVAILABLE,mac=[]):
#         self.__device_id = mac
#         self.__state     = state
#
#     def is_connected(self):
#         return self.__state == BLE_STATE.BAND_CONNECTED
#
#     def get_state(self):
#         return self.__state
#
#     def get_band(self):
#         return self.__device_id
#
