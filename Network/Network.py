import json
import requests
from Utils.logs import Logs
from multiprocessing import Process,Queue
from Auditor.Messages.wanings import Warnings
from Configurations.basestation import BaseStation
from Auditor.Messages.vitalupdate import VitalUpdate
from Auditor.Messages.crynotification import CryNotification
from Auditor.Messages.rechargereminder import RechargeReminder
from Auditor.Messages.lowbatterywarning import LowBatteryWarning
from Auditor.Messages.roomconditionupdate import RoomConditionUpdate


class Network(Process):

    __HOST       = 'http://35.239.84.160:8080'
    # __LOCAL_HOST = 'http://10.10.50.65:8080'

    def __init__(self, queue_network):
        super(Network, self).__init__()
        self.__network_queue = queue_network
        self.__logs          = Logs('Network')
        self.__infant_id     = BaseStation.get_infant_id()

    def send_warning(self,content):
        try:
            url = '{0}/domain-data-server/api/warnings/vitalroom'.format(Network.__HOST)
            response = requests.post(url=url, data=content,
                                     headers={"content-type": "application/json"})
            self.__logs.w('{0}'.format(response.status_code),'{0}'.format(content))

        except Exception as ex:
            self.__logs.e('warning','{0}'.format(ex))

    def send_cry_alert(self,content):
        try:
            url = '{0}/domain-data-server/api/warnings/cryalert'.format(Network.__HOST)
            response = requests.post(url=url, data=content,
                                     headers={"content-type": "application/json"})

            self.__logs.w('{0}'.format(response.status_code),'Cry Detected')
        except Exception as ex:
            self.__logs.e('cry', '{0}'.format(ex))

    def send_battery_low_warning(self,content):
        try:
            url = '{0}/domain-data-server/api/warnings/battery'.format(Network.__HOST)
            response = requests.post(url=url, data=content,
                                     headers={"content-type": "application/json"})

            self.__logs.w('{0}'.format(response.status_code),'{0}'.format(content))
        except Exception as ex:
            self.__logs.e('battery', '{0}'.format(ex))

    def send_recharge_reminder(self,content):
        try:
            url = '{0}/domain-data-server/api/reminders/batterylow'.format(Network.__HOST)
            response = requests.post(url=url, data=content,
                                     headers={"content-type": "application/json"})
            self.__logs.w('{0}'.format(response.status_code),'{0}'.format(content))
        except Exception as ex:
            self.__logs.e('battery', '{0}'.format(ex))

    def run(self):

        while True:
            instance = self.__network_queue.get()

            if isinstance(instance,VitalUpdate):
                self.__logs.i('Vital', '{0}'.format(instance.get(self.__infant_id)))

            elif isinstance(instance, RoomConditionUpdate):
                self.__logs.i('Room', '{0}'.format(instance.get(self.__infant_id)))

            elif isinstance(instance,Warnings):
                warnings = []
                for warning in instance.get():
                    warnings.append(json.loads(warning.get(self.__infant_id)))
                    self.__logs.w('RoomVital',warning.get(self.__infant_id))

                message = json.dumps({"warningsList":warnings})
                self.send_warning(message)

            elif isinstance(instance,CryNotification):
                self.send_cry_alert(instance.get(self.__infant_id))

            elif isinstance(instance,LowBatteryWarning):
                self.__logs.w('Battery', '{0}'.format(instance.get(self.__infant_id)))
                self.send_battery_low_warning(instance.get(self.__infant_id))

            elif isinstance(instance,RechargeReminder):
                self.__logs.w('Reminder', '{0}'.format(instance.get(self.__infant_id)))
                self.send_recharge_reminder(instance.get(self.__infant_id))


