import numpy as np


class OutlierRemoval:

    def __init__(self,width=9):
        self.__width        = width
        self.__n_sigma      = 1.0
        self.__scale        = 1.4826
        self.__odd_indexed  = (self.__width % 2) == 1
        self.__pivot_point  = int(np.floor(self.__width / 2))
        self.__result       = 0.0
        self.__buffer       = []

    def process(self, value=0):
        signal = self.__update_buffer(value)  # array with original vital values

        if len(signal) == self.__width:  # checking whether there is a sufficient history
            pivot_value = signal[self.__pivot_point]  # the vital value which is tested for outliers

            # sorting the array of vital values
            sorted_values = np.sort(signal)

            # calculating the median of the sorted vital array
            if self.__odd_indexed:
                median = sorted_values[self.__pivot_point]  # window size is odd, consider only the pivot point
            else:
                median = (sorted_values[self.__pivot_point] +
                          sorted_values[self.__pivot_point - 1])/2  # window size is even, consider both the pivot point and the previous point

            # calculating the absolute deviations of the sorted vital array
            abs_deviations = []
            for value in sorted_values:
                abs_deviations.append(np.abs(median - value))

            # sort the array to find the mad
            sorted_abs_deviations = np.sort(abs_deviations)
            if self.__odd_indexed:
                mad = sorted_abs_deviations[self.__pivot_point]  # window size is odd, consider only the pivot point
            else:
                mad = (sorted_abs_deviations[self.__pivot_point] +
                       sorted_abs_deviations[self.__pivot_point - 1]) / 2  # window size is even, consider both the pivot point and the previous point

            # absolute deviation of the point being checked
            pivot_point_deviation = np.abs(pivot_value - median)

            # Hampel filter threshold above which the value is considered an outlier
            threshold = self.__n_sigma * (self.__scale * mad)
            # print ("threshold : ", threshold)

            # decide whether the value being checked is not an outlier
            if pivot_point_deviation <= threshold:
                return pivot_value  # not an outlier, return the value being checked
            else:
                return median  # outlier, hence return the median
        else:
            return value

    def __update_buffer(self, value):
        self.__buffer.append(value)
        if len(self.__buffer) > self.__width:
            del self.__buffer[0]

        return self.__buffer
