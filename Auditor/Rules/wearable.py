from enum import Enum
from Utils.util import Util
from Decode.sensor import Sensor
from Auditor.Messages.rechargereminder import RechargeReminder
from Auditor.Messages.lowbatterywarning import LowBatteryWarning


class STATE(Enum):
    DEFAULT  = 0x00
    CHARGING = 0x01
    ACTIVE   = 0x02
    IDLE     = 0x03

class Wearable:

    def __init__(self,criteria={}):

        if 'idle' in criteria:
            self.__idle_event = IdleEvent(criteria['idle'])
        else:
            self.__idle_event = None

        if 'active' in criteria:
            self.__active_event = ActiveEvent(criteria['active'])
        else:
            self.__active_event = None
        self.__warning = None
        self.__state   = STATE.DEFAULT

    def evaluate(self, sensor=Sensor()):
        if sensor.is_on_charger():
            if not self.__state  == STATE.CHARGING:
                self.__active_event.reset()
                self.__idle_event.reset()
                self.__state    = STATE.CHARGING
            return True
        else:
            if sensor.is_monitoring():
                if not self.__state == STATE.ACTIVE:
                    self.__active_event.reset()
                    self.__state = STATE.ACTIVE
                state,self.__warning = self.__active_event.evaluate(sensor.get_battery_capacity())
            else:
                if not self.__state == STATE.IDLE:
                    self.__idle_event.reset()
                    self.__state = STATE.IDLE

                state, self.__warning = self.__idle_event.evaluate(sensor.get_battery_capacity(),sensor.get_idle_time())
            return state

    def get(self):
        return self.__warning

    def clear_notification(self):
        self.__idle_event.reset()
        self.__active_event.reset()


class IdleEvent:

    __NEXT_DEFAULT_VALUE  = -1
    __DEFAULT_EVENT_TIME  = -1

    def __init__(self,rule={}):
        self.__events   = rule['events']
        self.__priority = rule['priority']
        self.__start    = rule['start']
        self.__next     = self.__calculate_next_event()

    def __calculate_next_event(self,value=100):
        for event in self.__events:
            if event < value:
                return event
        return IdleEvent.__NEXT_DEFAULT_VALUE

    def evaluate(self,value,idle_time):
        # print 'idle time: {0}, Threshold : {1} Value :{2}'.format(idle_time,self.__next,value)
        if idle_time >= self.__start and value <= self.__next:
            self.__next = self.__calculate_next_event(value)
            return False, RechargeReminder(value,(value*10),priority=self.__priority)
        return True, None

    def reset(self):
        self.__next = self.__calculate_next_event()

class ActiveEvent:

    __NEXT_DEFAULT_VALUE  = -1

    def __init__(self,rule={}):
        self.__events   = rule['events']
        self.__priority = rule['priority']
        self.__next_event = self.__calculate_next_event()

    def evaluate(self,value):
        if value <= self.__next_event:
            self.__next_event = self.__calculate_next_event(value)
            return False, LowBatteryWarning(percentage=value, discharge_time=value*10,priority=self.__priority)
        return True, None

    def __calculate_next_event(self,value=100):
        for event in self.__events:
            if event < value:
                return event
        return IdleEvent.__NEXT_DEFAULT_VALUE

    def reset(self):
        self.__next_event = self.__calculate_next_event()
