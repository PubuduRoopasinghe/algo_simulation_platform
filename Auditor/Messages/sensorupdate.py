import json
from Utils.util import Util
from Auditor.Messages.message import Message

class SensorUpdate:

    def __init__(self,):

        pass

    def get(self, infant='', device=''):
        return json.dumps({'detectedTime'     : self.__detected_time,
                           'heartRate'        : self.__heart_rate,
                           'oxygenSaturation' : self.__oxygen_level,
                           'bodyTemperature'  : self.__temperature,
                           'infantId'         : infant,
                           'deviceId'         : device})