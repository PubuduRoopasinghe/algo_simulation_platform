import json
import struct
import enum as enum
from Utils.util import Util
"""
    defining sensor modes
"""
class MODES(enum.Enum):
    NORMAL_OPERATION_MODE       = 0x00  # all the sensors are working if the prerequisites are satisfied
    SENSOR_CALIBRATION_MODE     = 0x01  # only the optical sensor is switched on and other sensors/modules are disabled
    ANY_MOTION_DETECTION_MODE   = 0x02  # all the sensors are shutdown and motion detection is enabled
    COMPLETE_SHUTDOWN_MODE      = 0x03  # all the sensors are shutdown along with proximity sensor


"""
    defining sensor __states
"""
class STATES(enum.Enum):
    DEFAULT_SENSOR_STATE    = 0x00  # device is powered up and sensors are in unknown __states
    SENSOR_INACTIVE_STATE   = 0x01  # all the sensors are disabled
    PROXIMITY_ON_STATE      = 0x02  # proximity sensor is switched on and waiting to enter operational mode upon the presence of a baby skin
    CALIBRATION_ON_STATE    = 0x03  # device is in calibration mode upon satisfaction of prerequisites to enter calibration mode
    MOTION_DETECTION_STATE  = 0x04  # device is in motion detection __states and not transmitting optical,motion or temperature data
    NORMAL_OPERATION_STATE  = 0x05  # device is in normal operation __states

"""
    Extracts information about the sensor __states 
    from the serial byte __stream 
"""
class Sensor:
    """
        defining constants
    """

    __HEADER_FORMAT = '<IBBHBBI'

    __ON_CHARGER_FLAG  = 0x01
    __CHARGING_ON_FLAG = 0x02

    """
        initialize the byte __stream and decode.
        @buf : byte __stream
    """
    def __init__(self, data=[]):
        self.__buff             = data
        self.__time             = 0
        self.__mode      = MODES.COMPLETE_SHUTDOWN_MODE
        self.__state     = STATES.DEFAULT_SENSOR_STATE
        self.__health   = 0x31
        self.__charger  = 0x00
        self.__capacity = 0x00
        self.__idle_period      = 0
        self.__flags            = 0
        self.__decode()

    """
        private method which decodes the given byte array
    """
    def __decode(self):

        if len(self.__buff) == 14:
            header = struct.unpack(self.__HEADER_FORMAT, self.__buff)
            self.__time   = header[0]
            self.__mode   = header[1]
            self.__state  = header[2]
            self.__health = header[3]
            self.__charger= header[4]
            self.__capacity    = header[5]
            self.__idle_period = header[6]
            # self.__flags       = header[7]

        elif len(self.__buff) == 15:
            # print len(self.__buff)
            HEADER_FORMAT = '<IBBHBBBBBBB'
            header = struct.unpack(HEADER_FORMAT, self.__buff)
            self.__time     = header[0]
            self.__mode     = header[1]
            self.__state    = header[2]
            self.__health   = header[3]
            self.__charger  = header[4]
            self.__capacity = header[5]
            self.__idle_period       = header[0]
        # else :
            # print len(self.__buff)
    """
        converts the decoded information to json object
        @:return : json object
    """
    def __get_json(self):
        return json.dumps({
            'detectedTime'      : self.__time,
            'deviceMode'        : self.__mode,
            'deviceState'       : self.__state,
            'deviceHealth'      : self.__health,
            'chargingState'     : self.__charger,
            'batteryPercentage' : self.__capacity,
        })

    def get_operational_mode(self):
        return self.__mode

    def get_operational_state(self):
        return self.__state

    def is_monitoring(self):
        return self.__state == STATES.NORMAL_OPERATION_STATE or \
               self.__state == STATES.MOTION_DETECTION_STATE or \
               self.__state == STATES.CALIBRATION_ON_STATE

    """
        overriding the string method to return json value 
    """
    def __str__(self):
        return str(self.__get_json())

    """
        return the timestamp
    """
    def get_time(self):
        return self.__time

    """
        return the current battery level
    """
    def get_battery_capacity(self):
        return self.__capacity

    """
        return whether the __device is on the charger or not
    """
    def is_on_charger(self):
        return (self.__charger & self.__ON_CHARGER_FLAG) == self.__ON_CHARGER_FLAG
    """
        return whether the __device is charging or not
    """
    def is_charging(self):
        return (self.__charger & self.__CHARGING_ON_FLAG) == self.__CHARGING_ON_FLAG

    def is_idle(self):
        return not self.is_on_charger() and not self.is_monitoring()

    def get_idle_time(self):
        return self.__idle_period

    def get_device_health(self):
        return self.__health
