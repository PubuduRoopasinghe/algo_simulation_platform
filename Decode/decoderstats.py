import time

from Decode.sensor import MODES
from Utils.util import Util

"""
    this class is responsible for validating __data captured over uart link
    validation is based on continuity and invalid UART commands
"""

class Losses:

    def __init__(self):
        self.__index    = -1
        self.__received = 0
        self.__losses   = 0
        self.__recovered= 0

    def update(self,index,is_complete):
        if self.__index == -1:
            self.__index    = index
            self.__received = 1
        else:
            if index > self.__index:

                delta = index - self.__index - 1 # calculate the gap
                if not is_complete:              # check whether latest one is complete
                    delta += 1

                self.__received += 1
                self.__losses   += delta
            self.__index = index

    def update_recovery(self,is_complete):
        if is_complete:
            self.__recovered += 1

    def get_count(self):
        return self.__received

    def get_losses(self):
        return self.__losses

    def get_recovered(self):
        self.__recovered

    def get_json(self):
        data = {
            'received': self.__received,
            'losses'  : self.__losses,
            'recovered': self.__recovered
        }
        return data

class DecoderStats:
    """"
        initialize the internal variables
    """""

    def __init__(self,log):
        self.__motion_losses  = Losses()
        self.__optical_losses = Losses()
        self.__temperature_losses = Losses()
        self.__operational_mode   = MODES.COMPLETE_SHUTDOWN_MODE
        self.__printTime    = 0
        self.__connections  = 0
        self.__log          = log
        self.__start        = Util.get_current_time()

    @staticmethod
    def write_to_output_stream(text):
        print(text)

    """
        print accumulated stats
    """

    def write_logs(self):
        seconds = Util.get_time() - self.__start;
        days = int(seconds / 86400)
        seconds %= 86400
        hours = int((seconds / 3600))
        seconds %= 3600
        minutes = int((seconds / 60))
        seconds %= 60

        self.__log.v('\n------------------------------------------------------------------------------------------------')
        self.__log.v('                                  BLE COMMUNICATION STATISTICS                                  ')
        self.__log.v('------------------------------------------------------------------------------------------------')
        self.__log.v(' Device Time              : {0}'.format(int(time.time())))
        self.__log.v(' Monitoring duration      : {0:02d}:{1:02d}:{2:02d}:{3:02d}'.format(days, hours, minutes, seconds))

        self.__log.v(' Number of Connections    : {0}'.format(self.__connections))
        self.__log.v(' Optical fifo count       : {0}'.format(self.__optical_losses.get_count()))
        self.__log.v(' Motion fifo count        : {0}'.format(self.__motion_losses.get_count()))

        self.__log.v(' Temperature count        : {0}'.format(self.__temperature_losses.get_count()))
        self.__log.v(' Optical data losses      : {0}   {1:.6f}%'.format(self.__optical_losses.get_losses(),
                                                                  self.__ratio(self.__optical_losses.get_losses(), self.__optical_losses.get_count())))
        self.__log.v(' Motion data losses       : {0}   {1:.6f}%'.format(self.__motion_losses.get_losses(),
                                                                  self.__ratio(self.__motion_losses.get_losses(), self.__motion_losses.get_count())))
        self.__log.v(' Temperature data losses  : {0}   {1:.6f}%'.format(self.__temperature_losses.get_losses(),
                                                                  self.__ratio(self.__temperature_losses.get_losses(),
                                                                               self.__temperature_losses.get_count())))
        self.__log.v('------------------------------------------------------------------------------------------------')

    """
        generate the json object of the validator
    """

    def get_json(self):
        data = {
            'duration'  : self.__motion_sent,
            'motion'    : self.__motion_losses.get_json(),
            'optical'   : self.__optical_losses.get_json(),
            'temperature':self.__temperature_losses.get_json()
        }
        return data

    """
        return the json string
    """

    def __str__(self):
        return str(self.get_json())

    def validate_motion(self, motion, backup=False):
        if self.__operational_mode == MODES.NORMAL_OPERATION_MODE:
            if not backup:
                self.__motion_losses.update(motion.get_index(), motion.is_complete())
            else:
                self.__motion_losses.update_recovery(motion.is_complete())

    def validate_optical(self, optical, backup=False):
        if self.__operational_mode == MODES.NORMAL_OPERATION_MODE:
            if not backup:
                self.__optical_losses.update(optical.get_index(), optical.is_complete())
            else:
                self.__optical_losses.update_recovery(optical.is_complete())

    def validate_temperature(self,temperature):
        if self.__operational_mode == MODES.NORMAL_OPERATION_MODE:
            self.__temperature_losses.update(temperature.get_index(),is_complete=True)

    def process_sensor_status(self, sensor):

        self.__operational_mode = sensor.get_operational_mode()
        if self.__operational_mode != MODES.NORMAL_OPERATION_MODE:
            self.__motion_losses = Losses()
            self.__optical_losses= Losses()
            self.__temperature_losses = Losses()

        if int(time.time()) - self.__printTime >= 60:
            self.__printTime = int(time.time())
            self.write_logs()

    @staticmethod
    def __ratio(numerator, denominator):
        if denominator == 0:
            return 0
        else:
            return numerator * 100.0/denominator

    def increment_connections(self):
        self.__connections += 1