# from Bluetooth.ble import BLE
from Decode.decodermotion import Decodermotion
from Vital.Vitals import Vitals
from Auditor.audit import Audit
from multiprocessing import Queue
from Decode.decoder import Decoder
from Manager.manager import Manager
# from Notification.Alerts import Alerts
# from Audio.Record.audiorecord import AudioRecord
from Sensors.AirQuality.airquality import AirQuality
# from Audio.PlayBack.audioplayback import AudioPlayback
# from Audio.Cry_Detection.crydetection import CryDetection
from Notification.notificationmanager import NotificationManager
from Network.Network import Network
import sys

# BLE,Decoder,AirQuality,Audit = [3.3%-5%, 10-12%]
# BLE,Decoder,Vital,AirQuality,Audit = [3.3%-5%, 10-16%]
# BLE,Decoder,Vital,AudioRecord,AirQuality,Audit = [3.3%-5%, 10-16%]

if __name__ == '__main__':


    queue_network = Queue()
    queue_vitals  = Queue()
    queue_info = Queue()
    queue_audit   = Queue()
    queue_command = Queue()
    queue_notify  = Queue()

    queue_audio_record = Queue()
    queue_audio_play   = Queue()
    manager = Manager()

    manager.add_process(Vitals,(queue_vitals, queue_command,4,queue_audit,queue_info ))
    manager.add_process(Decoder,(queue_vitals,queue_audit,queue_info))
    manager.add_process(Decodermotion,(queue_vitals,queue_audit,queue_info))
    manager.add_process(Audit, (queue_audit, queue_network, queue_notify))
    # manager.add_process(Network, (queue_network,))
    # manager.add_process(BLE, (queue_decoder, queue_command, queue_audit,))
    # manager.add_process(NotificationManager, (queue_notify, queue_audio_play))

    # if 'complete' in sys.argv:
    #     manager.add_process(AirQuality,(queue_audit,30))
    #     manager.add_process(AudioRecord, (queue_audio_record,))
    #     manager.add_process(CryDetection, (queue_audio_record,queue_audit, 8000))
    #     manager.add_process(AudioPlayback,(queue_audio_play,))

    manager.initialize()
    manager.monitor()
