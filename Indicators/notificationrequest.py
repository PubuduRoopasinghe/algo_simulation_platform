

class NotificationRequest:

	def __init__(self):
		self.__pattern	 = None
		self.__intensity = None
		self.__time 	 = None

	def get_pattern(self):
		return self.__pattern
	
	def get_intensity(self):
		return self.__intensity
	
	def get_time(self):
		return self.__time

	def set_pattern(self, pattern):
		self.__pattern = pattern
	
	def set_intensity(self, intensity):
		self.__intensity = intensity
	
	def set_time(self, time):
		self.__time = time
