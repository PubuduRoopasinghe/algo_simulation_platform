# import time
# from multiprocessing import Process
# import Queue as queue_except
# import sounddevice as sd
# from Utils.logs import Logs
#
#
# class AudioPlayback(Process):
#
# 	def __init__(self, queue, device = 0, block_size=2048):
#
# 		super(AudioPlayback, self).__init__()
# 		self.__block_size 	= block_size
# 		self.__sample_rate 	= 44100
# 		self.__channels 	= 1
# 		self.__queue 		= queue
# 		self.__device 		= device
# 		self.__logs 		= Logs('PlayBack')
#
# 	def run(self):
# 		try:
# 			stream = sd.OutputStream(samplerate=self.__sample_rate, device=self.__device, blocksize=self.__block_size,
# 									 channels=self.__channels, dtype='float32', callback=self.__callback)
# 			with stream:
# 				while True:
# 					time.sleep(10)
#
# 		except RuntimeError:
# 			self.__logs.e('Terminated','RuntimeError')
#
# 	def __callback(self, output_stream, frames, time_, status):
# 		try:
# 			sound_packet = self.__queue.get_nowait()
# 			data  = sound_packet.get_data()
# 		except queue_except.Empty:
# 			output_stream[:] = 0
# 			return
#
# 		if len(data) < len(output_stream):
# 			output_stream[:len(data)] = data
# 			output_stream[len(data):] = 0
# 		else:
# 			output_stream[:] = data
#
