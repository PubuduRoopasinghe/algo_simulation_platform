from Utils.logs import Logs
from Utils.util import Util
from Configurations.basestation import BaseStation
from Notification.notificationmanager import NotificationManager


class Manager:

    def __init__(self, monitor_period=1.0):
        self.__classes   = []
        self.__arguments = []
        self.__instances = []
        self.__states    = []
        self.__period    = monitor_period
        self.__log       = Logs('Manager')
        # BaseStation.set_infant_id('96a4f5f1-52e1-4c9c-a98f-dfa455a2ca2e')

    def add_process(self, process, parameters):
        self.__classes.append(process)
        self.__arguments.append(parameters)
        self.__states.append(False)
        self.__instances.append(process(*parameters))

    def initialize(self):
        # self.__log.i('System Time', '{0}'.format(Util.get_date_time()))

        for index in range(len(self.__instances)):
            if self.__classes[index] == NotificationManager:
                self.__instances[index].daemon = False
            else:
                self.__instances[index].daemon = True
            self.__instances[index].start()

    def monitor(self):
        while True:
            for index in range(len(self.__instances)):
                self.__states[index] = self.__instances[index].is_alive()

                if not self.__states[index]:
                    self.__instances[index] = self.__classes[index](*self.__arguments[index])
                    self.__instances[index].start()
                    self.__log.e('{0}'.format(self.__classes[index]), 'Invoked')

            Util.sleep(self.__period)
