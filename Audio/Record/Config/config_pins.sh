#! /bin/bash
# Stop on first error
set -e

## Setup pin P9.14 for PWM (CLK) output
##echo "PWM/CLK output (BeagleBone)"
config-pin -a P9.14 pwm
config-pin -q P9.14

PWM_PERIOD_NS='971'
PWM_DUTY_CYCLE='485'
cat "Audio/Record/Config/PWMsetup.sh" | sudo bash -s $PWM_PERIOD_NS $PWM_DUTY_CYCLE

## DATA input pin to PRU1
##echo "DATA in (PRU1)"
config-pin -a P8.28 pruin
config-pin -q P8.28
## CLK input pin to PRU1
##echo "CLK in (PRU1)"
config-pin -a P8.30 pruin
config-pin -q P8.30
