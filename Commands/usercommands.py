import struct
from enum  import Enum
from Utils.util import Util
from Decode.sensor import MODES

class Command(Enum):
    Command_version                   = 0x00
    Command_timestamp                 = 0x01
    Command_ble_configuration         = 0x10
    command_motion_configuration      = 0x11
    command_optical_configuration     = 0x12
    command_proximity_configuration   = 0x13
    command_temperature_configuration = 0x14
    command_indicator_configuration   = 0x15
    command_battery_configuration     = 0x16
    command_sensor_configuration      = 0x17
    command_write_request             = 0x80

class UserCommands:

    __CMD_WRITE_COMMAND = 0x80

    def __init__(self):
        pass

    @staticmethod
    def __send_request_command(command):
        return struct.pack('<Bibiiibb', command, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00)

    @staticmethod
    def request_firmware_version():
        return UserCommands.__send_request_command(Command.Command_version)

    @staticmethod
    def request_timestamp():
        return UserCommands.__send_request_command(Command.Command_timestamp)

    @staticmethod
    def request_ble_configurations():
        return UserCommands.__send_request_command(Command.Command_ble_configuration)

    @staticmethod
    def request_optical_configuration():
        return UserCommands.__send_request_command(Command.command_optical_configuration)

    @staticmethod
    def request_motion_configuration():
        return UserCommands.__send_request_command(Command.command_motion_configuration)

    @staticmethod
    def request_proximity_configuration():
        return UserCommands.__send_request_command(Command.command_proximity_configuration)

    @staticmethod
    def request_temperature_configuration():
        return UserCommands.__send_request_command(Command.command_temperature_configuration)

    @staticmethod
    def request_indicator_configuration():
        return UserCommands.__send_request_command(Command.command_indicator_configuration)

    @staticmethod
    def request_battery_configuration():
        return UserCommands.__send_request_command(Command.command_battery_configuration)

    @staticmethod
    def request_sensor_configuration():
        return UserCommands.__send_request_command(Command.command_sensor_configuration)

    @staticmethod
    def update_device_timestamp():
        return UserCommands.__trigger_mode(MODES.NORMAL_OPERATION_MODE)

    @staticmethod
    def write_ble_configuration():
        command = (Command.Command_ble_configuration | UserCommands.__CMD_WRITE_COMMAND)
        return struct.pack('<Bibiiibb', command, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00)

    @staticmethod
    def __trigger_mode(mode=MODES.NORMAL_OPERATION_MODE):
         return struct.pack('<Bibiiibb', (Command.Command_timestamp | UserCommands.__CMD_WRITE_COMMAND), Util.get_time(), mode, 0x00, 0x00, 0x00, 0x00, 0x00)

    @staticmethod
    def trigger_motion_detection_mode():
        return UserCommands.__trigger_mode(MODES.ANY_MOTION_DETECTION_MODE)

    @staticmethod
    def trigger_normal_operation_mode():
        return UserCommands.__trigger_mode(MODES.NORMAL_OPERATION_MODE)

    @staticmethod
    def trigger_calibration_mode():
        return UserCommands.__trigger_mode(MODES.SENSOR_CALIBRATION_MODE)

    @staticmethod
    def trigger_complete_shutdown_mode():
        return UserCommands.__trigger_mode(MODES.COMPLETE_SHUTDOWN_MODE)