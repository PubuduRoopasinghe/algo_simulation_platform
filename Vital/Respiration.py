import math
from Decode.optical import *
import time
from numpy import fft
import numpy as np

class RespiratoryRate:

    def __init__(self, peaks, valleys, ir_signal, sample_rate):
        self.__peaks                = peaks
        self.__valleys              = valleys
        self.__signal               = ir_signal
        self.__sample_rate          = sample_rate
        self.__resample_frequency   = 4.0  # re-sampling frequency after extracting features
        self.__window_size          = 16   # 16s window for FFT
        self.__shift                = 1    # 1s self.__shift of windows for FFT
        self.__hamming_coefficients = [0.54, 0.46]
        self.__PI                   = 3.141592
        self.__MIN_RESP_RATE        = 4.0
        self.__MAX_RESP_RATE        = 60.0
        self.__bpm                  = 0

    def process(self):
        print ("ir_signal in res : ",len(self.__signal))
        if self:
            if len(self.__signal)/Optical.SAMPLE_RATE >= 39:
                if len(self.__peaks) and len(self.__valleys):
                    start_time = time.time()
                    peak_v      = []
                    onset_v     = []
                    peak_index  = []
                    onset_index = []
                    peak_t      = []
                    onset_t     = []
                    no_of_peaks = len(self.__peaks)
                    no_of_onsets = len(self.__valleys)

                    for index in range(0, no_of_peaks):
                        peak_v.append(float(self.__signal[self.__peaks[index]]))
                        peak_index.append(self.__peaks[index])
                        peak_t.append(float(self.__peaks[index]) / self.__sample_rate)
                    for index in range(0, no_of_onsets):
                        onset_v.append(float(self.__signal[self.__valleys[index]]))
                        onset_index.append(self.__valleys[index])
                        onset_t.append(float(self.__valleys[index]) / self.__sample_rate)

                    peak_v      = peak_v[1:]      # array of peak values neglecting the first one
                    peak_index  = peak_index[1:]  # array of peak positions neglecting the first one
                    peak_t      = peak_t[1:]      # array of __time stamps of peaks
                    onset_v     = onset_v[:]      # array of onset values
                    onset_index = onset_index[:]  # array of onset positions
                    onset_t     = onset_t[:]      # array of __time stamps of onsets

                    # extracting amplitude, __intensity and __period of the pulses
                    feat_data1_v = []
                    feat_data1_t = []
                    feat_data2_v = []
                    feat_data2_t = []
                    feat_data3_v = []
                    feat_data3_t = []

                    # Find amplitude and __intensity
                    for i in range(0, len(peak_v)):
                        feat_data1_v.append(float(peak_v[i]   - onset_v[i]))     # amplitude trend __data (RIAV)
                        feat_data1_t.append(float((peak_t[i]  + onset_t[i])/2))  # __time stamps of amplitude trend __data
                        feat_data2_v.append(float((onset_v[i] + peak_v[i])/2))   # __intensity trend __data(RIIV)
                        feat_data2_t.append(float((peak_t[i]  + onset_t[i])/2))  # __time stamps of __intensity trend __data

                    # Normalise
                    feat_data1_v_mean = float(sum(feat_data1_v)/len(feat_data1_v))
                    feat_data2_v_mean = float(sum(feat_data2_v)/len(feat_data2_v))

                    for i in range(0, len(feat_data1_v)):
                        feat_data1_v[i] = float(feat_data1_v[i]/feat_data1_v_mean)
                        feat_data2_v[i] = float(feat_data2_v[i]/feat_data2_v_mean)

                    # Find __period
                    for i in range(0, len(peak_t) - 1):
                        feat_data3_v.append(float((peak_t[i+1] - peak_t[i])/self.__sample_rate))  # taco-gram (beat intervals) __data(RIFV)
                        feat_data3_t.append(float((peak_t[i+1] + peak_t[i])/2))                   # timestamps of taco-gram

                    # Normalise
                    feat_data3_v_mean = float(sum(feat_data3_v)/len(feat_data3_v))

                    for i in range(0, len(feat_data3_v)):
                        feat_data3_v[i] = float(feat_data3_v[i]/feat_data3_v_mean)

                    time_one = time.time()
                    """
                        Respiratory frequency estimations of windows for three different features using FFT and power spectrum
                        frequencies of respiratory induces amplitude variation for each window
                    """
                    RIAV_frequencies = self.__estimate_frequency(feat_data1_t, feat_data1_v)
                    time_two = time.time()

                    # frequencies of respiratory induced __intensity variation for each window
                    RIIV_frequencies = self.__estimate_frequency(feat_data2_t, feat_data2_v)
                    time_three = time.time()

                    # frequencies of respiratory induced frequency variation for each window
                    RIFV_frequencies = self.__estimate_frequency(feat_data3_t, feat_data3_v)
                    time_four = time.time()

                    self.__bpm = RespiratoryRate.__smart_fusion(RIAV_frequencies, RIIV_frequencies, RIFV_frequencies)
                    time_five = time.time()

                    # print("Time for initial calculations :",time_one-start_time)
                    # print("Time for RIAV                 :",time_two-time_one)
                    # print("Time for RIIV                 :",time_three-time_two)
                    # print("Time for RIFV                 :",time_four-time_three)
                    # print("Time for smart fusion         :",time_five-time_four)

                    # print(self.__bpm)

                else:
                    print("peaks & valleys are not detected")
            else:
                print("number of Vital samples are not enough for the frame")

    def __estimate_frequency(self, feat_data_t, feat_data_v):

        global __NFFT
        start_time = time.time()
        no_of_sampled_elements = int(math.floor((feat_data_t[-1]-feat_data_t[0])*self.__resample_frequency) + 1)
        no_of_elem             = int(len(feat_data_t))

        # extracted features are re-sampled to 4Hz
        sampled_feat_t = []
        sampled_feat_v = []
        rr_estimates   = []

        for index in range(0, no_of_sampled_elements):
            sampled_feat_t.append(float(feat_data_t[0] + (index/self.__resample_frequency)))
            for j in range(0, no_of_elem):
                if feat_data_t[j] > sampled_feat_t[index] > feat_data_t[j - 1]:  # later part of condition is added by Ayanga
                    x0 = float(feat_data_t[j-1])
                    x1 = float(feat_data_t[j])
                    y0 = float(feat_data_v[j-1])
                    y1 = float(feat_data_v[j])
                    sampled_feat_v.append(float(y0 + ((y1 - y0)/(x1 - x0))*(sampled_feat_t[index] - x0)))
                    break

        # data are divided into 16s sliding windows self.shifted by 1s for FFT
        window_samples = int(self.__window_size * self.__resample_frequency)  # no of samples per window

        samples_shift  = int(self.__shift * self.__resample_frequency)        # self.__shift of 4 samples

        no_of_wins     = int(1 + math.floor((len(sampled_feat_v)-window_samples)/samples_shift))
        win_no         = 0

        # print("No of iterations :", (len(sampled_feat_v) - window_samples+1)/samples_shift)

        for i in range(0, len(sampled_feat_v) - window_samples + 1, samples_shift):
            win_no = win_no + 1
            window_feat_v = sampled_feat_v[i:window_samples+i]

            # Now that we have a processed Vital-waveform, i.e. the 'respiratory __signal', lets find the FFT
            # number of points for FFT
            __window_length = len(window_feat_v)
            for index in range(0, __window_length):
                if pow(2, index) >= __window_length:
                    __NFFT = pow(2, index);
                    break

            __hamming_window = []
            for index in range(0, __window_length):
                __hamming_window.append(self.__hamming_coefficients[0] - self.__hamming_coefficients[1]*math.cos(2*self.__PI*index/(__window_length - 1))) #index/(__window_length - 1)

            FREQS = []  # Array of correspondent FFT bin frequencies, in BR (RPM)
            f_nyq = float(self.__resample_frequency / 2)

            for index in range(0, (__NFFT/2) + 1):
                FREQS.append(float((f_nyq*index*2)/__NFFT))

            win_avg = float(sum(window_feat_v)/len(window_feat_v))
            for index in range(0, window_samples):
                window_feat_v[index] = float((window_feat_v[index] - win_avg)*__hamming_window[index])

            # FFT calculation
            window_feat_v = np.array(window_feat_v)
            time_one = time.time()
            FFT_window_feat_v = fft.fft(window_feat_v)/__NFFT
            time_two = time.time()
            # print("Time for FFT inside loop:",time_two-time_one)
            imag_last_value = 0.0
            imag_last_value = np.imag(FFT_window_feat_v[-1])

            amp_spec = []
            power_spec = []
            for j in range (0,__NFFT/2+1):
                amp_spec.append(np.abs(FFT_window_feat_v[j]))
                power_spec.append((amp_spec[j]**2) * (1 / (self.__resample_frequency * __NFFT)))
                if 0 < j < __NFFT/2:
                    power_spec[j] = 2*power_spec[j]
                power_spec[j] = 10*math.log10(power_spec[j])

            freq_range = [self.__MIN_RESP_RATE / 60, self.__MAX_RESP_RATE / 60]  # expected respiratory frequency range

            cand_els = []
            for i in range (0,len(power_spec)):
                cand_els.append(0)

            for s in range(1, len(power_spec)-1):
                if power_spec[s] > power_spec[s - 1] and power_spec[s] > power_spec[s + 1] and freq_range[0] < FREQS[s] < freq_range[1]:
                    cand_els[s] = 1

            power_spec_min = min(power_spec)
            r_el = []

            for i in range (0,len(power_spec)):
                power_spec[i] = power_spec[i]-power_spec_min            # because the FFT and ACF have -ve values for the power spectra.
                r_el.append(float(power_spec[i]*cand_els[i]))

            for i in range(0,len(r_el)):
                if r_el[i] == max(r_el):
                    r_el_indx = i
                    break

            r_freq = FREQS[r_el_indx]
            rr_estimates.append(60 * r_freq)
            # print("rr_estimates length :", len(rr_estimates))
        finish_all_time=time.time()
        # print("Time for FFT calculation: ", finish_all_time-start_time)


        return rr_estimates

    @staticmethod
    def __smart_fusion(amplitude_frequency, intensity_frequency, periodic_frequency):
        # Cycle through times at which RR was estimated
        # Find out min length in case one has fewer estimates.
        startf_time = time.time()

        min_length  = min(len(amplitude_frequency), len(intensity_frequency), len(periodic_frequency))
        resp_values = []  # creating an array for fused RR values of each window

        for index in range(0, min_length):
            total = amplitude_frequency[index] + intensity_frequency[index] + periodic_frequency[index]
            avg   = total/3.0  # average of RIAV, RIIV, and RIFV estimations for a particular window
            std   = math.sqrt((((avg - amplitude_frequency[index]) * (avg - amplitude_frequency[index]))  +
                                ((avg - intensity_frequency[index]) * (avg - intensity_frequency[index])) +
                                ((avg - periodic_frequency[index] ) * (avg - periodic_frequency[index]))) / 2.0)  # standard deviation of RIAV, RIIV, and RIFV estimations for a particular window

            # if total > 3 and std < 4:            # windows with low RR estimation quality are eliminated from fusing
            resp_values.append(avg)          # calculating the mean of RIAV, RIIV, and RIFV RR estimations

        resp_rate = sum(resp_values)/len(resp_values)  # averaged estimated respiratory rate for the frame
        finishf_time = time.time()
        # print("Time for smart fusion: ", finishf_time - startf_time)
        return resp_rate

    def get_value(self):
        return self.__bpm
