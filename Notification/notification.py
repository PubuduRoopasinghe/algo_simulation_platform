class Notification:

    def __init__(self, notify=False, priority = 0):
        self.__notify   = notify
        self.__priority = priority

    def is_enabled(self):
        return self.__notify

    def get_priority(self):
        return self.__priority