# from Adafruit_I2C import Adafruit_I2C
#
#
# class LightController:
#
#     def __init__(self, i2c_address):
#
#         self.__REG_D1_PWM    = 0x16
#         self.__REG_CTRL_1    = 0x00
#         self.__REG_MISC      = 0x36
#         self.__REG_D1_I_CTRL = 0x26
#         self.__REG_CTRL_2    = 0x01
#         self.__REG_PROG_PAGE_SEL = 0x4f
#         self.__REG_PROG_MEM_BASE = 0x50
#         self.__REG_PROG1_START   = 0x4C
#         self.__REG_PC1           = 0x37
#         self.__REG_STATUS_IRQ    = 0x3A
#         self.__i2c = Adafruit_I2C(i2c_address, busnum=2)
#
#     def __read_register(self, reg_):
#         return self.__i2c.readU8(reg_)
#
#     def __write_register(self, reg_, value_):
#         self.__i2c.write8(reg_, value_)
#
#     def enable(self):
#         self.__write_register(self.__REG_CTRL_1, 0x40) #  Set enable bit
#         self.__write_register(self.__REG_MISC, 0x53)   # enable internal clock & charge pump & write auto increment
#
#     def set_channel_pwm(self, channel_, value_):
#         self.__write_register(self.__REG_D1_PWM + channel_, value_)
#
#     # value_ in mA
#     def set_drive_current(self, channel, value):
#         value = int(value * 10)
#         self.__write_register(self.__REG_D1_I_CTRL + channel, value)
#
#     def load_program(self, program):
#         length = len(program)
#
#         if length > 96:
#             return 0
#
#         self.__write_register(self.__REG_CTRL_2, 0x00) # disable drive engines
#         self.__write_register(self.__REG_CTRL_2, 0x15) # Enter Load mode
#
#         while self.__read_register(self.__REG_STATUS_IRQ) & 0x10: # wait for mode change
#             pass
#
#         for index in range(0, len(program)):
#
#             if index % 16 == 0:
#                 page_number = int(index / 16)
#                 self.__write_register(self.__REG_PROG_PAGE_SEL, page_number)
#
#             page_location = index % 16
#
#             self.__write_register(self.__REG_PROG_MEM_BASE + (page_location * 2), (program[index] >> 8) & 0xff)
#             self.__write_register(self.__REG_PROG_MEM_BASE + (page_location * 2) + 1, (program[index]) & 0xff)
#
#         self.__write_register(self.__REG_CTRL_2, 0x00) # disabled mode
#         print('LED program loaded.....')
#
#     def set_entry_point(self, engine, address):
#         self.__write_register(self.__REG_PROG1_START + engine, address)
#
#     def set_program_counter(self, engine, address):
#         control_val = self.__read_register(self.__REG_CTRL_1)
#         control2_val = self.__read_register(self.__REG_CTRL_2)
#
#         temp = (control_val & ~(0x30 >> (engine * 2)))
#         self.__write_register(self.__REG_CTRL_2, 0x3ff)         # halts the engine
#
#         self.__write_register(self.__REG_CTRL_1, temp)         # put engine in hold mode
#         self.__write_register(self.__REG_PC1 + engine, address)
#         self.__write_register(self.__REG_CTRL_1, control_val)
#         self.__write_register(self.__REG_CTRL_2, control2_val)
#
#     def set_engine_mode_free(self, engine):
#         val = self.__read_register(self.__REG_CTRL_1)
#         val &= ~(0x30 >> (engine * 2))
#         val |= (0x20 >> (engine * 2))
#
#         # print 'Free mode : ' + str(val)
#         self.__write_register(self.__REG_CTRL_1, val)
#
#     def set_engine_mode_once(self, engine):
#         val = self.__read_register(self.__REG_CTRL_1)
#         val |= (0x30 >> (engine * 2))
#
#         # print 'Once mode : ' + str(val)
#         self.__write_register(self.__REG_CTRL_1, val)
#
#     def set_engine_running(self, engine):
#         val = self.__read_register(self.__REG_CTRL_2)
#         val &= ~(0x30 >> (engine * 2))
#         val |= (0x20 >> (engine * 2))
#         self.__write_register(self.__REG_CTRL_2, val)
