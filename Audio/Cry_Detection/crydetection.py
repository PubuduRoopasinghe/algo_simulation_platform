# from multiprocessing import Process
# import pickle
# import numpy as np
# from python_speech_features import mfcc
# from math import ceil, log
# from Utils.logs import Logs
# from Audio.Cry_Detection.crynotice import CryNotice
# import Queue as queue_except
#
# class CryDetection(Process):
#
#     __PROCESSING_INTERVAL = 20 # 10
#
#     def __init__(self, queue_audio,queue_audit ,sample_rate=8000):
#         super(CryDetection, self).__init__()
#         self.__queue_audio  = queue_audio
#         self.__queue_audit  = queue_audit
#         self.__segment      = np.array([], dtype=np.float32)
#         self.__sample_rate  = sample_rate
#         self.__frame_width  = 0.5  #in seconds
#         self.__frame_shift  = 0.8*self.__frame_width
#         self.__fft_length   = int(2**ceil(log(self.__frame_width*self.__sample_rate, 2)))
#         self.__log          = Logs('CryDetection')
#
#     def run(self):
#
#         with open('Resources/Classifier/knn.pickle', 'rb') as file:
#             knn = pickle.load(file)
#
#         num_of_packets = 0
#         while True:
#             try:
#                 self.__segment = np.concatenate((self.__segment, self.__queue_audio.get()))
#                 num_of_packets += 1
#                 if num_of_packets == CryDetection.__PROCESSING_INTERVAL:
#
#                     num_of_packets = 0
#                     mel_fs = mfcc(self.__segment, self.__sample_rate, winlen=self.__frame_width,
#                                   winstep=self.__frame_shift, nfft=self.__fft_length,
#                                   highfreq=4000)
#
#                     result = knn.predict(mel_fs)
#                     probability = np.sum(result) / len(result)
#
#                     if probability > 0.4:
#                         self.__queue_audit.put(CryNotice(probability=probability,signal=self.__segment.tolist()))
#
#                     self.__segment = np.array([], dtype=np.float32)
#
#             except:
#                 self.__log.e('Exception','Run time error')
