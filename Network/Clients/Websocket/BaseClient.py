from tornado.ioloop import IOLoop, PeriodicCallback
from tornado import gen, httpclient
from tornado.websocket import websocket_connect
# import configparser
import Queue as queueException
from DatabaseHandler import DatabaseHandler
from Configurations.basestation import BaseStation


class BaseClient:
    def __init__(self, url_, queue_in_, database_name_=None, period_=2000):
        self.url = url_
        self.queue_in = queue_in_
        self.ws = None

        if database_name_ is not None:
            self.db_handler = DatabaseHandler(db_name_=database_name_)
            self.db_handler.connect()
        else:
            self.db_handler = None
        self.__database_empty = True

        self.infant_ID = self.get_infant_id()
        self.token = self.get_token()

        self.connect()
        PeriodicCallback(self.keep_alive, period_).start()

    @gen.coroutine
    def connect(self):
        try:
            print "NETWORK - Connecting - WebSocket : " + self.url, self.infant_ID
            httpRequest = httpclient.HTTPRequest(url=self.url, method='GET',
                                                 headers={'infantID': str(self.infant_ID), 'deviceID': str(0x00),
                                                          'Token': self.token,
                                                          "content-type": "application/json"})

            # self.ws = yield websocket_connect(url=httpRequest, on_message_callback=self.on_callback,
            #                                   connect_timeout=10000.0)
            self.ws = yield websocket_connect(url=httpRequest, on_message_callback=self.on_callback)

        except Exception, e:
            print "NETWORK - Connection Error - WebSocket : " + self.url, e
        else:
            print "NETWORK - Connected - WebSocket : " + self.url

    def keep_alive(self):
        try:
            data = self.queue_in.get_nowait()

        except queueException.Empty:
            # print "NETWORK - Network Queue Empty"
            return

        self.update_data(data)

    def update_data(self, data):
        cur_time, json_msg = self.decode_msg(data)

        if cur_time is None:
            return

        if self.ws is None:
            self.db_handler.insert_record([cur_time, json_msg])
            self.__database_empty = False
            self.connect()
            return

        try:
            # print json_msg
            self.ws.write_message(json_msg)
        except Exception as e:
            print "NETWORK - Write failed - Websocket : " + self.url, e
            self.ws = None
            self.db_handler.insert_record([cur_time, json_msg])
            self.__database_empty = False
            return

        if not self.__database_empty:
            db_records = self.db_handler.get_all_records()
            self.db_handler.delete_all_records()

            if not self.upload_old_data(db_records):
                return

            # self.db_handler.delete_all_records()
            self.__database_empty = True

    def on_callback(self, msg):
        pass

    def decode_msg(self, msg):
        # should return the parameters as an (array, json_str)
        # array = [time, parameters ......]

        return ''

    def upload_old_data(self, data):
        # should be replaced with the method being used to upload the data
        # can be either the websocket or the post request
        # need to return True on sucessfully uploading
        # else need to send the data back to the database using self.db_handler.insert_record()

        return True

    @staticmethod
    def get_infant_id():
        # config = configparser.ConfigParser()
        # config.read('Network/Config/config.ini')
        #
        # if config.has_section('Infant Data'):
        #     return config.get('Infant Data', 'ID')
        #
        # else:
        #     config.add_section('Infant Data')
        #     config.set('Infant Data', 'ID', str(0))
        #
        #     with open('Network/Config/config.ini', 'w+') as configFile:
        #         config.write(configFile)
        #
        #     return 0
        return BaseStation.get_infant_id()
        # return 'd55cc8f5-1bf1-49d7-b83b-8c10d2f84fd3'

    def get_token(self):
        # expired = False
        #
        # if expired:
        #     # get new token some how
        #     return self.token
        # else:
        #     return self.token

        return 'eyJhbGciOiJSUzI1NiIsImtpZCI6IjI2OGNhNTBjZTY0YjQxYWIzNGZhMDM1NzIwMmQ5ZTk0ZTcyYmQ2ZWMiLCJ0eXAiOiJKV1QifQ.eyJpc3MiOiJodHRwczovL3NlY3VyZXRva2VuLmdvb2dsZS5jb20vYmFieW1vbml0b3ItZmx1dHRlciIsImF1ZCI6ImJhYnltb25pdG9yLWZsdXR0ZXIiLCJhdXRoX3RpbWUiOjE1NjY1MzY0NDQsInVzZXJfaWQiOiIxdUIxaXFVVkFXUVJITUFNYnNxYVFDcDV4S1EyIiwic3ViIjoiMXVCMWlxVVZBV1FSSE1BTWJzcWFRQ3A1eEtRMiIsImlhdCI6MTU2NjUzNjQ0NywiZXhwIjoxNTY2NTQwMDQ3LCJlbWFpbCI6InBycmFtaXlhOTQ4MUBnbWFpbC5jb20iLCJlbWFpbF92ZXJpZmllZCI6ZmFsc2UsInBob25lX251bWJlciI6Iis5NDc3MTEyMTc4NyIsImZpcmViYXNlIjp7ImlkZW50aXRpZXMiOnsicGhvbmUiOlsiKzk0NzcxMTIxNzg3Il0sImVtYWlsIjpbInBycmFtaXlhOTQ4MUBnbWFpbC5jb20iXX0sInNpZ25faW5fcHJvdmlkZXIiOiJwYXNzd29yZCJ9fQ.K_LeV8a6F6iWbTcpyHoREsv4PHC1zNhg4sxRzQJVJly9lM_xWC5YNGXwpNL5lEgB0NyluD7hRuqFAIq8v2NwpE_cJ_aLYL5CRsEAHEj-QuqOmudOXYlSU3ERHH0M0qymz3EVMhO8raoYOyUhKVHs3wdnzku1dGqjZHAnYhpTRdyg5OCoLqr_uFnk17uGWaNSYwejS5aOTsv3wfXJGtPW_jlQOrusHkC2zW6wAPXfb0L4FyHqpI963Upqz4iwP3ggTuP_7FTYbCsLGBo4_J4f_MSh-zq8wRGINEXAZVjSB1SNQ76IHLoGiUqJMuzPtFMDJEsgm8qVEq-N-YU3bmEOpw'
