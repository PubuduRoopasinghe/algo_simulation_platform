"""
 THIS CLASS IS DESIGNED FOR HEART RATE CALCULATION. FOR HEART RATE COMPUTATION USE THIS CLASS AND CONSTRUCT AN OBJECT.
 THE CLASS TAKES THE ARRAY OF CONSECUTIVE PEAKS POSITION AS INPUT ARGUMENT, TOGETHER WITH SAMPLING FREQUENCY.
 ANOTHER CLASS METHOD IS DEFINED TO BE USED WHEN THE SAMPLING FREQUENCY IS 25 Hz.
"""
from Utils.logs import Logs
from Vital.signalquality import SignalQuality


class HeartRate:
    """
        Standard initializer method for an object of this class. Input argument must be an array, and sampling frequency in Hz.
        once the object is initialized HearRate will be calculated and if the object is invalid, Hear Rate is set to 0
    """
    __SAMPLING_RATE       = 25
    __SECONDS_PER_MINUTE  = 60
    __SAMPLES_PER_MINUTES = __SAMPLING_RATE*__SECONDS_PER_MINUTE
    __bad_ppg_quality_time_stamp = []
    __number_of_abnomalities = 0

    def __init__(self, valleys, bad_quality_ppd_start_time):
        self.__valley_position_timestamps = valleys
        self.__heart_rate    = 0
        self.__bad_ppg_quality_time_stamp.append(bad_quality_ppd_start_time)

    """
        nonzero method is used identify a valid object of this class,
        This is done in order to avoid computational errors.This Heart 
        Rate algorithm at least require 2 consecutive peaks for successful
        computation.
    """
    def __nonzero__(self):
        return len(self.__valley_position_timestamps) >= 2

    """
        This __compute_HeartRate  is a hidden method and will be called by the object constructor.
        compute the Heart_rate as a floating point value in Beats per Minute
    """
    def process(self):
        # signal_quality = SignalQuality()
        # bad_ppg_quality_time = signal_quality.get_bad_ppg_quality_time_stamp()
        # print ("in HR : ", bad_ppg_quality_time)
        # if len(self.__valley_position) >= 2:
        #     first_peak = self.__valley_position[0]
        #     last_peak  = self.__valley_position[-1]
        #
        #     if last_peak > first_peak:
        #         mean_peak_interval = float(last_peak - first_peak) / (len(self.__valley_position) - 1)
        #         self.__heart_rate  = self.__SAMPLES_PER_MINUTES/mean_peak_interval
        #     else:
        #         self.__heart_rate = 1
        # print self.__valley_position_timestamps
        if len(self.__valley_position_timestamps) >= 2:
            time_loss = self.get_abnormal_time_gaps(self.__valley_position_timestamps)
            # print "time_loss : ", time_loss, self.__number_of_abnomalities
            number_of_valley_intervals = len(self.__valley_position_timestamps) - 1 - self.__number_of_abnomalities
            time_difference = (self.__valley_position_timestamps[len(self.__valley_position_timestamps)-1] - self.__valley_position_timestamps[0]) - (time_loss)

            HR = number_of_valley_intervals/time_difference
            # print (number_of_valley_intervals, time_difference)
            if HR > 0:
                self.__heart_rate = HR * 60
            else:
                self.__heart_rate = -1

    def get_abnormal_time_gaps(self, valley_time_stamps_array):
        if not len(self.__bad_ppg_quality_time_stamp) == 0:
            for index in range(0, len(self.__bad_ppg_quality_time_stamp)):
                if not (valley_time_stamps_array[0] <= self.__bad_ppg_quality_time_stamp[index] <= valley_time_stamps_array[len(valley_time_stamps_array)-1]):
                    self.__bad_ppg_quality_time_stamp.remove(self.__bad_ppg_quality_time_stamp[index])
        starting_time_stamps_Of_abnormal_gaps = []
        if len(self.__bad_ppg_quality_time_stamp) == 0:
            self.__number_of_abnomalities = 0
            return 0
        else:
            for element in self.__bad_ppg_quality_time_stamp:
                time_gaps = []
                for time_stamps in valley_time_stamps_array:
                    if element - time_stamps > 0 :
                        time_gaps.append(element - time_stamps)

                if len(time_gaps) == 0:
                    starting_time_stamps_Of_abnormal_gaps.append(valley_time_stamps_array[len(time_gaps)-1])

            self.__number_of_abnomalities = len(starting_time_stamps_Of_abnormal_gaps)
            time_loss = 0
            if not len(starting_time_stamps_Of_abnormal_gaps) == 0:
                for element in starting_time_stamps_Of_abnormal_gaps:
                    time_loss += valley_time_stamps_array[valley_time_stamps_array.index(element) + 1] - valley_time_stamps_array[valley_time_stamps_array.index(element)]
            return time_loss

    """
        This is the method used to the access the computed heart rate of the object.
        Only this method can be accessed by an object of this class to access the heart rate.
    """
    def get_heart_rate(self):
        return self.__heart_rate

    def __repr__(self):
        return "HeartRate(peaks, __sample_rate)"
