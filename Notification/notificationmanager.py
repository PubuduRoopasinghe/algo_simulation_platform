from Utils.logs import Logs
from multiprocessing import Process, Queue
from Notification.notification import Notification
from Audio.PlayBack.alertmanager import AlertManager as AudioAlertManager
from Audio.PlayBack.notificationrequest import NotificationRequest as AudioMsg
from Indicators.alertmanager import AlertManager as LEDAlertManager
from Indicators.notificationrequest import NotificationRequest as LedMsg

class NotificationManager(Process):

    def __init__(self, queue_notification, queue_audio_play):
        super(NotificationManager, self).__init__()
        self.__queue_notification = queue_notification
        self.__queue_audio        = queue_audio_play
        self.__queue_notification_audio = Queue()
        self.__queue_notification_led   = Queue()
        self.__logs                     = Logs('Notifications')

    def run(self):
        # audio_notification_manager = AudioAlertManager(queue_in=self.__queue_notification_audio,
        #                                          queue_out=self.__queue_audio, period=0.5, block_size=2048)
        # _led_alert_manager = LEDAlertManager(queue_=self.__queue_notification_led, period=0.5)

        # audio_notification_manager.start()
        # _led_alert_manager.start()

        while True:
            instance = self.__queue_notification.get()

            if isinstance(instance,Notification):
                # led_msg = LedMsg()
                self.__logs.i('{0}'.format(instance.is_enabled()),'{0}'.format(instance.get_priority()))
                # if instance.is_enabled():
                #     self.__logs.i('')
                #     # led_msg.set_pattern(_notification.get_led_pattern())
                #     # led_msg.set_time(10)
                #
                #     # self.__queue_notification_led.put(led_msg)
                #
                #     self.__queue_notification_audio.put(AudioMsg(priority=instance.get_priority()))
                #
                # else:
                #     # led_msg.set_pattern('nopattern')
                #     # led_msg.set_time(10)
                #
                #     # self.__queue_notification_led.put(led_msg)
                #     self.__queue_notification_audio.put(AudioMsg(priority=0,clear=True))


