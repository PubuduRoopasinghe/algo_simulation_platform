import math


class NoiseFilter:

    @staticmethod
    def apply_moving_average(signal=[], width=5):
        length = len(signal)
        if length > width:
            total  = 0
            output = []
            for i in range(width):
                total += signal[i]
            output.append(int(-1*(math.ceil(total/float(width)))))

            for i in range(width, length):
                total += (signal[i] - signal[i - width])
                output.append(int(-1*(math.ceil(total/float(width)))))
            return output