import re

from Utils.util import Util


class Readmotion:
    __MULTIPLIER_ACC = 8/(1 << 16)
    __MULTIPLIER_GYR = 4000/(1 << 16)
    __SAMPLE_RATE = 25
    __period = 0
    __delta  = 0
    __MOTION_SCHEME = "\\d{10}:\\d+:\\d+:(\\[(-?\\d+, )+(-?\\d+)\\]:){5}(\\[(-?\\d+, )+(-?\\d+)\\])"

    def __init__(self, record):
        self.__text  = record
        self.__timestamp = None
        self.__time_epoch  = None
        self.__original_epoch_time = None
        self.__original_timestamp = None
        self.__index = None
        self.__acc_x = None
        self.__acc_y = None
        self.__acc_z = None
        self.__gyr_x = None
        self.__gyr_y = None
        self.__gyr_z = None

    def is_valid(self):
        return self.__process()

    def __process(self):
        if re.search(Readmotion.__MOTION_SCHEME, self.__text):
            data         = self.__text.split(":")
            self.__original_timestamp = int(data[0])
            self.__original_epoch_time  = int(data[0]) + int(data[1]) / 1000.0
            self.__index = int(data[2])
            self.__acc_x = self.__extract_values_from_array(data[3], Readmotion.__MULTIPLIER_ACC)
            self.__acc_y = self.__extract_values_from_array(data[4], Readmotion.__MULTIPLIER_ACC)
            self.__acc_z = self.__extract_values_from_array(data[5], Readmotion.__MULTIPLIER_ACC)
            self.__gyr_x = self.__extract_values_from_array(data[6], Readmotion.__MULTIPLIER_GYR)
            self.__gyr_y = self.__extract_values_from_array(data[7], Readmotion.__MULTIPLIER_GYR)
            self.__gyr_z = self.__extract_values_from_array(data[8], Readmotion.__MULTIPLIER_GYR)
            return True
        return False

    @staticmethod
    def __extract_values_from_array(axis="[]", multiplier=1):
        values = axis.replace('[', '').replace(']', '').replace('\n', '').split(',')
        output = []
        for value in values:
            output.append(int(value)*multiplier)
        return output

    def is_complete(self):
        return True

    def get_original_timestamp(self):
        return self.__original_timestamp

    def get_original_epoch_time(self):
        return self.__original_epoch_time

    def get_time(self):
        return int(self.__time_epoch)

    def get_index(self):
        return self.__index

    def set_epoch(self, epoch):
        self.__time_epoch = epoch

    def get_epoch(self):
        return self.__time_epoch

    def get_acc_x(self):
        return self.__acc_x

    def get_acc_y(self):
        return self.__acc_y

    def get_acc_z(self):
        return self.__acc_z

    @staticmethod
    def set_sample_period(period):
        Readmotion.__period = period
        Readmotion.__delta  = period / Readmotion.__SAMPLE_RATE

    @staticmethod
    def get_sample_period():
        return Readmotion.__period

    def in_range(self, first, last):
        return last >= self.__time_epoch >= first or last >= self.__time_epoch + self.__period >= first \
               or last >= self.__time_epoch + self.__period / 2 >= first

    def __str__(self):
        return 'Timestamp : {0:.3f} and Index : {1}'.format(self.__time_epoch, self.__index)

    def get_sample_index_at(self, time):
        offset = time - self.__time_epoch

        if 0 <= offset <= Readmotion.__period:
            index = int(offset / Readmotion.__delta)
            error = offset - index * Readmotion.__delta
            if error >= (Readmotion.__delta / 2):
                index += 1
                if index < Readmotion.__SAMPLE_RATE:
                    return index
                return -1
            return index
        return -1

    def get_sample_at(self, index, with_time=False):
        if with_time:
            return '{0:d},{1:.4f},{2:.4f},{3:.4f},{4:.4f},{5:.4f},{6:.4f}'.format(self.get_time_for(index),
                                                                                    self.__acc_x[index],
                                                                                    self.__acc_y[index],
                                                                                    self.__acc_z[index],
                                                                                    self.__gyr_x[index],
                                                                                    self.__gyr_y[index],
                                                                                    self.__gyr_z[index])
        else:
            return '{0:.4f},{1:.4f},{2:.4f},{3:.4f},{4:.4f},{5:.4f}'.format(
                                                               self.__acc_x[index],
                                                               self.__acc_y[index],
                                                               self.__acc_z[index],
                                                               self.__gyr_x[index],
                                                               self.__gyr_y[index],
                                                               self.__gyr_z[index])

    def get_time_for(self, index):
        return self.__time_epoch + index * Readmotion.__delta
