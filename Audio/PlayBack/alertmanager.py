import threading
import time
from multiprocessing import Process, Queue
import numpy as np
# import soundfile as sf
from Audio.PlayBack.soundpacket import SoundPacket
from Utils.logs import Logs
from Audio.PlayBack.notificationrequest import NotificationRequest as AudioRequest

class AlertManager(Process):

    __Alert_Clips = ['Resources/Clips/noise.wav',
                     'Resources/Clips/noise.wav',
                     'Resources/Clips/noise.wav']

    def __init__(self, queue_in, queue_out, period=0.5, block_size=2048):
        super(AlertManager, self).__init__()
        self.__queue_out = queue_out
        self.__queue_in  = queue_in
        self.__period    = period
        self.__block_size= block_size
        self.__logs      = Logs('AudioAlert')
        self.__queue_notifications = Queue()
        self.__clear = False

    def run(self):
        thread = threading.Thread(target=self.__thread_handler)
        # thread.daemon = True
        thread.start()

        while True:
            instance = self.__queue_in.get_warning()
            if isinstance(instance,AudioRequest):
                if instance.is_cleared():
                    self.__clear = True
                    continue
                else:
                    self.__clear = False
                self.__queue_notifications.put(instance)



    def __thread_handler(self):

        while True:
            instance = self.__queue_notifications.get_warning()

            if isinstance(instance,AudioRequest):

                if instance.get_priority():
                    audio_clip = AlertManager.__Alert_Clips[instance.get_priority()]

                # with sf.SoundFile(audio_clip) as file:
                #
                #     data = file.buffer_read(self.__block_size, 'float')
                #     data = np.frombuffer(data, dtype=np.float32)
                #     data = data.reshape((len(data), 1))
                #
                #     self.__queue_out.put(SoundPacket(buffer=data))
                #
                #     while len(data) > 0 and not self.__clear:
                #
                #         data = file.buffer_read(self.__block_size, 'float')
                #         data = np.frombuffer(data, dtype=np.float32)
                #         data = data.reshape((len(data), 1))
                #
                #         self.__queue_out.put(SoundPacket(buffer=data))
                #         time.sleep(float(self.__block_size) / float(file.samplerate)*0.95)
