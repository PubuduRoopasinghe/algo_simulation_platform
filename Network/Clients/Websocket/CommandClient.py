from BaseClient import BaseClient


class CommandClient(BaseClient):
    def decode_msg(self, msg):
        pass

    def keep_alive(self):
        if self.ws is None:
            self.connect()
            return

        try:
            self.ws.write_message('BaseStation Alive')
        except Exception as e:
            self.ws = None

    def upload_old_data(self, data):
        return True

    def on_callback(self, msg):
        print 'Network - CommandClient - Command Msg received', msg
        pass





