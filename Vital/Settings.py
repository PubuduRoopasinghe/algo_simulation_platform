import numpy as np
from Wearable.max30102 import MAX30102


class Settings:

    def __init__(self):

        self.ppg_range = 0x00
        self.pulseWidth = 0x03
        self.sampleRate = 0 << 2
        self.current = 0x26
        self.numberOfAcceptedValues = 0
        self.updated = bool(False)
        self.stopAdjustments = bool(False)
        self.stopAdjustDueToInability = bool(False)
        self.rangeUpdated = bool(False)

    def is_stop_adjustments(self):
        return self.stopAdjustments

    def set_stop_adjustments(self, stop_adjustment):
        self.stopAdjustments = stop_adjustment

    def is_stop_adjust_due_to_inability(self):
        return self.stopAdjustDueToInability

    def set_stop_adjust_due_to_inability(self, stop_adjust_due_to_inability):
        self.stopAdjustDueToInability = stop_adjust_due_to_inability

    def get_pulse_width(self):
        return self.pulseWidth

    def set_pulse_width(self, pulse_width):
        self.pulseWidth = pulse_width

    def get_range(self):
        return self.ppg_range

    def set_range(self, new_range):
        if self.ppg_range != new_range:
            self.rangeUpdated = True
            self.ppg_range = new_range

    def reset_range_updated_flag(self):
        self.rangeUpdated = False

    def is_range_updated(self):
        return self.rangeUpdated

    def get_sample_rate(self):
        return self.sampleRate

    def set_sample_rate(self, sample_rate):
        self.sampleRate = sample_rate

    def mark_as_updated(self, new_updated):
        self.updated = new_updated

    def is_updated(self):
        return self.updated

    def set_current(self, new_current):
        self.current = new_current

    def get_current(self):
        return self.current

    def get_number_of_accepted_values(self):
        return self.numberOfAcceptedValues

    def set_number_of_accepted_values(self, number_of_accepted_values):
        self.numberOfAcceptedValues = number_of_accepted_values

    def equals_calibration(self, settings):
        eq = bool(settings.get_pulse_width() == self.get_pulse_width() or settings.get_sample_rate() == self.get_sample_rate() or settings.get_normal_range() == self.get_range())
        return eq

    def get_upper_range(self):
        optical_config = MAX30102.getInstance()
        ranges = optical_config.get_valid_ppg_ranges()
        for index in range(0, len(ranges)-1, 1):
            if ranges[index] == self.get_range():
                return ranges[index + 1]

        return self.get_range()

    def get_lower_range(self):
        optical_config = MAX30102.getInstance()
        ranges = optical_config.get_valid_ppg_ranges()
        for index in range(1, len(ranges), 1):
            if ranges[index] == self.get_range():
                return ranges[index - 1]

        return self.get_range()



