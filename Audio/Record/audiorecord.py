# import pypruss
# import numpy as np
# import mmap
# from multiprocessing import Process
# import sys
# from scipy.signal import firwin2, lfilter, resample
# import os
# import Queue as queue_except
#
#
# class AudioRecord(Process):
#
#     def __init__(self, queue):
#         super(AudioRecord, self).__init__()
#         self.__exit_audio_record = False
#         self.__frame_size = 32768
#         self.__queue      = queue
#
#     @staticmethod
#     def __configure_pins():
#         os.system('sudo sh Audio/Record/Config/config_pins.sh')
#
#     @staticmethod
#     def __map_memory():
#         memory_address = pypruss.ddr_addr()
#         memory_length  = pypruss.ddr_size()
#         host_memory    = pypruss.map_extmem()
#
#         data = np.array([memory_address, memory_length], dtype=np.uint32)
#         pypruss.pru_write_memory(1, 0, data)
#         return [memory_address, memory_length, host_memory]
#
#     def run(self):
#         self.__configure_pins()
#
#         pypruss.init()
#         pypruss.open(1)
#
#         pypruss.pruintc_init()
#         address = self.__map_memory()
#
#         pypruss.exec_program(1, 'Audio/Record/Config/cic_pru1.bin')
#
#         with open("/dev/mem", "r+b") as f:
#             ddr_mem = mmap.mmap(f.fileno(), address[1], offset=address[0])
#
#         first_half = True
#
#         h = self.__get_compensation_filter()
#
#         last_frame = np.zeros(self.__frame_size, dtype=np.float32)
#
#         signal_mean     = 18330.0  # previously calculated value
#         signal_factor   = 1000.0
#         padding_window_size = 1000
#
#         while True:
#             pypruss.wait_for_event(1)
#             pypruss.clear_event(1, pypruss.PRU1_ARM_INTERRUPT)
#
#             if first_half:
#                 raw_u32 = np.frombuffer(ddr_mem[:address[1] / 2], dtype=np.uint32)
#                 first_half = False
#             else:
#                 raw_u32 = np.frombuffer(ddr_mem[address[1] / 2:], dtype=np.uint32)
#                 first_half = True
#
#             frame = np.copy(raw_u32)
#             frame = frame.astype(np.float32)
#
#             con_frame = np.concatenate((last_frame[-padding_window_size:], frame))
#             last_frame[:] = frame
#
#             filtered_frame = lfilter(h, 1, con_frame)
#
#             filtered_frame = filtered_frame[padding_window_size:]
#             filtered_frame = filtered_frame - np.mean(filtered_frame)
#             filtered_frame = filtered_frame/signal_factor
#
#             filtered_frame = resample(filtered_frame, self.__frame_size/8)
#
#             try:
#                 self.__queue.put_nowait(filtered_frame)
#             except queue_except.Full:
#                 pass
#
#         ddr_mem.close()
#
#     @staticmethod
#     def __get_compensation_filter():
#         eps = sys.float_info.epsilon
#
#         N = 4
#         M = 1
#         R = 16
#
#         Fs = 64000
#         f = np.linspace(0, 1.0, Fs/2)
#
#         hf = (np.sin(np.pi * M * f) / (np.sin(np.pi * f / R) + eps)) ** (2 * N)
#         hf = hf[1:]
#         hf_db = 10 * np.log10(np.abs(hf))
#         hf_db = hf_db - np.max(hf_db)
#
#         hf = 10 ** (hf_db / 10.0)
#         gf = 1.0 / hf
#         Fc = 8000
#
#         L_fc = int(Fc/2.0)
#         gf[L_fc:] = 0
#
#         gf = np.concatenate((np.array([0]), gf))
#
#         n = 20
#         h = firwin2(n, f, gf)
#
#         return h