from Utils.logs import Logs
from Decode.sensor import Sensor
from Decode.motion import Motion
from Decode.optical import Optical
from Decode.response import Response
from multiprocessing import Process,Queue
from Decode.temperature import Temperature
from Decode.decoderstats import DecoderStats
# from Decode.Analyse import Analyse
from Decode.Readoptical import Readoptical
from Decode.Readmotion import Readmotion
import time

from Utils.util import Util


class Decodermotion(Process):

    __FRAME_BYTE_INDEX = 1
    __RESPONSE_handle = 26
    __ACC_0_handle    = 34
    __ACC_1_handle    = 38
    __SPO_0_handle    = 42
    __SPO_1_handle    = 46
    __TEMP_handle     = 50
    __SENSOR_handle   = 54
    __VITAL_PERIOD    = 2

    def __init__(self, queue_vitals, queue_audit, queue_info):
        super(Decodermotion, self).__init__()
        # self.__queue_decoder = queue_decoder
        self.__queue_vitals  = queue_vitals
        self.__queue_audit   = queue_audit
        self.__queue_info = queue_info

        self.__motion_0   = Motion()
        self.__motion_1   = Motion()
        self.__optical_0  = Optical()
        self.__optical_1  = Optical()
        self.__log        = Logs('Decoder')
        self.__decoder_stats = DecoderStats(self.__log)
        self.__response      = Response(self.__log)

        file_paths = ['F:\\Synergen_Work\\Work 7_Simulation platform\\Collected_Data\\HR_Test_Data_Rasa']
        for path in file_paths:
            print('------------------------------------------------------------------------------------')
            print('Directory Path : {0}\n'.format(path))
            # self.__optical_signal = Analyse(path)
            self.__motion_signal = self.__read_file_motion(path + '\\Motion Data.csv')
        self.__motion_start_time = Util.exact_time()
        print self.__motion_signal
        print('------------------------------------------------------------------------------------')

    def run(self):
        # while True:
        #     args = self.__queue_decoder.get()
        #     self.__process(handle=args[0], data=args[1])
        while True:
            try:
                # self.__queue_vitals.put(self.__motion_start_time)
                for packet in self.__motion_signal:
                    packet.set_epoch(self.__motion_start_time)
                    # print "In decoder motion", packet.get_index()
                    self.__process(handle=self.__ACC_0_handle, data=packet)
                    time.sleep(0.988)
                    self.__motion_start_time = self.__motion_start_time + 0.988
            except Exception as e:
                print 'DECODER ERROR', e
                # self.__process(handle=42, data=packet)

    def __process(self, handle, data):
        if handle == self.__ACC_0_handle:

            # if data[self.__FRAME_BYTE_INDEX] == Motion.HEADER_FRAME:
            #     if self.__motion_0.is_complete():
            #         self.__queue_vitals.put(self.__motion_0)
            #         self.__decoder_stats.validate_motion(self.__motion_0)
            #
            #     self.__motion_0 = Motion()
            # self.__motion_0.decode(data)
            self.__queue_vitals.put(data)
            self.__decoder_stats.validate_motion(data)

        elif handle == self.__SPO_0_handle:

            if data[self.__FRAME_BYTE_INDEX] == Optical.HEADER_FRAME:
                if self.__optical_0.is_complete():
                    # self.__log.i("IR", self.__optical_0.get_ir_signal())
                    self.__queue_vitals.put(self.__optical_0)
                    self.__decoder_stats.validate_optical(self.__optical_0)

                self.__optical_0 = Optical()
            self.__optical_0.decode(data)

        elif handle == self.__SPO_1_handle:
            if not self.__optical_1.is_frame(data):
                self.__optical_1 = Optical()

            if not self.__optical_1.is_complete():
                self.__optical_1.decode(data)

                if self.__optical_1.is_complete():
                    self.__queue_vitals.put(self.__optical_1)
                    self.__decoder_stats.validate_optical(self.__optical_1, backup=True)

        elif handle == self.__ACC_1_handle:
            if not self.__motion_1.is_frame_of(data):
                self.__motion_1 = Motion()

            if not self.__motion_1.is_complete():
                self.__motion_1.decode(data)

                if self.__motion_1.is_complete():
                    self.__queue_vitals.put(self.__motion_1)
                    self.__decoder_stats.validate_motion(self.__motion_1, backup=True)

        elif handle == self.__TEMP_handle:
            temperature = Temperature()
            temperature.decode(data)
            self.__queue_vitals.put(temperature)
            self.__decoder_stats.validate_temperature(temperature)

        elif handle == self.__SENSOR_handle:
            sensor = Sensor(data)
            self.__decoder_stats.process_sensor_status(sensor=sensor)
            self.__queue_vitals.put(sensor)
            self.__queue_audit.put(sensor)

        elif handle == self.__RESPONSE_handle:
            self.__response.decode(data)
            if not self.__response.get_optical_response_object() is None:
                self.__queue_vitals.put(self.__response.get_optical_response_object())
        else:
            print ("Handle : ", handle)

    @staticmethod
    def __read_file_optical(file_name):
        file = open(file_name, 'r')

        buffers = []
        for line in file:
            optical = Readoptical(line)
            if optical.is_valid():
                buffers.append(optical)
        return buffers

    @staticmethod
    def __read_file_motion(file_name):
        file = open(file_name, 'r')
        buffers = []
        for line in file:
            motion = Readmotion(line)
            if motion.is_valid():
                buffers.append(motion)
        return buffers

