import numpy as np
from Vital.Settings import Settings
from Bluetooth.monitor import *
from Wearable.max30102 import MAX30102
from multiprocessing import Process, Queue


class Calibrate:
    __instance = None

    @staticmethod
    def getInstance():
        if Calibrate.__instance is None:
            Calibrate(queue_command=Queue())
        return Calibrate.__instance

    def __init__(self, queue_command=Queue()):
        if Calibrate.__instance is not None:
            pass
            # raise Exception("This class is a singleton!")
        else:
            self.queue_command = queue_command
            self.irSaturationArray = []
            self.redSaturationArray = []
            self.lowerSaturation = 70.0         # 80.0%
            self.upperSaturation = 95.0         # 90.0%
            self.offsetAllowed = (self.upperSaturation - self.lowerSaturation) / 2
            self.device_ir_current = 24
            self.device_red_current = 33
            self.MIN_CURRENT = 0                # 0mA
            self.MAX_CURRENT = 255              # 50mA
            self.SENSOR_DETACH_CURRENT = 10     # 1.96mA (41 for testing purpose)
            self.enable_alg = bool(True)
            self.optical_config = MAX30102()
            self.irSettings = Settings()
            self.redSettings = Settings()
            Calibrate.__instance = self

    def enable_current_adjustment(self, enable_algorithm):
        self.enable_alg = enable_algorithm

    def is_current_adjustment(self):
        return self.enable_alg

    def get_lower_saturation(self):
        return self.lowerSaturation

    def get_upper_saturation(self):
        return self.upperSaturation

    def set_saturation(self, ir_saturation_array, red_saturation_array):
        self.irSaturationArray = ir_saturation_array
        self.redSaturationArray = red_saturation_array
        if self.enable_alg:
            self.adjust_currents()

    def set_currents(self, ir_current, red_current):
        self.irSettings.set_current(ir_current)
        self.redSettings.set_current(red_current)

    def set_lower_saturation(self, lower_saturation):
        self.lowerSaturation = lower_saturation
        self.offsetAllowed = (self.upperSaturation - self.lowerSaturation)/2

    def set_upper_saturation(self, upper_saturation):
        self.upperSaturation = upper_saturation
        self.offsetAllowed = (self.upperSaturation - self.lowerSaturation)/2

    def set_ppg_range1(self, new_range):
        self.irSettings.set_range(new_range)

    def get_count_in_range(self, saturation, lower_threshold, upper_threshold):
        count = 0
        for value in saturation:
            if lower_threshold < value < upper_threshold:
                count = count + 1
        return count

    def is_completed(self):
        status = bool(self.irSettings.get_number_of_accepted_values() == len(self.irSaturationArray) and self.redSettings.get_number_of_accepted_values() == len(self.redSaturationArray))
        return status

    def adjust_current(self, saturation_array, settings):
        if len(saturation_array) == 0:
            return settings                # do not have enough history to proceed, return

        latest_saturation = saturation_array[len(saturation_array) - 1]
        # print ("Latest saturation : ", latest_saturation)
        number_of_acceptable_values = self.get_count_in_range(saturation_array, self.lowerSaturation, self.upperSaturation)
        stop_current_adjustment = bool(number_of_acceptable_values == len(saturation_array))
        settings.set_stop_adjustments(stop_current_adjustment)
        # print ("Number of accepted values : ", number_of_acceptable_values)
        settings.set_number_of_accepted_values(number_of_acceptable_values)

        if stop_current_adjustment:
            return settings                 # do not adjust the current since all values are within the range

        minimum_saturation = min(saturation_array)
        minimum_saturation_index = saturation_array.index(minimum_saturation)
        maximum_saturation = max(saturation_array)
        maximum_saturation_index = saturation_array.index(maximum_saturation)
        offset = latest_saturation - (self.lowerSaturation + self.upperSaturation)/2
        study_trend = bool(self.lowerSaturation < latest_saturation < self.upperSaturation)

        if study_trend:
            return settings                 # don't update the current, but monitor the trend

        if abs(offset) >= self.offsetAllowed:
            last_index = int(len(saturation_array) - 1)

            if offset < 0:

                # if the saturation value seems to be changing away from the acceptable range, estimate the new current configuration
                if minimum_saturation == maximum_saturation or minimum_saturation_index == last_index or (0 < minimum_saturation_index < last_index and 0 < maximum_saturation_index < last_index):
                    updated_current = int(round(settings.get_current() - (settings.get_current()/ (latest_saturation + 1)) * offset))
                    updated_current = max(self.MIN_CURRENT, min(updated_current, self.MAX_CURRENT))
                    if updated_current <= self.SENSOR_DETACH_CURRENT:
                        updated_current = self.SENSOR_DETACH_CURRENT
                        settings.set_range(settings.get_upper_range())

                    if updated_current >= self.MAX_CURRENT:
                        updated_current = self.MAX_CURRENT
                        settings.set_stop_adjustments(True)
                        settings.set_stop_adjust_due_to_inability(True)              # terminate due to inability to reach the maximum
                        settings.set_current(updated_current)
                        settings.mark_as_updated(False)
                        return settings

                    settings.set_current(updated_current)
                    settings.mark_as_updated(True)
                    return settings

                # if the saturation value seems to be changing into the acceptable range, wait and check the next saturation value
                elif 0 < minimum_saturation_index < last_index or minimum_saturation_index == 0:
                    return settings

                else:
                    return settings

            elif offset > 0:

                # if the saturation value seems to be changing away from the acceptable range, estimate the new current configuration
                if minimum_saturation == maximum_saturation or (maximum_saturation_index == last_index) or (0 < minimum_saturation_index < last_index and 0 < maximum_saturation_index < last_index):
                    updated_current = int(round(settings.get_current() - (settings.get_current() / (latest_saturation + 1)) * offset))
                    updated_current = max(self.MIN_CURRENT, min(updated_current, self.MAX_CURRENT))

                    if updated_current <= self.SENSOR_DETACH_CURRENT:
                        updated_current = self.SENSOR_DETACH_CURRENT
                        settings.set_range(settings.get_upper_range())

                    if updated_current >= self.MAX_CURRENT:
                        updated_current = self.MAX_CURRENT
                        settings.set_stop_adjustments(True)
                        settings.set_stop_adjust_due_to_inability(True)                 # terminate due to inability to reach the maximum
                        settings.set_current(updated_current)
                        settings.mark_as_updated(False)
                        return settings

                    settings.set_current(updated_current)
                    settings.mark_as_updated(True)
                    return settings

                # if the saturation value seems to be changing into the acceptable range, wait and check the next saturation value
                elif 0 < maximum_saturation_index < last_index or maximum_saturation_index == 0:
                    return settings

                else:
                    return settings

            else:
                return settings

        return settings

    calibrationStarted = bool(False)

    def is_calibration_started(self):
        return self.calibrationStarted

    def set_calibration_started(self, calibration_started):
        self.calibrationStarted = calibration_started

    def adjust_currents(self):
        self.irSettings = self.adjust_current(self.irSaturationArray, self.irSettings)
        self.redSettings = self.adjust_current(self.redSaturationArray, self.redSettings)

        if self.irSettings.is_updated() or self.redSettings.is_updated():

            if self.irSettings.is_stop_adjust_due_to_inability() or self.redSettings.is_stop_adjust_due_to_inability():
                self.irSettings.set_stop_adjust_due_to_inability(False)
                self.redSettings.set_stop_adjust_due_to_inability(False)

            if not self.calibrationStarted:
                self.optical_config = MAX30102.getInstance()
                self.queue_command.put(UserCommands.trigger_calibration_mode())
                print ("*************************************************************************************Initializing Calibration Routine")
                self.set_upper_saturation(90.0)
                self.set_lower_saturation(80.0)
                self.calibrationStarted = True

            if self.irSettings.is_range_updated() or self.redSettings.is_range_updated():
                self.optical_config = MAX30102.getInstance()
                if self.irSettings.is_range_updated():

                    self.optical_config.set_ppg_range(self.irSettings.get_normal_range())
                    self.irSettings.reset_range_updated_flag()
                else:
                    self.optical_config.set_ppg_range(self.redSettings.get_normal_range())
                    self.redSettings.reset_range_updated_flag()

                self.optical_config.set_ir_current(self.irSettings.get_current())
                self.optical_config.set_red_current(self.redSettings.get_current())
                self.queue_command.put(self.optical_config.encode_write_configuration())
                print ("*************************************************************************************Optical sensor range is updated")

            else:
                self.optical_config = MAX30102.getInstance()
                self.optical_config.set_ir_current(self.irSettings.get_current())
                self.optical_config.set_red_current(self.redSettings.get_current())
                self.queue_command.put(self.optical_config.encode_write_configuration())

            self.irSettings.mark_as_updated(False)
            self.redSettings.mark_as_updated(False)

        case1 = bool(self.irSettings.is_stop_adjustments() and self.redSettings.is_stop_adjustments() and self.calibrationStarted)
        case2 = bool(self.irSettings.is_stop_adjust_due_to_inability() and self.redSettings.is_stop_adjust_due_to_inability() and self.calibrationStarted)
        case3 = bool(self.irSettings.is_stop_adjust_due_to_inability() and self.redSettings.is_stop_adjustments() and self.calibrationStarted)
        case4 = bool(self.irSettings.is_stop_adjustments() and self.redSettings.is_stop_adjust_due_to_inability() and self.calibrationStarted)

        if case1 or case2 or case3 or case4:
            self.optical_config = MAX30102.getInstance()
            if self.irSettings.is_stop_adjust_due_to_inability() or self.redSettings.is_stop_adjust_due_to_inability():
                self.optical_config.set_ir_current(self.irSettings.get_current())
                self.optical_config.set_red_current(self.redSettings.get_current())
                self.queue_command.put(self.optical_config.encode_write_configuration())

            self.queue_command.put(UserCommands.trigger_normal_operation_mode())
            self.calibrationStarted = False
            self.irSettings.set_stop_adjust_due_to_inability(False)
            self.redSettings.set_stop_adjust_due_to_inability(False)

            self.set_upper_saturation(95.0)
            self.set_lower_saturation(70.0)
            print ("*************************************************************************************Terminating Calibration Routine")


