from Utils.util import Util

class Reconnect:

    def __init__(self,replace=False,address=''):
        self.__replace = replace

        if replace:
            if Util.validate_address(address):
                self.__address = address
            else:
                self.__address = ''
        else:
            self.__address = ''

    def is_replaced(self):
        return self.__replace

    def get_replacing_mac(self):
        return self.__address