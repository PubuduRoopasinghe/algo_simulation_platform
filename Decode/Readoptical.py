import re
import json
import time

from Utils.util import Util


class Readoptical:
    SAMPLE_RATE = 25
    __period = 0
    __delta  = 0
    __OPTICAL_CONFIGURATION = '{"Optical Configuration":{[\\W|\\w]*,"PulseWidth":' \
                              '[\\W|\\w]*},"Session Info":{[\\W|\\w]*},"Device Info":{[\\W|\\w]*}'
    __OPTICAL_SCHEME = "\\d{10}:\\d+:\\d+:(\\[(\\d+, ){24}\\d+\\]:\\[(\\d+, ){24}\\d+\\])"
    __MAX_ADC_VALUE = (1 << 18)

    def __init__(self, record):
        self.__text  = record
        self.__time_epoch  = None
        self.__timestamp = None
        self.__original_epoch_time = None
        self.__original_timestamp = None
        self.__index = None
        self.__ir    = None
        self.__red   = None

    def is_valid(self):
        return self.__process()

    def __process(self):
        if re.search(Readoptical.__OPTICAL_SCHEME, self.__text):
            data = self.__text.split(":")
            self.__original_timestamp = int(data[0])
            self.__original_epoch_time  = int(data[0]) + int(data[1]) / 1000.0
            self.__index = int(data[2])
            self.__ir    = self.__extract_values_from_array(data[3])
            self.__red   = self.__extract_values_from_array(data[4])
            return True
        # elif re.search(Optical.__OPTICAL_CONFIGURATION,self.__text):
        #     line = json.loads(self.__text.replace('\n', ''))
        #     session = line['Optical Configuration']['PulseWidth']
        #     if '411us' in session:
        #         Optical.__MAX_ADC_VALUE = (1 << 18)
        #     elif '215us' in session:
        #         Optical.__MAX_ADC_VALUE = (1 << 17)
        #     elif '118us' in session:
        #         Optical.__MAX_ADC_VALUE = (1 << 16)
        #     elif '69us' in session:
        #         Optical.__MAX_ADC_VALUE = (1 << 15)
        # return False

    @staticmethod
    def __extract_values_from_array(input="[]"):
        values = input.replace('[', '').replace(']', '').replace('\n', '').split(',')
        output = []
        for value in values:
            output.append(int(value))
        return output

    def in_range(self, first, last):
        return last >= self.__time_epoch >= first or last >= self.__time_epoch + self.__period >= first \
               or last >= self.__time_epoch + self.__period / 2 >= first

    def is_complete(self):
        return len(self.__ir)==25 and len(self.__red)==25

    def get_original_timestamp(self):
        return self.__original_timestamp

    def get_original_epoch_time(self):
        return self.__original_epoch_time

    def set_epoch(self, epoch):
        self.__time_epoch = epoch

    def get_epoch(self):
        return self.__time_epoch

    def set_time(self, time):
        self.__timestamp = time

    def get_time(self):
        return int(self.__time_epoch)

    def get_ir_signal(self):
        return self.__ir

    def get_red_signal(self):
        return self.__red

    def get_index(self):
        return self.__index

    @staticmethod
    def set_sample_period(period):
        Readoptical.__period = period
        Readoptical.__delta= period / Readoptical.SAMPLE_RATE

    @staticmethod
    def get_sample_period():
        return Readoptical.__period

    def get_time_for(self, index):
        return self.__time_epoch + index * Readoptical.__delta

    def get_sample_index_at(self, time):
        offset = time - self.__time_epoch
        if 0 <= offset <= Readoptical.__period:
            index = int(offset / Readoptical.__delta)
            error = offset - index * Readoptical.__delta
            if error >= (Readoptical.__delta / 2):
                index += 1
                if index < Readoptical.__SAMPLE_RATE:
                    return index
                return -1
            return index
        return -1

    def get_sample_at(self, index,  with_time=True):
        if with_time:
            return '{0:.3f},{1:d},{2:d}'.format(self.get_time_for(index),self.__ir[index], self.__red[index])
        else:
            return '{0:d},{1:d}'.format(self.get_time_for(index),self.__ir[index], self.__red[index])

    def __str__(self):
        return 'Timestamp : {0:.3f} and Index : {1}'.format(self.__time_epoch, self.__index)

    def get_record(self):
        return self.__text