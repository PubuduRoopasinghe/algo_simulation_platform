import subprocess
import time

class WifiManager:
    def __init__(self):
        self.last_available_hotspots = []
        self.ssid = ''

    def scan(self):
        res = subprocess.check_output(['connmanctl', 'scan', 'wifi'])

        self.last_available_hotspots = subprocess.check_output(['connmanctl', 'services']).split()

    def availableHotspots(self):
        hotspots = []
        for i in range(1, len(self.last_available_hotspots), 2):
            hotspots.append(self.last_available_hotspots[i])

        return hotspots

    def initializeConnection(self, essid_, passphrase_):
        self.scan()
        time.sleep(1.0)

        for i in range(0, len(self.last_available_hotspots)):
            if self.last_available_hotspots[i] == essid_:
                try:
                    self.resetConnection()  #resets the connection
                    time.sleep(1.0)

                    txt = ''
                    self.ssid = self.last_available_hotspots[i + 1]

                    txt += '[' + self.ssid + ']\n'
                    txt += 'Name=' + essid_ + '\n'
                    txt += 'SSID=' + self.ssid.split('_')[2] + '\n'
                    txt += 'Frequency=2412\n'
                    txt += 'Favorite=true\n'
                    txt += 'AutoConnect=true\n'
                    txt += 'Modified=2019-06-03T04:04:06.265444Z\n'
                    txt += 'Passphrase=' + passphrase_ + '\n'
                    txt += 'IPv4.method=dhcp\n'
                    txt += 'IPv4.DHCP.LastAddress=192.168.43.67\n'
                    txt += 'IPv6.method=auto\n'
                    txt += 'IPv6.privacy=disabled\n'

                    print txt

                    subprocess.call(['rm -r /var/lib/connman/wifi*'], shell=True)
                    subprocess.call(['mkdir', '-p', '/var/lib/connman/' + self.ssid])

                    f = open('/var/lib/connman/' + self.ssid + '/settings', 'w+')
                    f.write(txt)
                    f.close()

                    self.resetConnection()  # resets the connection
                    time.sleep(1.0)

                    break

                except:
                    print 'Error while connecting to the network'
                    pass

    def resetConnection(self):
        subprocess.call(['systemctl', 'restart', 'connman.service'])

    def disconnectConnection(self):
        print self.ssid
        subprocess.call(['connmanctl', 'disconnect', self.ssid])

    def disableHotspot(self):
        subprocess.call(['systemctl', 'stop', 'bb-wl18xx-wlan0.service'])
        subprocess.call(['systemctl', 'disable', 'bb-wl18xx-wlan0.service'])

    def enableHotspot(self):
        subprocess.call(['systemctl', 'enable', 'bb-wl18xx-wlan0.service'])
        subprocess.call(['systemctl', 'start', 'bb-wl18xx-wlan0.service'])


# if __name__ == '__main__':
#
#     print 'started'
#     wifi = WifiManager()
#
#     wifi.initializeConnection('SYNERGEN-TL', '$3cur3@STL2019!')
#     print 'new connection initialized'
#
#     # wifi.resetConnection()
#
#     print 'waiting to be disconnected'
#     time.sleep(5.0)
#     wifi.disconnectConnection()
#     print 'connection disconnected'
#
#     print 'disconnecting Hotspot'
#     wifi.disableHotspot()
#     time.sleep(20.0)
#
#     print 'enabling Hotspot'
#     wifi.enableHotspot()