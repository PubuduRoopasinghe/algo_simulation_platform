from Utils.util import Util
from Auditor.Messages.outofrangewarning import AbnormalValueWarning


class VitalCondition:

    def __init__(self, name={}, conditions={},age=0):
        if "criteria" in conditions:
            for condition in conditions["criteria"]:
                if condition["age"]['minimum'] <= age < condition["age"]['maximum']:

                    self.__minimum   = condition["range"]['minimum']
                    self.__maximum   = condition["range"]['maximum']
                    self.__threshold = condition["tolerance"]['count']
                    self.__window    = condition["tolerance"]['width']
                    self.__suspend   = condition['suspend']
                    self.__repeat    = condition['repeat']
                    self.__priority  = condition['priority']
                    self.__alarms    = condition['alarm']
                    self.__event_notified  = False
                    self.__detected_normal = True
                    self.__next_evaluation = Util.get_time()
                    self.__detected_events = []
                    self.__abnormality     = None
                    self.__name            = name

    def clear(self):
        self.__event_notified  = False
        self.__detected_normal = True
        self.__abnormality     = None
        self.__next_evaluation = (Util.get_time() + self.__suspend)
        del self.__detected_events[:]

    def reset(self):
        self.__event_notified  = False
        self.__detected_normal = True
        self.__abnormality     = None
        self.__next_evaluation = Util.get_time()
        del self.__detected_events[:]
    """ 
    returns True if the condition is considered to be normal
    """
    def evaluate(self,value):
        if self.__next_evaluation <= Util.get_time():

            self.__detected_events.append(self.__minimum <= value <= self.__maximum)
            del self.__detected_events[0:-self.__window]

            self.__detected_normal = sum(1 for normal in self.__detected_events if not normal) < self.__threshold

            if not self.__detected_normal:
                self.__abnormality     = AbnormalValueWarning(name=self.__name, value=value, interval=[self.__minimum, self.__maximum],
                                                              priority=self.__priority)
                self.__next_evaluation = (Util.get_time() + self.__repeat)
                self.__event_notified  = False
            return self.__detected_normal
        return True

    def get_warning(self):
        return self.__abnormality

    def get_priority(self):
        return self.__priority

    def trigger_alarm(self):
        return self.__alarms