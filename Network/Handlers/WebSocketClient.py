from tornado.ioloop import IOLoop, PeriodicCallback
from tornado import gen
from tornado.websocket import websocket_connect
import json
import time
import configparser
import Queue as queueException
from Vital.BabyVitals import BabyVitals
from Network.Handlers.DatabaseHandler import DatabaseHandler
from BME680.RoomCondition import RoomCondition


class WebSocketClient:
    def __init__(self, url_, queue_network_, queue_vital_soc_):
        self.url = url_
        self.queue_network = queue_network_
        self.queue_vital_soc = queue_vital_soc_
        self.ioloop = IOLoop.instance()
        self.ws = None
        self.db_handler = DatabaseHandler('database.db')

        self.__infant_ID = self.get_infant_id()
        self.__token = ''

        self.__db_clear_interval = 20  #24 * 60 * 60
        self.__db_last_cleared = 0
        self.__upload_time = time.time()
        self.__upload_check_time = 20

        self.get_token()
        self.connect()
        self.db_handler.connect()
        PeriodicCallback(self.keep_alive, 2000).start()
        self.ioloop.start()

    @gen.coroutine
    def connect(self):
        try:
            print "NETWORK - Connecting - WebSocket : " + self.url
            self.ws = yield websocket_connect(self.url)
        except Exception, e:
            print "NETWORK - Connection Error - WebSocket : " + self.url
        else:
            print "NETWORK - Connected - WebSocket : " + self.url

    def keep_alive(self):
        try:
            data = self.queue_network.get_nowait()
            self.queue_vital_soc.put(data)

        except queueException.Empty:
            # print "NETWORK - Network Queue Empty"
            return

        if isinstance(data, BabyVitals):
            __heart_rate = data.getHeartRateOutlierRemoved()
            __spo2 = data.getOxygenLevelOutlierRemoved()
            __body_temperature = data.getBodyTemperatureOutlierRemoved()
            __time_stamp = time.time()

            values = '(' + str(__time_stamp) + ',' + str(__heart_rate) + ',' + str(__spo2) \
                     + ',' + str(__body_temperature) + ')'

            self.db_handler.insert_record('Vitals', '(time, heartrate, spo2, temperature)', values)

        elif isinstance(data, RoomCondition):
            __room_temperature = data.get_roomTemperature()
            __room_humidity = data.get_roomHumidity()
            __room_air_quality = data.get_roomGasQuality()
            __room_pressure = data.get_roomPressure()
            __time_stamp = time.time()

            values = '(' + str(__time_stamp) + ',' + str(__room_temperature) + ',' + str(__room_humidity) \
                     + ',' + str(__room_air_quality) + ',' + str(__room_pressure) + ')'

            self.db_handler.insert_record('RoomConditions', '(time, temperature, humidity, airQuality, pressure)', values)

        elif isinstance(data, str):  # updating device status
            pass

        self.upload_data(data)

    def upload_data(self, data):
        if self.ws is None:
            self.connect()
            return

        __time = time.time()

        if __time - self.__upload_time > self.__upload_check_time:

            print __time - self.__upload_time

            not_uploaded_vitals = self.db_handler.get_records('Vitals', __time,  __time - self.__upload_time)
            not_uploaded_room_conditions = self.db_handler.get_records('RoomConditions', __time, __time - self.__upload_time)
            not_uploaded_device_status = self.db_handler.get_records('DeviceStatus', __time, __time - self.__upload_time)

            print not_uploaded_vitals
            print not_uploaded_room_conditions
            print not_uploaded_device_status

            msg_buffer = []

            for record in not_uploaded_vitals:
                msg = {
                    'infantId': self.__infant_ID,
                    'generatedTime': record[0],
                    'heartRate': record[1],
                    'spo2Level': record[2],
                    'bodyTemperature': record[3],
                    'type': 'Vitals'
                }
                msg_buffer.append(msg)

            for record in not_uploaded_room_conditions:
                msg = {
                    'infantId': self.__infant_ID,
                    'generatedTime': record[0],
                    'roomTemperature': record[1],
                    'humidityLevel': record[2],
                    'roomAirQualityIndex': record[3],
                    'type': 'RoomConditions'
                }
                msg_buffer.append(msg)

            for record in not_uploaded_device_status:
                print record
                pass

            for msg in msg_buffer:
                msg_json = json.dumps(msg)

                try:
                    self.ws.write_message(msg_json)
                except Exception as e:
                    print "NETWORK - Write failed - Websocket : " + self.url
                    self.ws = None
                    return

            self.__upload_time = __time

        else:

            if isinstance(data, BabyVitals):
                msg = {
                    'infantId': self.__infant_ID,
                    'generatedTime': time.time(),
                    'heartRate': data.getHeartRateOutlierRemoved(),
                    'spo2Level': data.getOxygenLevelOutlierRemoved(),
                    'bodyTemperature': data.getBodyTemperatureOutlierRemoved(),
                    'type': 'Vitals'
                }

            elif isinstance(data, RoomCondition):
                msg = {
                    'infantId': self.__infant_ID,
                    'generatedTime': time.time(),
                    'roomTemperature': data.get_roomTemperature(),
                    'humidityLevel': data.get_roomHumidity(),
                    'roomAirQualityIndex': data.get_roomGasQuality(),
                    'type': 'RoomConditions'
                }
            elif isinstance(data, str):
                msg = {}
                pass
            else:
                print 'NETWORK - Invalid message type passed to network queue'
                return

            msg_json = json.dumps(msg)

            try:
                self.ws.write_message(msg_json)
                self.__upload_time = __time
            except Exception as e:
                print "NETWORK - Write failed - Websocket : " + self.url
                self.ws = None
                return

        self.clear_old_data(__time)

    def clear_old_data(self, time_):
        if time_ - self.__db_last_cleared > self.__db_clear_interval:
            self.db_handler.delete_old_records(self.__db_clear_interval)
            self.__db_last_cleared = time_
            print 'Old Data Cleared'

    @staticmethod
    def get_infant_id():
        config = configparser.ConfigParser()
        config.read('Network/Config/config.ini')

        if config.has_section('Infant Data'):
            return config.get('Infant Data', 'ID')

        else:
            config.add_section('Infant Data')
            config.set('Infant Data', 'ID', str(0))

            with open('Network/Config/config.ini', 'w+') as configFile:
                config.write(configFile)

            return 0

    def get_token(self):
        expired = False

        if expired:
            # get new token some how
            return self.__token
        else:
            return self.__token

# if __name__=='__main__':
#     queue = Queue()
#     client = WebSocketClient('ws://localhost:8888/vital_soc', queue)

    # ioloop = IOLoop.instance()
    # ioloop.start()

