import json
from enum import Enum
from Utils.logs import Logs
from Utils.util import Util
# from Bluetooth.connection import BLE_STATE
from Auditor.Rules.wearable import Wearable
from Auditor.Rules.room import RoomCondition
from Auditor.Messages.wanings import Warnings
from Auditor.Rules.vital import VitalCondition
from Auditor.Rules.paramters import ParameterName
from Configurations.basestation import BaseStation
from Notification.notification import Notification
from Audio.Cry_Detection.crynotice import CryNotice
from Auditor.Messages.vitalupdate import VitalUpdate
from Auditor.Messages.crynotification import CryNotification
from Auditor.Messages.roomconditionupdate import RoomConditionUpdate

class STATES(Enum):
    WEARABLE_UNKNOWN = 0x00
    BLE_DISCONNECTED = 0x01
    BLE_CONNECTED    = 0x02
    WEARABLE_CHARGING= 0x03
    WEARABLE_ACTIVE  = 0x04
    WEARABLE_INACTIVE= 0x05

class Engine:

    def __init__(self,queue_network,queue_notify,log=Logs('Engine')):
        self.__queue_network= queue_network
        self.__queue_notify = queue_notify
        self.__logs         = log
        self.__HR_condition = VitalCondition()
        self.__OS_condition = VitalCondition()
        self.__BT_condition = VitalCondition()
        self.__RT_condition = RoomCondition()
        self.__RH_condition = RoomCondition()
        self.__AQ_condition = RoomCondition()
        self.__wearable     = Wearable()
        self.__device_state = STATES.WEARABLE_UNKNOWN

    def initialize(self):
        self.__set_room_conditions()
        self.__set_vital_condition()

    def __set_vital_condition(self):
        age = Util.get_age(BaseStation.get_birthday())
        # self.__logs.i('Date of Birth', '{0} \t({1} days old)'.format(Util.get_date_time(BaseStation.get_birthday()), age))

        with open('Auditor/Rules/rules.json') as rule_set:
            rules = json.loads(rule_set.read())

            self.__HR_condition = VitalCondition('HEART_RATE',
                                                 rules['vital'][ParameterName.HeartRate]  , age)
            self.__OS_condition = VitalCondition('SPO2_LEVEL',
                                                 rules['vital'][ParameterName.OxygenSaturation], age)
            self.__BT_condition = VitalCondition('BODY_TEMPERATURE',
                                                 rules['vital'][ParameterName.BodyTemperature] , age)

    def __set_room_conditions(self):

        with open('Auditor/Rules/rules.json') as rule_set:
            rules = json.loads(rule_set.read())

            self.__RT_condition = RoomCondition('ROOM_TEMPERATURE',
                                                rules['room'][ParameterName.RoomTemperature])
            self.__RH_condition = RoomCondition('HUMIDITY_LEVEL',
                                                rules['room'][ParameterName.RoomHumidity])
            self.__AQ_condition = RoomCondition('ROOM_AIR_QUALITY_INDEX',
                                                rules['room'][ParameterName.RoomAirQuality])

            self.__wearable = Wearable(rules['wearable'])

    def evaluate_sensors(self, sensor):
        if not self.__device_state == STATES.WEARABLE_ACTIVE and sensor.is_monitoring():  # starting the monitoring session
            self.__device_state = STATES.WEARABLE_ACTIVE
            self.__set_vital_condition()

        elif not self.__device_state == STATES.WEARABLE_ACTIVE and not sensor.is_monitoring():# terminating the monitoring session
            self.__device_state = STATES.WEARABLE_INACTIVE

        if not self.__wearable.evaluate(sensor):
            self.__queue_network.put(self.__wearable.get())


    # def evaluate_ble_connection(self, connection):
    #
    #     if connection.get_state() == BLE_STATE.BAND_CONNECTED:
    #         self.__device_state = STATES.BLE_CONNECTED
    #         self.__set_vital_condition()
    #         self.__logs.i('Band', 'Connected to      :{0}'.format(connection.get_band()))
    #
    #     elif connection.get_state() == BLE_STATE.BAND_DISCONNECTED:
    #         self.__device_state = STATES.BLE_DISCONNECTED
    #         self.__logs.e('Band', 'Disconnected from : {0}'.format(connection.get_band()))
    #
    #     elif connection.get_state() == BLE_STATE.BAND_UNAVAILABLE:
    #         self.__logs.e('Band', 'Band unavailable  : {0}'.format(connection.get_band()))
    #
    #     elif connection.get_state() == BLE_STATE.BAND_DETECTED:
    #         self.__logs.i('Band','Band registered    :{0}'.format(connection.get_band()))
    #
    #     elif connection.get_state() == BLE_STATE.NO_CHARGING_DEVICES:
    #         self.__logs.e('Band', 'Device setup failed (no charging devices)')
    #
    #     elif connection.get_state() == BLE_STATE.NO_SCAN_RESULTS:
    #         self.__logs.e('Band', 'Device setup failed (devices not available)')

    def evaluate_wifi_connection(self):
        pass

    def evaluate_infant_vitals(self, vital_parameters):

        if vital_parameters.is_wiggling():
            self.__queue_network.put(VitalUpdate(wiggling=True))
            self.__logs.w('Wiggling','Baby Wiggling Detected')

        else:
            vital_update = VitalUpdate(vital_parameters.get_heart_rate(),
                                       vital_parameters.get_oxygen_level(),
                                       vital_parameters.get_temperature())

            warnings      = Warnings()
            send_warning  = False
            trigger_alarm = False
            priority      = 0

            if not self.__HR_condition.evaluate(vital_parameters.get_heart_rate()):
                warnings.append(self.__HR_condition.get_warning())
                send_warning   = True
                trigger_alarm |= self.__HR_condition.trigger_alarm()
                priority       = max(priority,self.__HR_condition.get_priority())

            if not self.__OS_condition.evaluate(vital_parameters.get_oxygen_level()):
                warnings.append(self.__OS_condition.get_warning())
                send_warning   = True
                trigger_alarm |= self.__OS_condition.trigger_alarm()
                priority       = max(priority, self.__OS_condition.get_priority())

            if not self.__BT_condition.evaluate(vital_parameters.get_temperature()):
                warnings.append(self.__BT_condition.get_warning())
                send_warning   = True
                trigger_alarm |= self.__BT_condition.trigger_alarm()
                priority       = max(priority, self.__BT_condition.get_priority())

            self.__queue_network.put(vital_update)

            if send_warning:
                self.__queue_network.put(warnings)

                if trigger_alarm:
                    self.__queue_notify.put(Notification(True,priority))


    def evaluate_room_condition(self, room_conditions):
        room_condition_update = RoomConditionUpdate(room_conditions.get_temperature(),
                                                    room_conditions.get_humidity(),
                                                    room_conditions.get_air_quality(),
                                                    room_conditions.get_pressure())
        warnings = Warnings()
        send_warning = False

        if not self.__RH_condition.evaluate(room_conditions.get_humidity()):
            warnings.append(self.__RH_condition.get_warning())
            send_warning = True

        if not self.__RT_condition.evaluate(room_conditions.get_temperature()):
            warnings.append(self.__RT_condition.get_warning())
            send_warning = True

        if not self.__AQ_condition.evaluate(room_conditions.get_air_quality()):
            warnings.append(self.__AQ_condition.get_warning())
            send_warning = True
        self.__queue_network.put(room_condition_update)

        if send_warning:
            self.__queue_network.put(warnings)

    def evaluate_cry_conditions(self,cry_notice=CryNotice()):
        cry_notification = CryNotification(cry_notice.get_signal())
        self.__queue_network.put(cry_notification)

    def __clear_vital_notifications(self):
        self.__HR_condition.clear()
        self.__OS_condition.clear()




