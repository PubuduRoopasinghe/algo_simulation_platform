from Utils.logs import Logs
from Decode.sensor import Sensor
from multiprocessing import Process
from Auditor.Rules.engine import Engine
# from Bluetooth.connection import Connection
from Notification.Message.error import Error
from Utils.util import Util
from Vital.vitalparameters import VitalParameters
from Audio.Cry_Detection.crynotice import CryNotice
from Notification.Message.clearmessage import ClearMessage
from Sensors.AirQuality.roomcondition import RoomCondition as RoomParameters


class Audit(Process):

    def __init__(self, queue_audit, queue_network, queue_notify):
        super(Audit, self).__init__()
        self.__queue_audit   = queue_audit
        self.__queue_network = queue_network
        self.__queue_notify  = queue_notify
        self.__logs          = Logs('Audit')
        self.__engine        = Engine(queue_network,queue_notify, self.__logs)
        self.__optical_data_time_diff_with_current_time = None

    def run(self):
        self.__engine.initialize()

        while True:
            instance = self.__queue_audit.get()

            if isinstance(instance, VitalParameters):
                self.__engine.evaluate_infant_vitals(instance)
                # print instance.get_heart_rate(),instance.get_oxygen_level(),instance.get_temperature(),instance.is_wiggling()
                self.__find_matching_empatica_hr(self.__empatica_data, instance.get_heart_rate(), Util.exact_time())

            elif isinstance(instance, RoomParameters):
                self.__engine.evaluate_room_condition(instance)

            elif isinstance(instance,Sensor):
                self.__engine.evaluate_sensors(instance)

            elif isinstance(instance, CryNotice):
                self.__engine.evaluate_cry_conditions(instance)

            elif isinstance(instance,Error):
                self.__handle_error_messages(error=instance)

            # elif isinstance(instance, Connection):
            #
            #     self.__engine.evaluate_ble_connection(instance)

            elif isinstance(instance,ClearMessage):
                self.__engine.clear_notifications()

            else:
                # print "audit queue : ", instance
                self.__optical_data_time_diff_with_current_time = instance
                file_paths = ['F:\\Synergen_Work\\Work 7_Simulation platform\\Collected_Data\\HR_Test_Data_Rasa']
                for path in file_paths:
                    print('------------------------------------------------------------------------------------')
                    print('Directory Path : {0}\n'.format(path))
                    self.__empatica_hr = self.__read_empatica_hr(path + '\\HR.csv')

                self.__empatica_data = [[0] * (len(self.__empatica_hr) - 2), [0] * (len(self.__empatica_hr) - 2)]
                self.__empatica_hr_start_time = self.__empatica_hr[0] + self.__optical_data_time_diff_with_current_time
                for index in range(0, len(self.__empatica_hr) - 2):
                    self.__empatica_data[0][index] = self.__empatica_hr_start_time
                    self.__empatica_data[1][index] = self.__empatica_hr[index + 2]
                    self.__empatica_hr_start_time = self.__empatica_hr_start_time + 1

    def __handle_error_messages(self,error):
        self.__logs.e(error.get_root(),error.get_event_message())

    @staticmethod
    def __read_empatica_hr(file_name):
        file = open(file_name, 'r')
        buffers = []
        for line in file:
            # motion = Readmotion(line)
            # if motion.is_valid():
            #     buffers.append(motion)
            buffers.append(float(line))
        return buffers

    def __find_matching_empatica_hr(self, empatica_data, hr_value, timestamp):
        for index in range(0, len(empatica_data[0])):
            timestamp_error = abs(empatica_data[0][index] - timestamp)
            if timestamp_error < 0.5:
                hr_error = empatica_data[1][index] - hr_value
                # print "Heart rate from empatica      :",empatica_data[0][index], "|", empatica_data[1][index]
                print timestamp, "|", hr_value, "|", empatica_data[0][index], "|", empatica_data[1][index], "|", hr_error
                # self.__logs.i("Data : ", str(timestamp) + " " + str(hr_value) + " " + str(empatica_data[0][index]) + " " + str(empatica_data[1][index]) + " " + str(hr_error))
                # print abs(hr_error)
                # if abs(hr_error) > 1:
                #     print "-------------------- more than 1 digit error------------------- "









