import numpy as np
from scipy.signal import butter, filtfilt
from enum import Enum

class ActivityLevel(Enum):
    ACTIVITY_INTENSITY_LOW    = 0x01
    ACTIVITY_INTENSITY_MEDIUM = 0x02
    ACTIVITY_INTENSITY_HIGH   = 0x03

class SMA:

    __DYNAMIC_FILTER_LENGTH = 21
    __STATIC_FILTER_LENGTH  = 11
    __MEDIUM_ACTIVITY_LEVEL_THRESHOLD = 15
    __HIGH_ACTIVITY_LEVEL_THRESHOLD   = 25

    def __init__(self):
        self.__sma_array     = []
        self.__sma_intensity = 0.0
        self.__first_event   = True

    def process(self, acc_x=[], acc_y=[], acc_z=[], sample_rate=25.0):

        filtered_acc_x = filtfilt(np.ones(3) / 3, 1, acc_x)
        filtered_acc_y = filtfilt(np.ones(3) / 3, 1, acc_y)
        filtered_acc_z = filtfilt(np.ones(3) / 3, 1, acc_z)

        # Dynamic Component caused by motion
        dynamic_isolation_motion_acc_x = self.__dynamic_iso_motion(filtered_acc_x)
        dynamic_isolation_motion_acc_y = self.__dynamic_iso_motion(filtered_acc_y)
        dynamic_isolation_motion_acc_z = self.__dynamic_iso_motion(filtered_acc_z)

        # Static Component Due to isolated_dynamicity
        # isolated_static_component_acc_x = self.isolated_static(filtered_acc_x)
        # isolated_static_component_acc_y = self.isolated_static(filtered_acc_y)
        # isolated_static_component_acc_z = self.isolated_static(filtered_acc_z)

        # Mean of dynamic isolation motion of x, y, z axes for 6 seconds
        mean_dim_acc_x = np.mean(dynamic_isolation_motion_acc_x)
        mean_dim_acc_y = np.mean(dynamic_isolation_motion_acc_y)
        mean_dim_acc_z = np.mean(dynamic_isolation_motion_acc_z)

        # Compute SMA value
        if not self.__first_event:
            no_of_seconds = 1
        else:
            no_of_seconds = int(len(acc_x)/sample_rate)

        new_sma_values = np.zeros(no_of_seconds)

        for second_index in range(no_of_seconds):
            new_sma_values[second_index] = 0.0

            if self.__first_event:
                starting_index_of_packet = second_index
            else:
                starting_index_of_packet = int(len(acc_x)/sample_rate) - 1    # newly added

            # compute SMA value for 1 second duration
            for index in range(int(starting_index_of_packet*sample_rate), int(sample_rate*(starting_index_of_packet + 1))):
                new_sma_values[second_index] +=  np.abs(dynamic_isolation_motion_acc_x[index] - mean_dim_acc_x)\
                                                                      + np.abs(dynamic_isolation_motion_acc_y[index] - mean_dim_acc_y) \
                                                                      + np.abs(dynamic_isolation_motion_acc_z[index] - mean_dim_acc_z)

        self.__sma_array.extend(new_sma_values)
        self.__first_event = False
        del self.__sma_array[0:-6]

        self.__sma_intensity = np.mean(self.__sma_array)


    def reset(self):
        del self.__sma_array[:]
        self.__sma_intensity = 0
        self.__first_event   = True

    @staticmethod
    def __dynamic_iso_motion(acc, sample_rate=25.0, cut_off=0.5, order=2):
        high = cut_off/(sample_rate/2)
        [b, a] = butter(order,high, btype='high')
        acc = [float(t) for t in acc]

        if len(acc) > SMA.__DYNAMIC_FILTER_LENGTH:
            return filtfilt(b, a, acc, padtype='odd', padlen=3*(max(len(b), len(a)) - 1))
        else:
            return np.zeros(len(acc))

    @staticmethod
    def __isolated_static(acc, sample_rate=25.0, cut_off=0.5, order=1):

        low = cut_off/(sample_rate/2)
        [b, a] = butter(order, low, btype='low')
        acc = [float(t) for t in acc]

        if len(acc) > SMA.__STATIC_FILTER_LENGTH:
            return filtfilt(b, a, acc, padtype='odd', padlen=3 * (max(len(b), len(a)) - 1))
        else:
            return np.zeros(len(acc))

    def get_activity_intensity(self):
        return self.__sma_intensity

    def get_activity_level(self):
        if self.__sma_intensity < SMA.__MEDIUM_ACTIVITY_LEVEL_THRESHOLD:
            return ActivityLevel.ACTIVITY_INTENSITY_LOW
        elif self.__sma_intensity < SMA.__HIGH_ACTIVITY_LEVEL_THRESHOLD:
            return ActivityLevel.ACTIVITY_INTENSITY_MEDIUM
        else:
            return ActivityLevel.ACTIVITY_INTENSITY_HIGH
