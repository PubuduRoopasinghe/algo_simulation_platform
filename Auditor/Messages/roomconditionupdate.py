import json
from Utils.util import Util
from Auditor.Messages.message import Message


class RoomConditionUpdate(Message):

    def __init__(self, temperature, humidity, air_quality, pressure):

        self.__detected_time= Util.get_epoch()
        self.__humidity     = humidity
        self.__pressure     = pressure
        self.__temperature  = temperature
        self.__air_quality  = air_quality

    def get(self, infant=''):
        return json.dumps({'detectedTime'     : self.__detected_time,
                           'roomHumidity'     : self.__humidity,
                           'roomPressure'     : self.__pressure,
                           'roomTemperature'  : self.__temperature,
                           'roomAirQuality'   : self.__air_quality,
                           'infantId'         : infant})