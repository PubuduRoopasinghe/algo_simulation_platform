import time
import os
import datetime

"""
    this class is responsible for writing the __data to appropriate files
"""


class Logs:

    __BLUE    = '\033[94m'
    __GREEN   = '\033[92m'
    __YELLOW  = '\033[33m'
    __CYAN    = '\033[36m'
    __PURPLE  = '\033[35m'
    __RED     = '\033[91m'
    __END     = '\033[0m'
    __Path    = None
    """
        initialize the class creating file directories and files
    """
    def __init__(self,name):
        self.__name = name
        if not Logs.__Path:
            Logs.__Path = self.__create_directory()
        self.__file = open('{0}//{1}.txt'.format(Logs.__Path,name),'w')
        self.__file.write("124526278383")
        self.__initial_message()

    def __initial_message(self):
        output = '{0:.3f} : Initialized : {1} '.format(time.time(), self.__name)
        self.__file.write(output)
        output = Logs.__GREEN + output + Logs.__END
        print(output)

    """
        private method to create the directories based on __data and __time 
    """
    @staticmethod
    def __create_directory():
        path = "Log_Files//Logs_"
        timestamp = time.time()
        path += datetime.datetime.fromtimestamp(timestamp).strftime('%Y_%m_%d')
        if not(os.path.exists(path)):
            os.makedirs(path)

        path += '//Log_'+datetime.datetime.fromtimestamp(timestamp).strftime('%H_%M_%S')
        if not(os.path.exists(path)):
            os.makedirs(path)
        return path

    def i(self,tag,message):
        output = '{0:.3f} : {1} : Info : {2} : {3}'.format(time.time(), self.__name, tag, message)
        self.__file.write('{0}\n'.format(output))
        self.__file.flush()
        output = Logs.__PURPLE + output + Logs.__END
        print(output)

    def d(self,tag,message):
        output = '{0:.3f} : {1} : Debug : {2} : {3}'.format(time.time(),self.__name,tag,message)
        self.__file.write('{0}\n'.format(output))
        self.__file.flush()
        output = Logs.__BLUE + output + Logs.__END
        print(output)

    def e(self,tag,message):
        output = '{0:.3f} : {1} : Error : {2} : {3}'.format(time.time(),self.__name,tag,message)
        self.__file.write('{0}\n'.format(output))
        self.__file.flush()
        output = Logs.__RED + output + Logs.__END
        print(output)

    def w(self,tag,message):
        output = '{0:.3f} : {1} : Warning : {2} : {3}'.format(time.time(), self.__name, tag, message)
        self.__file.write('{0}\n'.format(output))
        self.__file.flush()
        output = Logs.__CYAN + output + Logs.__END
        print(output)

    def v(self,message):
        self.__file.write('{0}\n'.format(message))
        self.__file.flush()
        print(message)

    def n(self,tag,message):
        output = '{0:.3f} : {1} : Note : {2} : {3}'.format(time.time(), self.__name, tag, message)
        self.__file.write('{0}\n'.format(output))
        self.__file.flush()
        output = Logs.__GREEN + output + Logs.__END
        print(output)
