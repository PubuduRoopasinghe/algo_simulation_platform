import numpy as np
from scipy.signal import butter, filtfilt


class PPGQualityParameter:

    def __init__(self):
        self.__relative_power  = 0.0
        self.__perfusion_index = 0.0

    def process(self, signal=[], sample_rate=25.0):
        filtered_ir = self.butter_bandpass(signal, sample_rate)
        filtered_ir = [float(t) for t in filtered_ir]

        # amplitude spectrum of filtered ir __signal
        fft_ir = self.__one_side_fft(filtered_ir)

        # one sided power spectrum of filtered ir __signal
        power_ir = np.power(fft_ir, 2)

        # Relative power calculation
        self.__relative_power = np.sum(power_ir[int(np.ceil(len(power_ir) / (sample_rate / 2) * 1) - 1):
                                              int(np.ceil(len(power_ir)/(sample_rate/2)*2.25))]) / np.sum(power_ir[0: int(np.ceil(len(power_ir)/(sample_rate/2)*8))]);

        # Perfusion index calculation
        self.__perfusion_index = np.abs((np.max(filtered_ir) - np.min(filtered_ir)) / np.mean(signal) * 100)

    @staticmethod
    def butter_bandpass(ir_frame, sample_rate=25.0, low_cutoff=0.5, high_cutoff=5, order=1):
        low     = low_cutoff/(sample_rate/ 2)
        high    = high_cutoff/(sample_rate/2)
        [b, a]  = butter(order, [low, high], btype='band')
        ir_frame= [float(t) for t in ir_frame]
        return filtfilt(b, a, ir_frame, padtype='odd', padlen=3*(max(len(b), len(a))-1))

    @staticmethod
    def __one_side_fft(filtered_ir_signal):
        next_power_of_two = int(np.ceil(np.log2(len(filtered_ir_signal))))
        n_of_fft = int(np.power(2, next_power_of_two))
        ir_fft   = np.fft.fft(filtered_ir_signal, n_of_fft)
        ir_fft   = 2*np.abs(ir_fft/n_of_fft)
        return ir_fft[0:int((n_of_fft/2)+1)]

    def get_relative_power(self):
        return self.__relative_power

    def get_perfusion_index(self):
        return self.__perfusion_index


