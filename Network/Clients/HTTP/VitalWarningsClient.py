from HttpClient import HttpClient
import json
import time


class VitalWarningsClient(HttpClient):
    def decode_msg(self, msg):
        content = []
        for warning in msg.get():
            content.append(json.loads(warning.get(self.infant_ID)))
            # self.__logs.w('RoomVital', warning.get(self.__infant_id))
        print 'VitalWarningsClient', json.dumps({"warningsList": content})
        return time.time(), json.dumps({"warningsList": content})