# import struct
# from Queue import Empty
# import bluepy.btle as btle
# from Utils.util import Util
# from Bluetooth.token import Token
# from Decode.request import Request
# from Bluetooth.connection import Connection
# from Commands.usercommands import UserCommands
# from Configurations.reconnect import Reconnect
# from Configurations.basestation import BaseStation
#
# class DeviceDelegate(btle.DefaultDelegate):
#
#     def __init__(self, decoder):
#         btle.DefaultDelegate.__init__(self)
#         self.__decoder = decoder
#
#     def handleNotification(self, handle, data):
#         data_array = bytearray()
#         data_array.extend(data)
#         self.__decoder.put([handle, data_array])
#
#
# class Monitor:
#     __BAND_INFO_SERVICE = 0xFFF0
#     __USER_INFO_SERVICE = 0x181C
#
#     __COMMAND_CHARACTERISTIC  = '0000fff1-0000-1000-8000-00805f9b34fb'
#     __RESPONSE_CHARACTERISTIC = '0000fff2-0000-1000-8000-00805f9b34fb'
#     __REPEAT_REQ_UUID         = '0000fff8-0000-1000-8000-00805f9b34fb'
#     __AUTHENTICATION_UUID     = '00002a9a-0000-1000-8000-00805f9b34fb'
#     __WRITE_ATTEMPT_DELAY     = 0.2 # in seconds
#
#     def __init__(self, address, queue_decoder, queue_command, queue_audit, logs):
#         self.__logs          = logs
#         self.__address       = address
#         self.__queue_command = queue_command
#         self.__queue_audit   = queue_audit
#         self.__queue_decoder = queue_decoder
#         self.__command_handle        = None
#         self.__repeat_request_handle = None
#         self.__authentication_handle = None
#         self.__peripheral            = None
#
#     def connect(self):
#         try:
#             self.__logs.i('Connection', 'Attempting {0}'.format(self.__address))
#             self.__peripheral = btle.Peripheral(self.__address)
#             self.__peripheral.setDelegate(DeviceDelegate(self.__queue_decoder))
#             return True
#         except btle.BTLEException as exception:
#             self.__logs.e('Monitor', '{0}'.format(exception))
#             return False
#
#     def run(self):
#         self.start_monitor()
#
#     def start_monitor(self):
#         try:
#             user_info_service = self.__peripheral.getServiceByUUID(Monitor.__USER_INFO_SERVICE)
#             band_info_service = self.__peripheral.getServiceByUUID(Monitor.__BAND_INFO_SERVICE)
#             user_info_characteristics = user_info_service.getCharacteristics()
#             band_info_characteristics = band_info_service.getCharacteristics()
#
#             for user_info_characteristic in user_info_characteristics:
#                 user_info_handle = user_info_characteristic.getHandle()
#                 notify_handle    = (user_info_handle + 1)
#                 if not(str(user_info_characteristic.uuid) == Monitor.__REPEAT_REQ_UUID):
#                     self.__peripheral.writeCharacteristic(notify_handle, struct.pack('<bb', 0x01, 0x00))
#                     self.__logs.v('\t\tNotification Enabled : UUID {0}  (Handle : {1})'.format(user_info_characteristic.uuid, notify_handle))
#                 else:
#                     self.__repeat_request_handle = user_info_handle
#
#             for band_info_characteristic in band_info_characteristics:
#                 band_info_handle = band_info_characteristic.getHandle()
#                 notify_handle    = (band_info_handle + 1)
#                 if str(band_info_characteristic.uuid) == Monitor.__RESPONSE_CHARACTERISTIC:
#                     self.__peripheral.writeCharacteristic(notify_handle, struct.pack('<bb', 0x01, 0x00))
#                     self.__logs.v('\t\tNotification Enabled : UUID {0}  (Handle : {1})'.format(band_info_characteristic.uuid, notify_handle))
#
#                 elif str(band_info_characteristic.uuid) == Monitor.__COMMAND_CHARACTERISTIC:
#                     self.__command_handle = band_info_handle
#
#                 elif str(band_info_characteristic.uuid) == Monitor.__AUTHENTICATION_UUID:
#                     self.__authentication_handle = band_info_handle
#
#             if self.__authentication_handle:
#                 self.authentication()
#
#             if self.__command_handle:
#                 self.synchronize_timestamp()
#                 self.send_commands(UserCommands.request_optical_configuration())
#
#             self.wait_for_notifications()
#
#         except Exception as exception:
#             try:
#                 self.__peripheral.disconnect()
#             except Exception as exception:
#                 return 0
#         finally:
#             pass
#
#     def synchronize_timestamp(self):
#         self.send_commands(UserCommands.update_device_timestamp())
#
#     def authentication(self):
#         temp_token = Token(self.get_mac_address(self.__address))
#         self.send_authentication_token(temp_token.get_token())
#         Util.sleep(self.__WRITE_ATTEMPT_DELAY)
#
#     def send_commands(self, structure):
#         self.__peripheral.writeCharacteristic(self.__command_handle, structure, withResponse=True)
#         Util.sleep(self.__WRITE_ATTEMPT_DELAY)
#
#     def send_request(self, structure):
#         self.__peripheral.writeCharacteristic(self.__repeat_request_handle, structure, withResponse=True)
#
#     def send_authentication_token(self, structure):
#         self.__peripheral.writeCharacteristic(self.__authentication_handle, structure, withResponse=True)
#
#     def wait_for_notifications(self):
#         while True:
#             if self.__peripheral.waitForNotifications(self.__WRITE_ATTEMPT_DELAY):
#                 try:
#                     instance = self.__queue_command.get(False)
#
#                     if isinstance(instance, Request):
#                         self.send_request(instance.get_request())
#                     elif isinstance(instance, Reconnect):
#                         if instance.is_replaced():
#                             BaseStation.set_band_id(instance.get_replacing_mac())
#                         self.disconnect()
#                     else:
#                         self.send_commands(instance)
#                 except Empty:
#                     pass
#
#     def disconnect(self):
#         self.__peripheral.disconnect()
#
#     def __exit__(self, exc_type, exc_val, exc_tb):
#         self.__peripheral.disconnect()
#         self.__queue_audit.put(Connection(self.__address, False))
#
#     @staticmethod
#     def get_mac_address(mac):
#         address = []
#         for x in range(0, len(mac), 3):
#             address.append(int(mac[x] + mac[x + 1], 16))
#         return address
#
