

class PeakDetection:
    """
        This class is defined for peak and valley detection.
        The class must be accessed by the main python program to identify peaks and valleys.
        The class has two getters used for obtaining the peaks and valley positions of a corresonding input array
    """
    __DELTA_Y                    = 10
    __MAXIMUM_HEART_RATE         = 250.0
    __PEAK_COMPARISION_THRESHOLD = 10

    def __init__(self, signal=[], sample_rate=25):
        """
            This is the initializer method of the class.
            Example for creating an object of Peak_and_Valley_detection class is as follows,
             Peak = Peak_and_Valley_detection(ppg_array, __length, __sample_rate)
            @inputs:
            __signal - The input IR/RED array that needs to used for peaks and valley position identification
            __length      - The length of array which need to processed. Most of the __time it will be euqal to length of __signal
            __sample_rate - the sampling_freqeuncy of the input __data array
        """
        self.__length  = len(signal)
        self.__signal  = signal
        self.__peaks   = []
        self.__final_peaks_positions = []
        self.__final_valley_positions = []
        self.__valleys = []
        self.__MINtab  = []
        self.__sample_rate = sample_rate
        self.__delta_x     = int(60 * self.__sample_rate / PeakDetection.__MAXIMUM_HEART_RATE)
        self.__forward_max  = []
        self.__backward_max = []

        if self:
            self.__compute_peaks_and_valleys()

    def __compute_peaks_and_valleys(self):

        # --------------------------------- FORWARD PEAK DETECTION ALGORITHM------------------------------------------------------------
        mn = float("inf")                                                    # value used instead of plus infinity
        mnpos = 0
        mx = float("-inf")                                                   # value used instead of minus infinity
        mxpos = 0
        MN = float("inf")
        MNpos = 0
        look_for_peaks = 0                                                       # for setting whether we are detecting peak or valley first

        if self.__signal[0] < self.__signal[1]:
            look_for_peaks = 1
        elif self.__signal[0] > self.__signal[1]:
            look_for_peaks = 0

        for k in range(0, self.__length):
            this = self.__signal[k]
            if this > mx:
                mx = this
                mxpos = k
            if this < mn:
                mn = this
                mnpos = k

            if look_for_peaks == 0:
                if this < MN:
                    MN = this
                    MNpos = k

            # start of peak detection algo
            if look_for_peaks:
                if this < mx - PeakDetection.__DELTA_Y:
                    row_no_max = len(self.__forward_max)
                    if row_no_max > 0:
                        mxpos_last = self.__forward_max[-1]
                        difference_max = (mxpos - mxpos_last)
                        if difference_max > self.__delta_x:
                            self.__forward_max.append(mxpos)
                            row_no_max = len(self.__forward_max)
                            if row_no_max > 1:
                                self.__MINtab.append(MNpos)
                                MN = this
                                MNpos = k
                            mn = this
                            mnpos = k
                            look_for_peaks = 0
                        else:
                            mxpos = self.__forward_max[-1]
                            mx = self.__signal[mxpos]
                            look_for_peaks = 1
                    else:
                        self.__forward_max.append(mxpos)
                        mn = this
                        mnpos = mxpos
                        MN = this
                        MNpos = k
                        look_for_peaks = 0
            else:
                if this > mn + PeakDetection.__DELTA_Y:
                    row_no_max = len(self.__forward_max)
                    if row_no_max > 0:
                        mxpos_last = self.__forward_max[-1]
                        difference_min = (mnpos - mxpos_last)
                        if difference_min > self.__delta_x:
                            look_for_peaks = 1
                            mx = this
                            mxpos = k
                        else:
                            mn = float("inf")
                            mnpos = 0
                            look_for_peaks = 0
                    else:
                        look_for_peaks = 1
                        mx = this
                        mxpos = k

        ## -----------------------------------------BACKWARD ALGO FOR PEAKS AND VALLEYS------------------------------------------------  ##
        mn = float("inf")                                                    # value used instead of plus infinity
        mnpos = 0
        mx = float("-inf")                                                   # value used instead of minus infinity
        mxpos = 0
        MN = float("inf")
        MNpos = 0
        look_for_peaks = 0                                                               # for setting whether we are detecting peak or valley first

        self.__signal.reverse()
        if self.__signal[0] < self.__signal[1]:
            look_for_peaks = 1
        elif self.__signal[0] > self.__signal[1]:
            look_for_peaks = 0

        for k in range(0, self.__length):
            this = self.__signal[k]
            if this > mx:
                mx = this
                mxpos = k
            if this < mn:
                mn = this
                mnpos = k
            if look_for_peaks == 0:
                if this < MN:
                    MN = this
                    MNpos = k

            # start of peak detection algo
            if look_for_peaks:
                if this < mx - PeakDetection.__DELTA_Y:
                    row_no_max = len(self.__backward_max)
                    if row_no_max > 0:
                        mxpos_last = self.__backward_max[-1]
                        difference_max = (mxpos - mxpos_last)
                        if difference_max > self.__delta_x:
                            self.__backward_max.append(mxpos)
                            row_no_max = len(self.__backward_max)
                            if row_no_max > 1:
                                self.__MINtab.append(self.__length - MNpos + 1)
                                MN = this
                                MNpos = k
                            mn = this
                            mnpos = k
                            look_for_peaks = 0
                        else:
                            mxpos = self.__backward_max[-1]
                            mx = self.__signal[mxpos]
                            look_for_peaks = 1
                    else:
                        self.__backward_max.append(mxpos)
                        mn = this
                        mnpos = mxpos
                        MN = this
                        MNpos = k
                        look_for_peaks = 0
            else:
                if this > mn + PeakDetection.__DELTA_Y:
                    row_no_max = len(self.__backward_max)
                    if row_no_max > 0:
                        mxpos_last = self.__backward_max[-1]
                        difference_min = (mnpos - mxpos_last)
                        if difference_min > self.__delta_x:
                            look_for_peaks = 1
                            mx = this
                            mxpos = k
                        else:
                            mn = float("inf")
                            mnpos = 0
                            look_for_peaks = 0
                    else:
                        look_for_peaks = 1
                        mx = this
                        mxpos = k

        self.__signal.reverse()

        ## _________________________________________END OF FORWARD AND BACKWARD ALGO__________________________________________ ##
        i = 0
        ROW_NO_MAXTAB1 = len(self.__forward_max)
        ROW_NO_MAXTAB2 = len(self.__backward_max)
        self.__backward_max.reverse()
        while (i < ROW_NO_MAXTAB1):
            j = 0
            ith_max_position = self.__forward_max[i]
            while (j < ROW_NO_MAXTAB2):
                if ith_max_position == self.__length - (self.__backward_max[j] + 1):
                    self.__peaks.append(ith_max_position)
                    self.__forward_max[i] = 2 * self.__length
                    self.__backward_max[j] = 2 * self.__length
                    break
                else:
                    if abs(ith_max_position - self.__length + (self.__backward_max[j] + 1)) < PeakDetection.__PEAK_COMPARISION_THRESHOLD:
                        forward_peak_val = self.__signal[ith_max_position]
                        backward_peak_val = self.__signal[self.__length - (self.__backward_max[j] + 1)]
                        if forward_peak_val > backward_peak_val:
                            self.__peaks.append(ith_max_position)
                            self.__forward_max[i] = 2 * self.__length
                            self.__backward_max[j] = 2 * self.__length
                            break
                        elif forward_peak_val <= backward_peak_val:
                            self.__peaks.append(self.__length - (self.__backward_max[j] + 1))
                            self.__forward_max[i] = 2 * self.__length
                            self.__backward_max[j] = 2 * self.__length
                            break
                j = j + 1
            i = i + 1
        ##__________________________________________________END OF COMPARING FORWARD AND BACKWARD PEAKS__________________________________##
        self.__compare_minimum()

        no_of_peaks = len(self.__peaks)
        no_of_valleys = len(self.__valleys)
        self.__final_peaks_positions.append(self.__peaks[0])
        # print no_of_peaks
        for i in range(0, no_of_peaks-2):
            # print i
            pktoval_ratio = self.__signal[self.__peaks[i+1]] - self.__signal[self.__valleys[i]]/self.__signal[self.__peaks[i+2]] - self.__signal[self.__valleys[i+1]]
            if i < no_of_peaks-3:
                if pktoval_ratio*100 > 20:
                    self.__final_peaks_positions.append(self.__peaks[i+1])
                    self.__final_valley_positions.append(self.__valleys[i])
            else:
                if pktoval_ratio*100 > 20:
                    self.__final_peaks_positions.append(self.__peaks[i+1])
                    self.__final_valley_positions.append(self.__valleys[i])
                if 1/pktoval_ratio*100 > 20:
                    self.__final_peaks_positions.append(self.__peaks[i+2])
                    self.__final_valley_positions.append(self.__valleys[i+1])

        # print ("Peaks positions/ final : ", self.__peaks, self.__final_peaks_positions)
        # print ("Valley positions/ final : ", self.__valleys, self.__final_valley_positions)

    def __compare_minimum(self):
        """
            This is a hidden method of this class, Need not be called by an instanc of this class.
            This method is accessed by other methods in thi class
            This method computes the valley position of a given array
        """
        No_of_peaks = len(self.__peaks)
        No_of_valleys = len(self.__MINtab)
        for i in range(0, No_of_peaks - 1):
            this = self.__peaks[i]
            next_val = self.__peaks[i + 1]
            mnpos = 0
            MIN = float("inf")
            count = 0
            for j in range(0, No_of_valleys):
                minpos = self.__MINtab[j]
                minval = self.__signal[minpos]
                if (minpos > this) and (minpos < next_val):
                    if (minval < MIN):
                        mnpos = minpos
                        MIN = minval
                        count = 1
            if count:
                self.__valleys.append(mnpos)

    def get_valleys(self):
        return self.__final_valley_positions

    def get_peaks(self):
        return self.__final_peaks_positions

    def __repr__(self):
        return "Peak_and_Valley_detection(arrayinput,{},{})".format(self.__length, self.__sample_rate)

    def __str__(self):
        return "Peak_and_Valley_detection(arrayinput,arraysize,sampling_frequency)"

    def __nonzero__(self):
        """
            this inbuilt method is modified in order to verify the validity of an object of this class
            The object will be valid only when the following condition is satisfied.
            Returns a bool output.
        """
        return len(self.__signal) >= self.__sample_rate