from Utils.util import Util
import json
from Auditor.Messages.message import Message


class CryNotification(Message):
    __TYPE = "CRY_ALERT"

    def __init__(self,signal=[],priority=2):
        self.__detected_time = Util.get_epoch()
        self.__audio_signal  = signal
        self.__priority      = priority

    def get(self, infant=''):
        return json.dumps({'detectedTime': self.__detected_time,
                           'audioSignal': self.__audio_signal,
                           'infantId': infant,
                           'priority': self.__priority,
                           'warningType': self.__TYPE})