import tornado.web
from wifi import Cell
import json
from Network.Handlers.WifiManager import WifiManager


class WifiHandler(tornado.web.RequestHandler):
    def get(self):
        cell = Cell.all('wlan0')
        networks = []
        for i in cell:
            networks.append(i.ssid)

        nets = {'SSID': networks
                }

        self.write(json.dumps(nets))

    def post(self, *args, **kwargs):
        data = self.request.body

        j = json.loads(data)

        wifi = WifiManager()

        username_ = j['username']
        password_ = j['password']

        wifi.initializeConnection(username_, password_)

        self.write("Success:Wifi_connected - " + username_)