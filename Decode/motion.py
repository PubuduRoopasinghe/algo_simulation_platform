import struct


class Motion:

    __FRAMES_PER_FIFO   = 9
    __ALL_FRAMES_RECEIVED = (1 << __FRAMES_PER_FIFO) - 1
    HEADER_FRAME        = 0
    __SAMPLES_IN_HEADER = 1
    __SAMPLE_RATE       = 25
    __SAMPLE_PER_FRAME  = 3
    __NO_OF_AXIS        = 3

    __ACC_X = 0
    __ACC_Y = 1
    __ACC_Z = 2

    __HEADER_FORMAT = '<BBIHIBhhhB'
    __FRAME_FORMAT  = '<BBhhhhhhhhh'

    def __init__(self):
        self.__fifo_flag    = 0
        self.__fifo_index   = 0
        self.__frame_index  = 0
        self.__acc_range    = None
        self.__timestamp    = None
        self.__duration     = None
        self.__gyro_range   = None
        self.__milliseconds = 0
        self.__multiplier   = self.__get_multiplier()
        self.__acceleration = [[0]*Motion.__SAMPLE_RATE,[0]*Motion.__SAMPLE_RATE,[0]*Motion.__SAMPLE_RATE]

    def decode(self, data):
        self.__fifo_index  = data[0]
        self.__frame_index = data[1]

        if self.__frame_index >= self.__FRAMES_PER_FIFO or self.__frame_index < 0:
            return

        if self.__frame_index == self.HEADER_FRAME:
            header = struct.unpack(self.__HEADER_FORMAT, data)
            self.__timestamp    = header[2]
            self.__milliseconds = self.__get_milliseconds(header[3])
            self.__duration     = header[4]
            self.__acc_range    = header[5]
            self.__multiplier   = self.__get_multiplier(self.__acc_range)

            for index in range(self.__NO_OF_AXIS):
                self.__acceleration[index][0] = self.__convert_si_units(header[6 + index])
            self.__fifo_flag   |= 1

        else:
            frame = struct.unpack(self.__FRAME_FORMAT, data)
            self.__fifo_flag |= (1 << self.__frame_index)
            sample_index = (self.__frame_index - 1)*self.__SAMPLE_PER_FRAME + self.__SAMPLES_IN_HEADER
            frame_loc    = 2 #first two includes fifo index and frame index
            for index in range(Motion.__SAMPLE_PER_FRAME):
                for axis in range(Motion.__NO_OF_AXIS):
                    self.__acceleration[axis][sample_index] = self.__convert_si_units(frame[frame_loc])
                    frame_loc += 1
                sample_index += 1

    def __convert_si_units(self, value):
        return value*self.__multiplier

    def is_complete(self):
        return self.__fifo_flag == self.__ALL_FRAMES_RECEIVED

    def is_frame_of(self, frame):
        return self.__fifo_index == frame[0]

    def get_time(self):
        return self.__timestamp

    """ 
        return the index of the motion fifo 
    """

    def get_index(self):
        return self.__duration

    def get_epoch(self):
        return self.__timestamp + float(self.__milliseconds)/1000

    """
        return the accelerometer values along x axis
    """

    def get_acc_x(self):
        return self.__acceleration[self.__ACC_X]

    """
        return the accelerometer values along y axis
    """

    def get_acc_y(self):
        return self.__acceleration[self.__ACC_Y]

    """
        return the accelerometer values along z axis
    """

    def get_acc_z(self):
        return self.__acceleration[self.__ACC_Z]

    def __str__(self):
        return "{0}:{1}:{2}:{3}:{4}:{5}".format(self.__timestamp,
                                                self.__milliseconds,
                                                self.__duration,
                                                self.get_acc_x(),
                                                self.get_acc_y(),
                                                self.get_acc_z())

    @staticmethod
    def __get_milliseconds(rtc_value):
        return (rtc_value*1000)/(1 << 15)

    @staticmethod
    def __get_multiplier(acc_range=0x03):
        if acc_range == 0x03:
            return 4.0 / (1 << 16)
        elif acc_range == 0x05:
            return 8.0 / (1 << 16)
        elif acc_range == 0x08:
            return 16.0 / (1 << 16)
        else:
            return 32.0 / (1 << 16)