from Utils.util import Util


class Message:

    def __init__(self, root, message, priority=0):
        self.__time = Util.get_epoch()
        self.__message = message
        self.__root    = root
        self.__priority= priority

    def get_event_time(self):
        return self.__time

    def get_event_message(self):
        return self.__message

    def get_root(self):
        return self.__root

    def get_priority(self):
        return self.__priority