from tornado import websocket
from tornado.ioloop import PeriodicCallback
from Auditor.Messages.vitalupdate import VitalUpdate
from Configurations.basestation import BaseStation
import Queue


class VitalSocketHandler(websocket.WebSocketHandler):
    def initialize(self, vital_queue_):
        self.vital_queue = vital_queue_

    def open(self):
        self.write_message("BabyIO Websocket Opened")
        self.callback = PeriodicCallback(self.send_vitals, 2000)
        self.callback.start()

    def send_vitals(self):
        try:
            vital_data = self.vital_queue.get_nowait()

            if not isinstance(vital_data, VitalUpdate):
                return

            vitals = vital_data.get(BaseStation.get_infant_id())

            self.write_message(vitals)

        except Queue.Empty:
            pass
        except websocket.WebSocketClosedError:
            # print 'WebSocket has Closed'
            self.callback.stop()

    def on_message(self, message):
        print message
        self.write_message("Message Received")

    def on_close(self):
        print "BabyIO Websocket closed"

    def check_origin(self, origin):
        # print origin
        return True