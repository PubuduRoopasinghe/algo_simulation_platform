from Utils.util import Util
import json

class VitalParameters:

    def __init__(self, heart_rate=None, oxygen_level=None, temperature=None, wiggling=False):
        self.__time     = Util.get_epoch()
        self.__wiggling = wiggling

        if wiggling:
            self.__heart_rate  = None
            self.__oxygen_level= None
            self.__temperature = None
        else:
            self.__heart_rate  = heart_rate
            self.__oxygen_level= oxygen_level
            self.__temperature = temperature

    def __get_json(self):
        return json.dumps({'detectedTime'     : self.__time,
                           'heartRate'        : self.__heart_rate,
                           'oxygenSaturation' : self.__oxygen_level,
                           'bodyTemperature'  : self.__temperature,
                           'wiggling'         : self.__wiggling})

    def get_heart_rate(self):
        return self.__heart_rate

    def get_temperature(self):
        return self.__temperature

    def get_oxygen_level(self):
        return self.__oxygen_level

    def is_wiggling(self):
        return self.__wiggling
