import json
from Utils.util import Util
from Auditor.Messages.message import Message


class LowBatteryWarning(Message):
    __TYPE = 'BATTERY_LOW'

    def __init__(self, percentage, discharge_time, priority=2):
        self.__detected_time  = Util.get_epoch()
        self.__percentage     = percentage
        self.__discharge_time = discharge_time
        self.__priority       = priority

    def get(self, infant=''):
        return json.dumps({'detectedTime'      : self.__detected_time,
                           'dischargeTime'     : self.__discharge_time,
                           'remainingCapacity' : self.__percentage,
                           'priority'          : self.__priority,
                           'warningType'       : self.__TYPE,
                           'infantId'          : infant})
