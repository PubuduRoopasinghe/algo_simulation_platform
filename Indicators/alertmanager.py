from multiprocessing import Process
# from Indicators.lightcontroller import LightController


class AlertManager(Process):

    __I2C_ADDRESS = 0x32

    def __init__(self, queue, period=0.5):
        super(AlertManager, self).__init__()
        self.__period = period
        self.__queue  = queue

    def run(self):
        with open("LED/Patterns/pattern.hex", "r") as file:
            code = file.read()
            code = code.split()

        with open("LED/Patterns/pattern.txt", "r") as file:
            names = file.read()
            names = names.split()

        patterns = {}

        for pattern in names:
            pattern_split = pattern.split(':')
            patterns[pattern_split[0]] = int(pattern_split[1],16)

        program = []
        for index in range(0, len(code) - 3, 2):
            try:
                hex_num = code[index] + code[index + 1]
                program.append(int(hex_num, 16))
            except:
                pass

        program[:] = program[0:95] # only 96 memory locations available

        # led = LightController(self.__I2C_ADDRESS)
        # led.enable()
        # led.load_program(program)
        #
        # led.set_entry_point(0, patterns['nopattern'])
        # led.set_program_counter(0, patterns['nopattern'])
        #
        # led.set_engine_mode_free(0)
        # led.set_engine_running(0)

        while True:

            notification = self.__queue.get_warning()

            pattern_name = notification.get_pattern()

            # led.set_entry_point(0, patterns[pattern_name])
            # led.set_program_counter(0, patterns[pattern_name])



