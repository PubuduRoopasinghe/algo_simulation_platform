import math
import json
import struct
from Utils.util import Util


class Temperature:

    __HEADER_FORMAT = '<BBIII'
    __Resistance_25 = 100       # kilo ohms
    __RESISTANCE_2  = 100       # kilo ohms
    __B_coefficient = 3900      # B coefficient for thermistor
    __TEMPERATURE_REF = 298.15  # in kelvin
    __VOLTAGE_DRIVE   = 3300.0  # Milli-volts
    __VOLTAGE_REF     = 4300.0  # Milli-volts
    __ADC_RESOLUTION  = 12      # bits per sample
    __KELVIN_SHIFT    = 273.15  # kelvin conversion
    __ADC_MAXIMUM     = (1 << __ADC_RESOLUTION)
    __VOLTAGE_RATIO   = float(__VOLTAGE_REF/(__ADC_MAXIMUM *__VOLTAGE_DRIVE))

    def __init__(self):
        self.__temperature = 0
        self.__timestamp   = 0
        self.__index       = 0
        self.__thermistor  = 0

    def get_timestamp(self):
        return self.__timestamp

    def get_temperature(self):
        return self.__temperature

    def get_index(self):
        return self.__index

    ''' 
        DO NOT MODIFY
        following calculations to extract the temperature values is as per the 
        description provided in the data sheet of the sensor. 
    '''
    @staticmethod
    def __get_temperature(byte_low, byte_high):
        temperature = ((byte_high & 0x7F) + byte_low / 256.0)

        if (byte_high & 0x80) == 0x80:
            return Util.to_fahrenheit(temperature * (-1))
        else:
            return Util.to_fahrenheit(temperature)

    def decode(self, data):
        header = struct.unpack(self.__HEADER_FORMAT, data)
        self.__temperature = self.__get_temperature(byte_low=header[1], byte_high=header[0])
        self.__timestamp   = header[2]
        self.__index       = header[3]
        self.__thermistor  = self.__get_thermistor_temperature(header[4])
        print self.get_json()

    def get_json(self):
        return json.dumps({
            'detectedTime'  : self.__timestamp,
            'temperature': self.__temperature,
            'index'      : self.__index,
            'thermistor' : self.__thermistor,
        })

    def __str__(self):
        return str(self.get_json())

    @staticmethod
    def __get_thermistor_temperature(adc):
        voltage     = (adc*Temperature.__VOLTAGE_RATIO)
        resistance  = float(Temperature.__RESISTANCE_2*voltage/ (1 - voltage))
        temperature = (1 / (math.log(resistance/Temperature.__Resistance_25)/Temperature.__B_coefficient + 1 / Temperature.__TEMPERATURE_REF))
        return Util.to_fahrenheit(temperature - Temperature.__KELVIN_SHIFT)
