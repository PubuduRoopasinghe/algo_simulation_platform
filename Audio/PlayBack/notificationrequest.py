from Utils.util import Util


class NotificationRequest:

    def __init__(self, priority=0, clear=False):
        self.__sound_file = None
        self.__priority = priority
        self.__clear    = clear
        self.__triggered_at = Util.get_time()

    def get_sound_file(self):
        return self.__sound_file

    def get_priority(self):
        return self.__priority

    def is_cleared(self):
        return self.__clear
