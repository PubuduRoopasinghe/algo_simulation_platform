from Decode.datatypes import*


class Optical:

    __FRAMES_PER_FIFO   = 9
    __ALL_FRAMES_RECEIVED = (1 << __FRAMES_PER_FIFO) - 1
    HEADER_FRAME        = 0
    SAMPLE_RATE         = 25
    __SAMPLES_IN_HEADER = 1
    __SAMPLE_PER_FRAME  = 3
    __NO_OF_CHANNELS    = 2
    __DEFAULT_SAMPLE_WIDTH = 18

    def __init__(self):
        self.__fifo_flag    = 0
        self.__fifo_index   = -1
        self.__red_signal   = []
        self.__ir_signal    = []
        self.__offset       = None
        self.__frame_index  = None
        self.__timestamp    = None
        self.__duration     = None
        self.__millis       = None
        self.__bits         = Optical.__DEFAULT_SAMPLE_WIDTH
        self.__rate         = None

    def decode(self, frame):
        self.__fifo_index  = get_unsigned_byte(frame)
        self.__frame_index = get_unsigned_byte(frame)

        if self.__frame_index >= self.__FRAMES_PER_FIFO:
            return

        if self.__frame_index == self.HEADER_FRAME:
            self.__timestamp = get_unsigned_int_32(frame)
            self.__millis    = Optical.__get_milliseconds(get_unsigned_short(frame))
            self.__duration  = get_unsigned_int_32(frame)
            self.__rate      = get_unsigned_byte(frame)
            self.__bits      = get_unsigned_byte(frame)
            self.__fifo_flag |= 1
            self.__red_signal.insert(0, self.__get_sample(frame))
            self.__ir_signal.insert(0, self.__get_sample(frame))

        else:
            self.__fifo_flag |= (1 << self.__frame_index)
            self.__offset = (self.__frame_index - 1) * self.__SAMPLE_PER_FRAME + self.__SAMPLES_IN_HEADER

            for self.sample_index in range(0, self.__SAMPLE_PER_FRAME):
                self.__red_signal.insert(self.sample_index + self.__offset, self.__get_sample(frame))
                self.__ir_signal.insert(self.sample_index + self.__offset, self.__get_sample(frame))

    def get_epoch(self):
        return self.__timestamp + float(self.__millis)/1000

    def is_complete(self):
        return self.__fifo_flag == self.__ALL_FRAMES_RECEIVED

    def is_frame(self, frame):
        return self.__fifo_index == frame[0]

    def get_time(self):
        return self.__timestamp

    def get_index(self):
        return self.__duration

    @staticmethod
    def __get_sample(frame):
        byte_h = get_signed_byte(frame)
        byte_m = get_signed_byte(frame)
        byte_l = get_signed_byte(frame)
        return ((byte_h << 16) & 0xFF0000) | ((byte_m << 8) & 0xFF00) | (byte_l & 0xFF)

    def get_ir_signal(self):
        return self.__ir_signal

    def get_red_signal(self):
        return self.__red_signal

    def get_sample_width(self):
        return self.__bits

    def __str__(self):
        return "{0}:{1}:{2}:{3}:{4}".format(self.__timestamp, self.__millis, self.__duration, self.__ir_signal, self.__red_signal)

    @staticmethod
    def __get_milliseconds(rtc_value):
        return (rtc_value*1000)/(1 << 15)

    def get_maximum_ir_saturation(self):
        return self.__get_maximum(self.__ir_signal) * 100.0 / (1 << self.__bits)

    def get_maximum_red_saturation(self):
        return self.__get_maximum(self.__red_signal) * 100.0 / (1 << self.__bits)

    @staticmethod
    def __get_maximum(input_array):
        max_value = -2147483648
        for sample in input_array:
            max_value = max(max_value, sample)

        return max_value




