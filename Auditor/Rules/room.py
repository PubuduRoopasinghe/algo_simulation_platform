from Utils.util import Util
from Auditor.Messages.outofrangewarning import AbnormalValueWarning


class RoomCondition:

    def __init__(self, name='', condition={}):
        if "range" in condition:
            self.__name     = name
            self.__minimum    = condition["range"]['minimum']
            self.__maximum    = condition["range"]['maximum']
            self.__threshold  = condition["tolerance"]['count']
            self.__window     = condition["tolerance"]['width']
            self.__wait_delay = condition['suspend']
            self.__repeat_delay = condition['repeat']
            self.__priority     = condition['priority']

            self.__event_notified  = False
            self.__detected_normal = True
            self.__next_evaluation = Util.get_time()
            self.__detected_events = []
            self.__abnormality     = None

    def set_notified(self):
        self.__event_notified = True

    def is_notified(self):
        return self.__event_notified

    def is_normal(self):
        return self.__detected_normal

    def get_name(self):
        return self.__name

    def clear_notifications(self):
        self.__event_notified  = False
        self.__detected_normal = True
        self.__abnormality     = None
        self.__next_evaluation = (Util.get_time() + self.__wait_delay)
        del self.__detected_events[:]

    """ 
    returns True if the condition is considered to be normal
    """
    def evaluate(self,value):
        if self.__next_evaluation <= Util.get_time():

            self.__detected_events.append(self.__minimum <= value <= self.__maximum)
            del self.__detected_events[0:-self.__window]

            self.__detected_normal = sum(1 for normal in self.__detected_events if not normal) < self.__threshold

            if not self.__detected_normal:
                self.__abnormality     = AbnormalValueWarning(name=self.__name, value=value, interval=[self.__minimum, self.__maximum])
                self.__next_evaluation = (Util.get_time() + self.__repeat_delay)
                self.__event_notified  = False
            return self.__detected_normal
        return True

    def get_warning(self):
        return self.__abnormality