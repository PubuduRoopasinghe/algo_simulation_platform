from tornado.ioloop import IOLoop, PeriodicCallback
import Queue as queue_exceptions
from Vital.BabyVitals import BabyVitals
from BME680.RoomCondition import RoomCondition


class QueueHandler:
    def __init__(self, queue_network_, queue_vital_, queue_room_condition_, queue_warnings_):
        self.__queue_network = queue_network_
        self.__queue_vital = queue_vital_
        self.__queue_room_condition = queue_room_condition_
        self.__queue_warnings = queue_warnings_

        self.__period = 1000
        PeriodicCallback(self.handler, self.__period).start()

    def handler(self):
        try:
            msg = self.__queue_network.get_nowait()

            if isinstance(msg, BabyVitals):
                self.__queue_vital.put(msg)

            elif isinstance(msg, RoomCondition):
                self.__queue_room_condition.put(msg)

            else:
                print 'Network - QueueHandler - Invalid Msg Type'
        except queue_exceptions.Empty:
            return