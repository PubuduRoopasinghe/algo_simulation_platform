from Decode.Band import *
from Vital.Calibrate import Calibrate
from Decode.opticalresponse import Opticalresponse
from Commands.usercommands import Command


class Response:
    CMD_VERSION     = 0x00
    CMD_TIMESTAMP   = 0x01
    CMD_BLE_CONF    = 0x10
    CMD_OPTICAL_CONF = 0x12
    __ERROR          = 0x80

    def __init__(self, log):
        self.__log       = log
        self.__calibrate = Calibrate.getInstance()
        self.__optical_response = Opticalresponse()

    def decode(self,data):
        command = data[0]
        flag    = data[1]

        if self.__is_error(flag):
            self.__log.i('Response','Error Attempt to read')
            return False
        else:
            header = get_signed_short(data)

            if Command.Command_version == command:
                version = get_signed_int(data)
                self.__log.i('Response','Firmware version :{0}'.format(version))

            elif Command.Command_timestamp == command:
                time = get_signed_int(data)
                self.__log.i('Response','TimeStamp :{0}'.format(time))

            elif Command.Command_ble_configuration == command:
                ble_conf = BLE_Conf(data)

            elif Command.command_optical_configuration == command:
                self.__optical_response = Opticalresponse()
                self.__optical_response.decode_configurations(data)

    @staticmethod
    def __is_error(flag):
        return (flag & Response.__ERROR) == Response.__ERROR

    def get_optical_response_object(self):
        return self.__optical_response