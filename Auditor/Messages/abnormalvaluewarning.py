import json
from Utils.util import Util
from Auditor.Messages.message import Message

class AbnormalValueWarning(Message):
    __TYPE = 'VITAL_ROOM'

    def __init__(self, name, value, interval, priority=1):
        self.__detected_time   = Util.get_epoch()
        self.__parameter_name  = name
        self.__abnormal_value  = value
        self.__normal_interval = interval
        self.__priority        = priority

    def get(self, infant=''):
        return json.dumps({'detectedTime'  : self.__detected_time,
                           'parameterName' : self.__parameter_name,
                           'value'         : self.__abnormal_value,
                           'warningType'   : AbnormalValueWarning.__TYPE,
                           'priority'      : self.__priority,
                           'normalMinimum' : self.__normal_interval[0],
                           'normalMaximum' : self.__normal_interval[1],
                           'infantId'      : infant})

