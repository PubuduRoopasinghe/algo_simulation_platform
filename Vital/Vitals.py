import time

from Decode.Readmotion import Readmotion
from Utils.util import Util
from Utils.logs import Logs
from Decode.motion import Motion
from Decode.sensor import STATES
from Decode.sensor import Sensor
from Decode.optical import Optical
from Decode.Readoptical import Readoptical
from Decode.request import Request
from multiprocessing import Process
from Vital.activitylevel import SMA
from Vital.heartrate import HeartRate
from Vital.Calibrate import Calibrate
from Wearable.max30102 import MAX30102
from Vital.noisefilter import NoiseFilter
from Decode.temperature import Temperature
from Vital.signalquality import SignalQuality
from Vital.peakdetection import PeakDetection
from Commands.usercommands import UserCommands
from Vital.OutlierRemoval import OutlierRemoval
from Configurations.reconnect import Reconnect
from Vital.vitalparameters import VitalParameters
from Decode.opticalresponse import Opticalresponse
from Vital.signalquality import SignalQualityTypes
from Vital.oxygensaturation import OxygenSaturation
from Vital.ppgsignalquality import PPGQualityParameter

"""
    this class is responsible for constructing/updating ppg buffers
    and calculate the heart rate, spo2 level 
"""


class Vitals(Process):
    """
        defining constants (use with ppg data)
    """
    __PPG_FIFO_SIZE = 25  # Input array size for ppg fifo
    __MERGE_SIZE_PPG = __PPG_FIFO_SIZE * 6  # 6*25 6 SECONDS WORTH DATA WILL BE THE MAXIMUM SIZE (Must be greater than PPG_FIFO_SIZE)
    __MIN_SAMPLE_WIDTH = __PPG_FIFO_SIZE * 6

    """
        defining constants (use with mpu data)  
    """
    __MPU_FIFO_SIZE = 25  # Input array size for Activity fifo
    __MERGE_SIZE_MPU = __MPU_FIFO_SIZE * 6

    """
        defining constants (use with auto calibration)  
    """
    __SATURATION_HISTORY = 10

    """
        defining constants (use to decide algorithm flow based on __signal quality)  
    """
    __TIME_HISTORY_BAD_PPG_DISPLACEMENT = 2
    __EXPIRY_PERIOD_DISPLACEMENT = 60

    __EXPIRY_PERIOD = 30
    __MAXIMUM_OPTICAL_HISTORY = 6
    __MOTION_MAX_HISTORY = 7

    """
        initialize the basic variables
    """

    def __init__(self, queue_vitals, queue_command, period, queue_audit, queue_info):
        super(Vitals, self).__init__()
        self.__LOG_TAG = self.__class__.__name__

        self.__period = period
        self.__queue_vitals = queue_vitals
        self.__queue_audit = queue_audit
        self.__queue_command = queue_command
        self.__queue_info = queue_info

        self.__baby_vitals = VitalParameters()
        self.__heart_rate_outlier_filter = OutlierRemoval()
        self.__spo2_outlier_filter = OutlierRemoval()
        self.__temperature_outlier_filter = OutlierRemoval()
        self.__signal_quality = PPGQualityParameter()
        self.__activity_level = SMA()
        self.__auto_calibrate = Calibrate(self.__queue_command)
        self.__optical_config = MAX30102.getInstance()
        self.__logs = Logs('Vitals')
        self.__sensor = None

        self.__optical_history = []
        self.__motion_history = []
        self.__temperature = None

        self.__time = 0
        self.__last_update = 0
        self.__index = 0
        self.__ir_signal = []
        self.__ir_signal_timestamps = []
        self.__ir_valley_timestamps_history = []
        self.__bad_ppg_quality_time_stamps = 0
        self.__time_difference_between_ir_packets = 974
        self.__red_signal = []
        self.__ir_saturation_array = []
        self.__red_saturation_array = []

        self.__time_diffs_with_curent_time = []
        self.__time_diff_with_curent_time = 0

        self.__index_motion = 0
        self.__time_motion = 0
        self.__acc_x = []
        self.__acc_y = []
        self.__acc_z = []

        self.__time_array = []

        self.__bad_quality_due_to_displacement = bool(False)
        self.__bad_ppg_waiting_expired = bool(False)
        self.__last_update_bad_due_to_displacement = 0

        # file_paths = ['F:\\Synergen_Work\\Work 7_Simulation platform\\Collected_Data\\HR_Test_Data_Rasa']
        # for path in file_paths:
        #     print('------------------------------------------------------------------------------------')
        #     print('Directory Path : {0}\n'.format(path))
        #     self.__empatica_hr = self.__read_empatica_hr(path + '\\HR.csv')
        #
        # self.__empatica_data =[[0]*(len(self.__empatica_hr)-2), [0]*(len(self.__empatica_hr)-2)]
        # self.__empatica_hr_start_time = self.__empatica_hr[0]
        # for index in range(0, len(self.__empatica_hr)-2):
        #     self.__empatica_data[0][index] = self.__empatica_hr_start_time
        #     self.__empatica_data[1][index] = self.__empatica_hr[index+2]
        #     self.__empatica_hr_start_time = self.__empatica_hr_start_time+1

    def __send_to_auditor(self, vital):
        self.__queue_audit.put(vital)

    def __clear_buffers(self):
        del self.__optical_history[:]
        del self.__motion_history[:]

    def run(self):
        executed_at = Util.get_current_time() - 2

        while True:
            instance = self.__queue_vitals.get()

            if isinstance(instance, Optical):
                self.__construct_optical_signal(optical=instance)
                # if len(self.__ir_signal) >= Vitals.__MIN_SAMPLE_WIDTH:
                #     self.__process()

            elif isinstance(instance, Readoptical):
                # print "Readoptical time, epoch, current time : ", instance.get_time(),instance.get_epoch(), time.time()
                # self.__construct_optical_signal(optical=instance)
                # if Util.time_elapsed(executed_at) >= 2:
                #     self.__process()
                #     executed_at = Util.get_current_time()

                self.__construct_optical_signal(optical=instance)
                if len(self.__ir_signal) >= Vitals.__MIN_SAMPLE_WIDTH:
                    self.__process()

            elif isinstance(instance, Readmotion):
                # print "Readmotion time, epoch, current time : ", instance.get_time(), instance.get_epoch(), time.time()
                self.__construct_motion_signal(motion=instance)

            elif isinstance(instance, Motion):
                self.__construct_motion_signal(motion=instance)

            elif isinstance(instance, Temperature):
                self.__temperature = instance

            elif isinstance(instance, Sensor):
                self.__process_sensor_information(sensor=instance)

            elif isinstance(instance, Opticalresponse):
                self.__optical_config = MAX30102.getInstance()
                self.__optical_config.set_parameters(instance.get_ppg_rate(), instance.get_ppg_pwm(),
                                                     instance.get_ppg_range(),
                                                     instance.get_ir_current(), instance.get_red_current())
                self.__auto_calibrate.set_currents(self.__optical_config.get_ir_current(),
                                                   self.__optical_config.get_red_current())

            else:
                print "Start Times -------------> ", instance, Util.Current_time() - instance
                self.__time_diffs_with_curent_time.append(Util.Current_time() - instance)
                self.__time_diff_with_curent_time = max(self.__time_diffs_with_curent_time)
                print "Selected time ----------> ", self.__time_diff_with_curent_time

            # elif isinstance(instance, Readoptical):
            #     print instance.get_index()

    def __process_sensor_information(self, sensor):
        if sensor:
            if not sensor.get_operational_state() == STATES.NORMAL_OPERATION_STATE:
                self.__clear_buffers()
                self.__activity_level.reset()
            self.__sensor = sensor

    def __construct_optical_signal(self, optical):
        if optical.is_complete():
            # if optical.get_index() <= 1 :
            #     del self.__optical_history[:]
            #     self.__optical_history.append(optical)
            # else:
            #     self.__optical_history.append(optical)
            self.__optical_history.append(optical)
            # self.__update_saturation_values(optical)
            self.__optical_history = self.__filter_optical_data(self.__optical_history)
            if not self.__check_optical_continuity(self.__optical_history):
                return False

            self.__get_time_difference_between_optical_packets(self.__optical_history)
            self.__construct_ppg_array(self.__optical_history)
            return True
        return False

    def __get_time_difference_between_optical_packets(self, optical_history):
        if len(optical_history) > 1:
            self.__time_difference_between_ir_packets = optical_history[1].get_epoch() - optical_history[0].get_epoch()
        else:
            self.__time_difference_between_ir_packets = 0.974

    def __construct_ppg_array(self, input_ppg):
        # self.__index = input_ppg[0].get_index()
        self.__ir_signal_timestamps = []
        for packet in input_ppg:
            packet_time = packet.get_epoch()
            for x in range(0, 25):
                self.__ir_signal_timestamps.append(packet_time)
                packet_time = packet_time + round(self.__time_difference_between_ir_packets / 25.0, 3)
            self.__update_ppg_buffer(packet)

    def __update_ppg_buffer(self, ppg):
        delta = ppg.get_index() - self.__index
        if ppg.is_complete() and delta == 1:
            self.__merge(ppg.get_ir_signal(), ppg.get_red_signal())
        else:
            self.__ir_signal = ppg.get_ir_signal()
            self.__red_signal = ppg.get_red_signal()
        self.__time = ppg.get_time()
        self.__index = ppg.get_index()

    def __filter_optical_data(self, optical_input=[]):
        optical_output = []
        current_time = Util.get_time()
        start_time = int(current_time - self.__EXPIRY_PERIOD)
        if len(optical_input):
            sorted_input = sorted(optical_input, key=lambda x: x.get_index())
            last_index = sorted_input[0].get_index() - 1

            for optical in sorted_input:
                if optical.get_time() >= start_time and last_index != optical.get_index():
                    optical_output.append(optical)
                    if len(optical_output) > self.__MAXIMUM_OPTICAL_HISTORY:
                        optical_output.remove(optical_output[0])
                last_index = optical.get_index()
        index_list = []
        for element in optical_output:
            index_list.append(element.get_index())
        # print index_list
        return optical_output

    def __check_optical_continuity(self, signal=[]):
        if not len(signal):
            return False

        index = signal[0].get_index() - 1
        for optical in signal:
            delta = optical.get_index() - index
            if not delta == 1:
                if delta > 1:
                    self.__queue_command.put(Request(optical_index=(index + 1), optical_period=(delta - 1)))
                    return False
            index = optical.get_index()
        return True

    def __check_motion_continuity(self, signal=[]):
        if not len(signal):
            return False

        index = signal[0].get_index() - 1
        for motion in signal:
            delta = motion.get_index() - index
            if not delta == 1:
                if delta > 1:
                    self.__queue_command.put(Request(motion_index=(index + 1), motion_period=(delta - 1)))
                    return False
            index = motion.get_index()
        return True

    def __merge_motion(self, acc_x, acc_y, acc_z):
        # print "Length acc_x (inside merge_motion) : ", len(acc_x)
        if len(acc_x) == self.__MPU_FIFO_SIZE:
            if len(self.__acc_x) >= self.__MERGE_SIZE_MPU:
                self.__acc_x[0:self.__MPU_FIFO_SIZE] = []
                self.__acc_y[0:self.__MPU_FIFO_SIZE] = []
                self.__acc_z[0:self.__MPU_FIFO_SIZE] = []

            self.__acc_x += acc_x
            self.__acc_y += acc_y
            self.__acc_z += acc_z

        else:
            self.__acc_x = []
            self.__acc_y = []
            self.__acc_z = []

    @staticmethod
    def __celsius_to_fahrenheit(value):
        return int((value * 9.0 / 5.0 + 32.0) * 100) / 100.0

    def __construct_motion_signal(self, motion):
        # if motion.get_index() <= 1:
        #     del self.__motion_history[:]
        #     self.__motion_history.append(motion)
        # else:
        #     self.__motion_history.append(motion)
        self.__motion_history.append(motion)
        self.__motion_history = self.__filter_motion_data(self.__motion_history)

        if self.__check_motion_continuity(self.__motion_history):
            # self.__synchronize_motion(self.__optical_history, self.__motion_history)
            self.__construct_motion_array(self.__motion_history)

    def __construct_motion_array(self, input_motion):
        for packet in input_motion:
            self.__update_motion_buffer(packet)

    def __update_motion_buffer(self, motion):
        delta_motion = motion.get_index() - self.__index_motion
        if motion.is_complete() and delta_motion == 1:
            self.__merge_motion(motion.get_acc_x(), motion.get_acc_y(), motion.get_acc_z())
        else:
            self.__acc_x = motion.get_acc_x()
            self.__acc_y = motion.get_acc_y()
            self.__acc_z = motion.get_acc_z()

        self.__time_motion = motion.get_time()
        self.__index_motion = motion.get_index()

    def __filter_motion_data(self, motion_input=[]):
        motion_output = []
        current_time = Util.get_time()  # current epoch __time
        start_time = int(current_time - self.__EXPIRY_PERIOD)

        if len(motion_input):
            sorted_input = sorted(motion_input, key=lambda x: x.get_index())
            last_index = sorted_input[0].get_index() - 1

            for motion in sorted_input:
                if motion.get_time() >= start_time and last_index != motion.get_index():
                    motion_output.append(motion)
                    if len(motion_output) > self.__MOTION_MAX_HISTORY:
                        motion_output.remove(motion_output[0])
                last_index = motion.get_index()

        return motion_output

    def __synchronize_motion(self, optical_output, motion_output):
        synced_motion = motion_output[0]
        synced_motion_output = []
        min_time_stamp_gap = 0
        motion_input_index_list = []
        for packet in motion_output:
            motion_input_index_list.append(packet.get_index())
        # print('------------------------------------------------------------------------------------\n\n')
        for optical in optical_output:
            is_first = True
            optical_time_stamp = optical.get_epoch()
            for motion in motion_output:
                new_time_stamp_gap = abs(optical_time_stamp - motion.get_epoch())
                if is_first:
                    is_first = False
                    min_time_stamp_gap = new_time_stamp_gap
                    synced_motion = motion
                else:
                    if new_time_stamp_gap <= min_time_stamp_gap:
                        min_time_stamp_gap = new_time_stamp_gap
                        synced_motion = motion
            # print('optical __time : {0:.3f} motion Time : {1:.3f}'.format(optical_time_stamp,synced_motion.getEpoch()))
            # if len(synced_motion_output):
            #     if synced_motion_output[-1].getIndex() == synced_motion.getIndex():
            #         print("---------------------+++++++++-----------------------\t {0:.3f}".format(__time.__time()))
            synced_motion_output.append(synced_motion)
        output_index_list = []
        time_stamp_list = []
        for packet in synced_motion_output:
            output_index_list.append(packet.get_index())
            time_stamp_list.append(packet.get_epoch())
        # print ("Motion input index list : ", motion_input_index_list)
        # print ("Synced Motion Index List : ", output_index_list)
        # print ("Synced Motion timestamp List : ", time_stamp_list)
        self.__construct_motion_array(synced_motion_output)

    # ------------------------------------------------------------------------------------------------------------------

    """
        construct the saturation arrays for auto calibration
    """

    def __update_saturation_values(self, optical):
        ir_saturation = optical.get_maximum_ir_saturation()
        red_saturation = optical.get_maximum_red_saturation()

        self.__ir_saturation_array.append(ir_saturation)
        self.__red_saturation_array.append(red_saturation)

        if len(self.__ir_saturation_array) > self.__SATURATION_HISTORY:
            del self.__ir_saturation_array[0]
            del self.__red_saturation_array[0]

        self.__auto_calibrate.set_saturation(self.__ir_saturation_array, self.__red_saturation_array)

    """
        construct/update the buffers adding new samples and if require the oldest __data will be replaces  
    """

    def __merge(self, ir_signal, red_signal):
        if len(ir_signal) == self.__PPG_FIFO_SIZE and len(red_signal) == self.__PPG_FIFO_SIZE:
            if len(self.__ir_signal) >= self.__MERGE_SIZE_PPG:
                del self.__ir_signal[0:self.__PPG_FIFO_SIZE]
                del self.__red_signal[0:self.__PPG_FIFO_SIZE]
                self.__ir_signal += ir_signal
                self.__red_signal += red_signal
            else:
                self.__ir_signal += ir_signal
                self.__red_signal += red_signal
        else:
            self.__ir_signal = []
            self.__ir_signal_timestamps = []
            self.__red_signal = []

    """
        trigger the algorithms and calculate the values
    """

    # def __process(self):
    #     if self.__sensor and self.__sensor.get_operational_state() == STATES.NORMAL_OPERATION_STATE:
    #         if not (len(self.__acc_x) == self.__MERGE_SIZE_MPU and len(self.__ir_signal) == self.__MERGE_SIZE_PPG):
    #             return False
    #
    #         self.__signal_quality.process(self.__ir_signal)
    #         self.__activity_level.process(self.__acc_x, self.__acc_y, self.__acc_z)
    #
    #         signal_quality_index = SignalQuality.decision_quality(relative_power   = self.__signal_quality.get_relative_power(),
    #                                                               perfusion_index  = self.__signal_quality.get_perfusion_index(),
    #                                                               activity_intesity= self.__activity_level.get_activity_intensity())
    #
    #         self.__bad_ppg_quality_time_stamps = signal_quality_index[1]
    #
    #         if signal_quality_index[0] == SignalQualityTypes.GOOD_QUALITY_PPG \
    #                 or signal_quality_index[0] == SignalQualityTypes.GOOD_QUALITY_HIGH_MOTION:
    #             self.__execute_algorithm()
    #             self.__bad_quality_due_to_displacement = False
    #
    #         elif signal_quality_index[0] == SignalQualityTypes.BAD_QUALITY_DUE_TO_RELATIVE_MOTION \
    #             or signal_quality_index[0] == SignalQualityTypes.BAD_QUALITY_PPG_DUE_TO_MOTION:
    #             self.__bad_quality_due_to_displacement = False
    #             self.__send_to_auditor(VitalParameters(wiggling=True))
    #
    #         elif signal_quality_index[0] == SignalQualityTypes.BAD_QUALITY_DUE_TO_DISPLACEMENT:
    #             self.__send_to_auditor(VitalParameters(wiggling=True))
    #             current_time = Util.get_time()
    #
    #             self.__time_array.append(current_time)
    #             if len(self.__time_array) > self.__TIME_HISTORY_BAD_PPG_DISPLACEMENT:
    #                 del self.__time_array[0]
    #
    #             if not self.__bad_quality_due_to_displacement or (self.__time_array[1] - self.__time_array[0] > 1):
    #                 self.__bad_quality_due_to_displacement = True
    #                 self.__last_update_bad_due_to_displacement = current_time
    #
    #             self.__bad_ppg_waiting_expired = Util.time_elapsed(self.__last_update_bad_due_to_displacement) > self.__EXPIRY_PERIOD_DISPLACEMENT
    #
    #             if self.__bad_ppg_waiting_expired:
    #                 self.__last_update_bad_due_to_displacement = current_time
    #                 UserCommands.trigger_motion_detection_mode()

    def __process(self):
        if True:
            if not (len(self.__acc_x) == self.__MERGE_SIZE_MPU and len(self.__ir_signal) == self.__MERGE_SIZE_PPG):
                return False
            self.__signal_quality.process(self.__ir_signal)
            self.__activity_level.process(self.__acc_x, self.__acc_y, self.__acc_z)

            signal_quality_index = SignalQuality.decision_quality(
                relative_power=self.__signal_quality.get_relative_power(),
                perfusion_index=self.__signal_quality.get_perfusion_index(),
                activity_intesity=self.__activity_level.get_activity_intensity())

            self.__bad_ppg_quality_time_stamps = signal_quality_index[1]

            if signal_quality_index[0] == SignalQualityTypes.GOOD_QUALITY_PPG \
                    or signal_quality_index[0] == SignalQualityTypes.GOOD_QUALITY_HIGH_MOTION:
                self.__execute_algorithm()
                self.__bad_quality_due_to_displacement = False

            elif signal_quality_index[0] == SignalQualityTypes.BAD_QUALITY_DUE_TO_RELATIVE_MOTION \
                    or signal_quality_index[0] == SignalQualityTypes.BAD_QUALITY_PPG_DUE_TO_MOTION:
                self.__bad_quality_due_to_displacement = False
                self.__send_to_auditor(VitalParameters(wiggling=True))

            elif signal_quality_index[0] == SignalQualityTypes.BAD_QUALITY_DUE_TO_DISPLACEMENT:
                self.__send_to_auditor(VitalParameters(wiggling=True))
                current_time = Util.get_time()

                self.__time_array.append(current_time)
                if len(self.__time_array) > self.__TIME_HISTORY_BAD_PPG_DISPLACEMENT:
                    del self.__time_array[0]

                if not self.__bad_quality_due_to_displacement or (self.__time_array[1] - self.__time_array[0] > 1):
                    self.__bad_quality_due_to_displacement = True
                    self.__last_update_bad_due_to_displacement = current_time

                self.__bad_ppg_waiting_expired = Util.time_elapsed(
                    self.__last_update_bad_due_to_displacement) > self.__EXPIRY_PERIOD_DISPLACEMENT

                if self.__bad_ppg_waiting_expired:
                    self.__last_update_bad_due_to_displacement = current_time
                    UserCommands.trigger_motion_detection_mode()

    def __construct_ir_valley_time_stamps_history(self, input, timeStamp):
        if len(self.__ir_valley_timestamps_history) == 0:
            # print ("============================ came bcs of size is zero ==================================")
            # print len(input), len(timeStamp)
            for valleyIndex in input:
                self.__ir_valley_timestamps_history.append(timeStamp[valleyIndex])
                # self.__logs.i("valley Timestamp", timeStamp[valleyIndex])
            # print self.__ir_valley_timestamps_history
            return self.__ir_valley_timestamps_history
        else:
            # print ("============================ came inside else ==================================")
            lastValleyTimestamp = self.__ir_valley_timestamps_history[-1]
            # print "lastValleyTimestamp : ", lastValleyTimestamp
            # print self.__time_difference_between_ir_packets
            # print round(self.__time_difference_between_ir_packets/25.0 * 3, 3)
            for valleyIndex in input:
                newStartTimeStamp = timeStamp[valleyIndex]
                if newStartTimeStamp >= (
                        lastValleyTimestamp + round(self.__time_difference_between_ir_packets / 25.0 * 3, 3)):
                    # print ("============================ added to TimeStamp history ==================================")
                    self.__ir_valley_timestamps_history.append(newStartTimeStamp)
                    # self.__logs.i("valley Timestamp", newStartTimeStamp)
            if self.__ir_valley_timestamps_history[-1] - self.__ir_valley_timestamps_history[0] > 60:
                # print ("============================ valleys with more than 60s ==================================")
                for index in range(0, len(self.__ir_valley_timestamps_history)):
                    # diff = self.__ir_valley_timestamps_history[-1] - self.__ir_valley_timestamps_history[index]
                    # self.__logs.e("diff", diff)
                    if self.__ir_valley_timestamps_history[-1] - self.__ir_valley_timestamps_history[index] <= 60.078:
                        self.__ir_valley_timestamps_history = self.__ir_valley_timestamps_history[
                                                              index: len(self.__ir_valley_timestamps_history)]
                        # print self.__ir_valley_timestamps_history
                        return self.__ir_valley_timestamps_history
                # print self.__ir_valley_timestamps_history
                return self.__ir_valley_timestamps_history
            else:
                # print self.__ir_valley_timestamps_history
                return self.__ir_valley_timestamps_history

    def __execute_algorithm(self):
        # print "came inside __execute_algorithm"
        if len(self.__ir_signal) >= Vitals.__MIN_SAMPLE_WIDTH:
            # print "run peak detection"

            filtered_ir = NoiseFilter.apply_moving_average(
                PPGQualityParameter.butter_bandpass(self.__ir_signal, sample_rate=25.0, low_cutoff=0.5, high_cutoff=5,
                                                    order=1))
            filtered_ir_for_spo2 = NoiseFilter.apply_moving_average(self.__ir_signal)
            filtered_red = NoiseFilter.apply_moving_average(self.__red_signal)
            peak_detection = PeakDetection(filtered_ir)
            if peak_detection:
                # print "peak detected"

                valleys_ir = peak_detection.get_valleys()
                peaks_ir = peak_detection.get_peaks()

                # print ("valleys_ir : ", valleys_ir)
                array = self.__construct_ir_valley_time_stamps_history(valleys_ir, self.__ir_signal_timestamps)
                # self.__logs.e("array",array)
                # print self.__ir_signal_timestamps
                # print len(array)
                # for i in range(0, len(array)-1):
                #     diff = array[i+1]-array[i]
                #     if diff > 3:
                #         print "**********************************"
                #         print diff
                #         print i

                #         print i
                #         self.__logs.e("missed a valley", diff)
                #         self.__logs.e("ssss",i)

                oxygen_level = OxygenSaturation(peaks=peaks_ir, valleys=valleys_ir, ir_signal=filtered_ir_for_spo2,
                                                red_signal=filtered_red)

                heart_rate = HeartRate(self.__ir_valley_timestamps_history, self.__bad_ppg_quality_time_stamps)

                heart_rate.process()
                oxygen_level.process()

                temperature_value = None
                oxygen_saturation_value = None
                heart_rate_value = None

                if self.__temperature:
                    temperature_value = self.__temperature.get_temperature()

                if oxygen_level:
                    oxygen_saturation_value = oxygen_level.get_value()

                if heart_rate:
                    heart_rate_value = heart_rate.get_heart_rate()
                    # print heart_rate_value
                    # self.__logs.i("HR_vitals", heart_rate_value)
                    # print "Heart rate from base station: ", Util.exact_time(self.__time_diff_with_curent_time),"|", heart_rate_value
                    # print Util.exact_time(self.__time_diff_with_curent_time), heart_rate_value
                    # self.__find_matching_empatica_hr(self.__empatica_data, heart_rate_value, Util.exact_time())

                self.__send_to_auditor(VitalParameters(heart_rate_value, oxygen_saturation_value, temperature_value))
                return True
        return False

    def __str__(self):
        return str(self.__baby_vitals)

    @staticmethod
    def __round_off(value):
        return int(value * 100) / 100.0

    @staticmethod
    def __read_empatica_hr(file_name):
        file = open(file_name, 'r')
        buffers = []
        for line in file:
            # motion = Readmotion(line)
            # if motion.is_valid():
            #     buffers.append(motion)
            buffers.append(float(line))
        return buffers

    @staticmethod
    def __find_matching_empatica_hr(empatica_data, hr_value, timestamp):
        for index in range(0, len(empatica_data[0])):
            timestamp_error = abs(empatica_data[0][index] - timestamp)
            if timestamp_error < 0.5:
                hr_error = empatica_data[1][index] - hr_value
                # print "Heart rate from empatica      :",empatica_data[0][index], "|", empatica_data[1][index]
                print timestamp, "|", hr_value, "|", empatica_data[0][index], "|", empatica_data[1][index], "|", hr_error
                # print abs(hr_error)
                # if abs(hr_error) > 1:
                #     print "-------------------- more than 1 digit error------------------- "