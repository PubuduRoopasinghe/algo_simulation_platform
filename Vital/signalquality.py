import time
import enum as enum
from Utils import util
from Utils.util import Util


class SignalQualityTypes(enum.Enum):
    """
        defining constants (use with __signal quality evaluation)
    """
    GOOD_QUALITY_PPG                    = 0x01
    BAD_QUALITY_PPG_DUE_TO_MOTION       = 0x02
    BAD_QUALITY_DUE_TO_DISPLACEMENT     = 0x03
    BAD_QUALITY_DUE_TO_RELATIVE_MOTION  = 0x04
    GOOD_QUALITY_HIGH_MOTION            = 0x05


class SignalQuality:

    __bad_ppg_time_duration = []
    __is_decision_good_ppg = False
    __bad_quality_start_time = 0
    __SMA_MEAN_THRESHOLD_1 = 0.0
    __SMA_MEAN_THRESHOLD_2 = 5.0
    __BABY_WIGGLING_LEVEL  = 2.0
    __PERFUSION_THRESHOLD_1 = 0.05
    __PERFUSION_THRESHOLD_2 = 4.2
    __RP_THRESHOLD          = 0.56

    def __init__(self):
        self.__is_decision_good_ppg = False
        self.__bad_quality_start_time = 0
        self.__bad_ppg_time_duration = []
    @staticmethod
    def decision_quality(relative_power, perfusion_index, activity_intesity):

        # if SignalQuality.__PERFUSION_THRESHOLD_2 > perfusion_index > SignalQuality.__PERFUSION_THRESHOLD_1 \
        #         and relative_power >= SignalQuality.__RP_THRESHOLD:
        if SignalQuality.__PERFUSION_THRESHOLD_2 > perfusion_index > SignalQuality.__PERFUSION_THRESHOLD_1:
            if not SignalQuality.__is_decision_good_ppg:
                SignalQuality.__is_decision_good_ppg = True
                SignalQuality.__bad_ppg_time_duration.append(Util.get_epoch())
                if len(SignalQuality.__bad_ppg_time_duration) == 2:
                    SignalQuality._time_gap_bad_quality = SignalQuality.__bad_ppg_time_duration[1] - SignalQuality.__bad_ppg_time_duration[0]
                    SignalQuality.__bad_quality_start_time = SignalQuality.__bad_ppg_time_duration[0]
                    # HeartRate.set_bad_ppg_quality_time_stamps(SignalQuality.__bad_ppg_time_duration[0])
                    print ("In SQ: ",SignalQuality.__bad_ppg_time_duration, SignalQuality.__bad_quality_start_time)
            if activity_intesity >= SignalQuality.__BABY_WIGGLING_LEVEL:
                return SignalQualityTypes.GOOD_QUALITY_HIGH_MOTION, SignalQuality.__bad_quality_start_time          # good ppg quality when the baby is wiggling

        else:
            if SignalQuality.__is_decision_good_ppg:
                SignalQuality.__bad_ppg_time_duration = []
                SignalQuality.__is_decision_good_ppg = False
                SignalQuality.__bad_ppg_time_duration.append(Util.get_epoch())

            if perfusion_index <= SignalQuality.__PERFUSION_THRESHOLD_1:
                return SignalQualityTypes.BAD_QUALITY_DUE_TO_DISPLACEMENT, SignalQuality.__bad_quality_start_time     # bad ppg quality due to sensor displacement

            elif SignalQuality.__SMA_MEAN_THRESHOLD_2 > activity_intesity > SignalQuality.__SMA_MEAN_THRESHOLD_1:
                return SignalQualityTypes.BAD_QUALITY_DUE_TO_RELATIVE_MOTION, SignalQuality.__bad_quality_start_time     # bad ppg quality due to relative motion

            elif activity_intesity >= SignalQuality.__SMA_MEAN_THRESHOLD_2:
                return SignalQualityTypes.BAD_QUALITY_PPG_DUE_TO_MOTION, SignalQuality.__bad_quality_start_time     # bad ppg quality due to high motion

        return SignalQualityTypes.GOOD_QUALITY_PPG, SignalQuality.__bad_quality_start_time

    def get_bad_ppg_quality_time_stamp(self):
        return self.__bad_quality_start_time
