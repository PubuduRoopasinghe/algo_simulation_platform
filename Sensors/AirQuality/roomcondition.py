from Utils.util import Util
import json


class RoomCondition:

    def __init__(self,time=Util.get_epoch(),temperature=0,pressure=1,humidity=100,air_quality=0):
        self.__humidity    = humidity
        self.__air_quality = air_quality
        self.__temperature = temperature
        self.__pressure    = pressure
        self.__timestamp   = time

    def get_timestamp(self):
        return self.__timestamp

    def get_humidity(self):
        return self.__humidity

    def get_air_quality(self):
        return self.__air_quality

    def get_temperature(self):
        return self.__temperature

    def get_pressure(self):
        return self.__pressure

    def get_json(self):
        return json.dumps({
            'Timestamp'  : self.__timestamp,
            'Temperature': self.__temperature,
            'Air Quality': self.__air_quality,
            'Humidity'   : self.__humidity,
            'Pressure'   : self.__pressure
        })

    def __str__(self):
        return str(self.get_json())
