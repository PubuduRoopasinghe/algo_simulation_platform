# import struct
# from Bluetooth.encrypt import Encrypt
#
#
# class Token:
#
#     __HEADER_FORMAT = '<HHHI'
#     __Auth_KEY   = [0x462D4A40, 0x4E635266, 0x556A586E, 0x32723575]
#     __NONCE      = 0xFA329CDA
#     __KEY        = [0xE4A5, 0x7CA3, 0xD4B9]
#     __MAC_LENGTH = 6
#     __KEY_LENGTH = 3
#
#     def __init__(self, mac=[], nonce=__NONCE, keys=__KEY):
#         self.__address = mac
#         self.__nonce   = nonce
#         self.__keys    = keys
#         self.__token   = []
#         if len(self.__address) == Token.__MAC_LENGTH and len(self.__keys) == Token.__KEY_LENGTH:
#             header = struct.pack(self.__HEADER_FORMAT, self.__keys[0], self.__keys[1],self.__keys[2], self.__nonce)
#             self.__token = bytearray()
#             self.__token.extend(header)
#             self.__token.extend(self.__address)
#             self.__token = Encrypt.encrypt(Token.__Auth_KEY, self.__token)
#
#     def get_token(self):
#         return self.__token
#
