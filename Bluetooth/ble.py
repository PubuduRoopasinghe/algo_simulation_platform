# import os
# from Utils.logs import Logs
# from Utils.util import Util
# from Bluetooth.scan import Scan
# from multiprocessing import Process
# from Bluetooth.monitor import Monitor
# from Configurations.basestation import BaseStation
# from Bluetooth.connection import Connection,BLE_STATE
#
#
# class BLE(Process):
#
#     __RESTART_BLE_INTERFACE     = ['sudo systemctl enable bb-wl18xx-bluetooth.service','sudo systemctl start bb-wl18xx-bluetooth.service']
#     __CHECK_BLE_INTERFACE_STATE = 'hcitool dev'
#     __DEFAULT_SCAN_PERIOD       = 5
#     __DEFAULT_RECONNECT_ATTEMPTS= 5
#     __DEFAULT_INTERFACE_RECHECK = 10
#
#     def __init__(self, queue_decoder, queue_command,queue_audit):
#         super(BLE, self).__init__()
#         self.__queue_decoder = queue_decoder
#         self.__queue_command = queue_command
#         self.__queue_audit   = queue_audit
#         self.__logs          = Logs('BLE')
#
#     @staticmethod
#     def restart_ble_service():
#         for command in BLE.__RESTART_BLE_INTERFACE:
#             os.popen(command).read()
#             Util.sleep(1)
#         return BLE.__check_ble_interface()
#
#     @staticmethod
#     def __check_ble_interface():
#         return 'hci0' in os.popen(BLE.__CHECK_BLE_INTERFACE_STATE).read()
#
#     def run(self):
#         scan_attempts = 0
#         conn_attempts = 0
#
#         while True:
#
#             if BLE.__check_ble_interface():
#
#                 configurations     = BaseStation()
#                 available, address = configurations.get_band_id()
#                 no_of_detected_devices = 0
#                 no_of_filtered_devices = 0
#
#                 while not available:
#
#                     scan_attempts += 1
#                     self.__logs.i('Scan'," Scanning for BabyIo Devices : {0}\n".format(scan_attempts))
#
#                     device_scan      = Scan(BLE.__DEFAULT_SCAN_PERIOD, self.__logs)
#                     detected_devices = device_scan.scan_devices()
#                     filtered_devices = device_scan.filter(detected_devices)
#                     device_scan.print_devices(filtered_devices)
#
#
#                     no_of_detected_devices += len(detected_devices)
#                     no_of_filtered_devices += len(filtered_devices)
#
#                     if len(filtered_devices) != 0:
#                         scan_attempts = 0
#                         available     = True
#                         address       = filtered_devices[0].addr
#
#                         configurations.set_band_id(address)
#                         self.__queue_audit.put(Connection(state=BLE_STATE.BAND_DETECTED,mac=address))
#
#                     elif scan_attempts%5 == 0:
#
#                         if no_of_detected_devices == 0:
#                             self.__queue_audit.put(Connection(state=BLE_STATE.NO_SCAN_RESULTS, mac=address))
#
#                         elif no_of_filtered_devices == 0:
#                             self.__queue_audit.put(Connection(state=BLE_STATE.NO_CHARGING_DEVICES))
#
#                         no_of_detected_devices = 0
#                         no_of_filtered_devices = 0
#
#                 conn_attempts = 0
#
#                 while available:
#                     monitor        = Monitor(address, self.__queue_decoder, self.__queue_command,self.__queue_audit,self.__logs)
#                     conn_attempts += 1
#
#                     if monitor.connect():
#                         conn_attempts = 0
#                         self.__queue_audit.put(Connection(state=BLE_STATE.BAND_CONNECTED,mac=address))
#                         monitor.run()
#
#                     elif conn_attempts % BLE.__DEFAULT_RECONNECT_ATTEMPTS == 0:
#                         self.__queue_audit.put(Connection(state=BLE_STATE.BAND_UNAVAILABLE, mac=address))
#
#                     available, address = configurations.get_band_id()
#
#             else:
#                 self.__logs.e('Interface','BLE Interface not detected')
#                 self.__logs.i('Interface', 'Attempt to restart BLE service')
#                 while not BLE.restart_ble_service():
#                     Util.sleep(BLE.__DEFAULT_INTERFACE_RECHECK)
