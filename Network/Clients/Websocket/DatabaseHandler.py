import sqlite3
from sqlite3 import Error
import time


class DatabaseHandler:
    def __init__(self, db_name_):
        self.__name = db_name_
        self.conn = None
        self.c = None
        self.__table_name = 'Data'

    def connect(self):
        try:
            self.conn = sqlite3.connect('Network/Database/' + self.__name)
            self.c = self.conn.cursor()
            self.create_table()
        except Error:
            print 'Database Creation Error'
        except:
            print 'Database Creation Error - Unknown'

    def create_table(self):
        try:
            sql_create_projects_table = "CREATE TABLE IF NOT EXISTS " + self.__table_name + " (time real NOT NULL, json_msg text NOT NULL);"

            # print sql_create_projects_table

            self.c.execute(sql_create_projects_table)

        except Error as e:
            print 'Database Table Creation Error', e

    def insert_record(self, array):
        try:
            values = '(' + str(array)[1:-1] + ')'
            sql_script = "INSERT INTO " + self.__table_name + " (time, json_msg) VALUES " + values + ';'

            # print sql_script

            self.c.execute(sql_script)
            self.conn.commit()
        except Error as e:
            print 'Database Insert Error', e

    def get_records(self, current_time, duration):
        try:
            sql_script = 'SELECT * FROM ' + self.__table_name + ' WHERE time BETWEEN ' + str(
                current_time - duration) + ' AND ' + str(current_time) + ';'

            # print sql_script

            self.c.execute(sql_script)
            return self.c.fetchall()
        except Error:
            print 'Database Get Records Error'

    def get_all_records(self):
        try:
            sql_script = 'SELECT * FROM ' + self.__table_name + ';'

            # print sql_script

            self.c.execute(sql_script)
            return self.c.fetchall()
        except Error:
            print 'Database Get Records Error'

    def delete_records(self, current_time, duration):
        try:
            sql_script = 'DELETE FROM ' + self.__table_name + ' WHERE time <' + str(current_time - duration) + ';'
            self.c.execute(sql_script)
            self.conn.commit()
        except Error:
            print 'Database Delete Records Error'

    def delete_all_records(self):
        try:
            sql_script = 'DELETE FROM ' + self.__table_name + ';'

            # print sql_script
            self.c.execute(sql_script)
            self.conn.commit()
        except Error:
            print 'Database Delete All Records Error'
