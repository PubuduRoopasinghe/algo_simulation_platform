import json
from Utils.util import Util
from Auditor.Messages.message import Message


class VitalUpdate(Message):

    def __init__(self, heart_rate=None, oxygen_level=None, temperature=None, wiggling=False):
        self.__detected_time = Util.get_epoch()
        self.__wiggling      = wiggling
        self.__heart_rate    = heart_rate
        self.__oxygen_level  = oxygen_level
        self.__temperature   = temperature

    def get(self, infant=''):
        return json.dumps({'detectedTime'     : self.__detected_time,
                           'heartRate'        : self.__heart_rate,
                           'oxygenSaturation' : self.__oxygen_level,
                           'bodyTemperature'  : self.__temperature,
                           'wiggling'         : self.__wiggling,
                           'infantId'         : infant})