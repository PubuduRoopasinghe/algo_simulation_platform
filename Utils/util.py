import re
import time
import datetime
from time import gmtime, strftime


class Util:

    def __init__(self):
        pass

    @staticmethod
    def get_epoch():
        return int(time.time()*1000)

    @staticmethod
    def get_time():
        return int(time.time())

    @staticmethod
    def exact_time():
        return time.time()

    @staticmethod
    def get_current_time():
        return int(time.time())

    @staticmethod
    def Current_time():
        return time.time()

    @staticmethod
    def time_elapsed(start):
        return int(time.time()) - start

    @staticmethod
    def get_timezone():
        print(strftime("%z", gmtime()))

    @staticmethod
    def to_fahrenheit(celsius):
        return round((1.8*celsius + 32)*100)/100.0

    @staticmethod
    def get_date_time(t=0):
        if t:
            return datetime.datetime.utcfromtimestamp(t)
        return datetime.datetime.now()

    @staticmethod
    def sleep(seconds):
        time.sleep(seconds)

    @staticmethod
    def get_age(dob):
        dob = datetime.datetime.utcfromtimestamp(dob)
        now = datetime.datetime.now()
        return (now - dob).days

    @staticmethod
    def validate_address(address):
        if re.match('([0-9A-Fa-f]{2}:){5}([0-9A-Fa-f]{2})', address):
            return True
        else:
            return False