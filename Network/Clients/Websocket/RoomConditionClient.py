from BaseClient import BaseClient
import time


class RoomConditionClient(BaseClient):
    def decode_msg(self, msg):
        data = msg.get(self.infant_ID)
        print 'roomConditionClient', data
        return time.time(), data

    def upload_old_data(self, data):
        for i in range(0, len(data)):
            json_msg = data[i][1]
            try:
                self.ws.write_message(json_msg)
            except Exception as e:
                print "NETWORK - Write failed - Websocket : " + self.url
                self.ws = None
                for j in range(i, len(data)):
                    self.db_handler.insert_record(data[j])
                return False
        print 'All old data was written'
        return True





