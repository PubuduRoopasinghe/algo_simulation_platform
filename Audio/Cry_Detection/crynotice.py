from Utils.util import Util

class CryNotice:

    def __init__(self,probability=0,signal=[],time=Util.get_epoch()):
        self.__probability = probability
        self.__time        = time
        self.__signal      = signal

    def get_time(self):
        return self.__time

    def get_probability(self):
        return self.__probability

    def get_signal(self):
        return self.__signal