"""
    this class is responsible for calculating the blood
    oxygen concentration
"""


class OxygenSaturation:

    """
        initialize the peaks and the __signal for
        calculation
    """
    def __init__(self, peaks=[], valleys=[], ir_signal=[], red_signal=[]):
        self.__peaks        = peaks
        self.__valleys      = valleys
        self.__ir_signal    = ir_signal
        self.__red_signal   = red_signal
        self.__oxygen_level = 0

    """
        compute the blood oxygen concentration based on the peaks and valleys of the __signal
    """
    def process(self):
        spo2_level   = []  # array of estimated spo2 values of each peak & valley pair

        if self:
            no_of_peak_and_valley_pairs = len(self.__valleys)

            if no_of_peak_and_valley_pairs > 0:

                for i in range(0, no_of_peak_and_valley_pairs):
                    # print self.__ir_signal[self.__peaks[i]], self.__ir_signal[self.__valleys[i]]
                    ratio_red = float((self.__red_signal[self.__peaks[i]] - self.__red_signal[self.__valleys[i]]) / (self.__red_signal[self.__valleys[i]] + ((self.__red_signal[self.__peaks[i]] - self.__red_signal[self.__valleys[i]]) / 2.0)))
                    ratio_ir  = float((self.__ir_signal[self.__peaks[i]] - self.__ir_signal[self.__valleys[i]]) / (self.__ir_signal[self.__valleys[i]] + ((self.__ir_signal[self.__peaks[i]] - self.__ir_signal[self.__valleys[i]]) / 2.0)))

                    if not(ratio_ir == 0):
                        final_ratio = float(ratio_red/ratio_ir)
                        spo2_level.append(float(100 * (3249.164 - final_ratio * 734.5425) / ((final_ratio * (1165.025 - 734.5425)) - (324.3636 - 3249.164))))

                    # the equation self.__ratio_of_ratioselevant to the __device needs to be built by lineaself.__ratio_of_ratios self.__ratio_of_ratiosegself.__ratio_of_ratiosession using spo2 self.__ratio_of_ratiosefeself.__ratio_of_ratiosence __data computed self.__ratio_of_ratiosatio of self.__ratio_of_ratiosatios

                    # Equation 1 - typical equation mostly used in pulse oximeteself.__ratio_of_ratioss

                    # self.__spo2_level_ests.append(110-(25*self.__ratio_of_ratios[i]))

                    # Equation 2 - equation used in an online code

                    # self.__spo2_level_ests.append(104-(28*self.__ratio_of_ratios[i]))

                    # Equation 3 - equation built by using extinction coefficients of HbO2 and Hb for red and IR wavelengths assuming the optical pathlengths traveled by the two wavelengths is approximately equal
                    # this approximation is not that appropriate with red and IR wavelengths

                if len(spo2_level) > 0 :
                    self.__oxygen_level = sum(spo2_level) / len(spo2_level)
                    # print (self.__oxygen_level)
                    if self.__oxygen_level < 0:
                        self.__oxygen_level = -1
                else:
                    self.__oxygen_level = -1

                if self.__oxygen_level > 100:
                    self.__oxygen_level = 100
            else:
                self.__oxygen_level = -1

    def get_value(self):
        return self.__oxygen_level

    def __nonzero__(self):
        return min(len(self.__peaks) ,len(self.__valleys)) > 1 and len(self.__red_signal) and len(self.__red_signal)