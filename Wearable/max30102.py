import struct
from enum import Enum


class MAX30102:

    __COMMAND_OPTICAL_CONFIGURATION = 0x12
    __WRITE_COMMAND = 0x80

    __ENCODE_WRITE_CONFIG_FORMAT = '<BbbbBBiiih'
    __ENCODE_READ_CONFIG_FORMAT  = '<Biiiihb'

    __DECODE_CONFIG_FORMAT = '<bbbBBiiib'

    __DEFAULT_IR_LED_CURRENT  = 0x24
    __DEFAULT_RED_LED_CURRENT = 0x33

    __instance = None

    class Range(Enum):
        PPG_RANGE_2048 = 0x00
        PPG_RANGE_4096 = 0x20
        PPG_RANGE_8192 = 0x40
        PPG_RANGE_16384 = 0x60

        @staticmethod
        def list():
            return [MAX30102.Range.PPG_RANGE_2048, MAX30102.Range.PPG_RANGE_4096, MAX30102.Range.PPG_RANGE_8192,
                    MAX30102.Range.PPG_RANGE_16384]

    class PWM(Enum):
        PPG_PWM_69us = 0x00
        PPG_PWM_118us = 0x01
        PPG_PWM_215us = 0x02
        PPG_PWM_411us = 0x03

        @staticmethod
        def list():
            return [MAX30102.PWM.PPG_PWM_69us, MAX30102.PWM.PPG_PWM_118us, MAX30102.PWM.PPG_PWM_215us,
                    MAX30102.PWM.PPG_PWM_411us]

    class Rate(Enum):
        PPG_RATE_50_Hz  = 0 << 2
        PPG_RATE_100_Hz = 1 << 2
        PPG_RATE_200_Hz = 2 << 2
        PPG_RATE_400_Hz = 3 << 2
        PPG_RATE_800_Hz = 4 << 2

        @staticmethod
        def list():
            return [MAX30102.Rate.PPG_RATE_50_Hz, MAX30102.Rate.PPG_RATE_100_Hz, MAX30102.Rate.PPG_RATE_200_Hz,
                    MAX30102.Rate.PPG_RATE_400_Hz, MAX30102.Rate.PPG_RATE_800_Hz]

    @staticmethod
    def getInstance():
        if MAX30102.__instance is None:
            MAX30102()
        return MAX30102.__instance

    def __init__(self):
        if MAX30102.__instance is not None:
            raise Exception("This class is a singleton!")
        else:
            MAX30102.__instance = self

        self.__range = MAX30102.Range.PPG_RANGE_2048
        self.__rate  = MAX30102.Rate.PPG_RATE_50_Hz
        self.__pwm   = MAX30102.PWM.PPG_PWM_411us
        self.__red_current = self.__DEFAULT_RED_LED_CURRENT
        self.__ir_current  = self.__DEFAULT_IR_LED_CURRENT

    @staticmethod
    def get_valid_sampling_values():
        return MAX30102.Rate.list()

    @staticmethod
    def get_valid_pulse_widths():
        return MAX30102.PWM.list()

    @staticmethod
    def get_valid_ppg_ranges():
        return MAX30102.Range.list()

    def set_parameters(self, ppg_rate, ppg_pwm, ppg_range, ir_current, red_current):
        self.__rate = ppg_rate
        self.__pwm  = ppg_pwm
        self.__range = ppg_range
        self.__ir_current = ir_current
        self.__red_current = red_current

    def get_ppg_rate(self):
        return self.__rate

    def set_ppg_rate(self, rate):
        self.__rate = rate

    def get_ppg_range(self):
        return self.__range

    def set_ppg_range(self, range):
        self.__range = range

    def get_ppg_pwm(self):
        return self.__pwm

    def set_ppg_pwm(self, pwm):
        self.__pwm = pwm

    def get_ir_current(self):
        return self.__ir_current

    def set_ir_current(self, current):
        self.__ir_current = current

    def get_red_current(self):
        return self.__red_current

    def set_red_current(self, current):
        self.__red_current = current

    def decode_configurations(self, configurations):
        data = struct.unpack(self.__DECODE_CONFIG_FORMAT, configurations)
        self.__rate = data[0]
        self.__pwm  = data[1]
        self.__range= data[2]
        self.__red_current = data[3]
        self.__ir_current  = data[4]
        print (self)

    @staticmethod
    def __convert_to_ma(current):
        return int(round(current * 50/255))

    def set_default_values(self):
        self.__range = MAX30102.Range.PPG_RANGE_2048
        self.__rate  = MAX30102.Rate.PPG_RATE_50_Hz
        self.__pwm   = MAX30102.PWM.PPG_PWM_411us
        self.__red_current = self.__DEFAULT_RED_LED_CURRENT
        self.__ir_current = self.__DEFAULT_IR_LED_CURRENT

    def __get_json(self):
        data = {
            'PPG Rate'   : self.__rate,
            'PPG PWM'    : self.__pwm,
            'PPG Range'  : self.__range,
            'Red Current': self.__convert_to_ma(self.__red_current),
            'IR Current' : self.__convert_to_ma(self.__ir_current),
        }
        return data

    def __str__(self):
        return str(self.__get_json())

    def encode_write_configuration(self):
        return struct.pack(self.__ENCODE_WRITE_CONFIG_FORMAT,
                                self.__WRITE_COMMAND | self.__COMMAND_OPTICAL_CONFIGURATION,
                                self.__rate, self.__pwm, self.__range, self.__red_current, self.__ir_current,
                                0x00, 0x00, 0x00, 0x00)

    def encode_read_configuration(self):
        structure = struct.pack(self.__ENCODE_READ_CONFIG_FORMAT, self.__COMMAND_OPTICAL_CONFIGURATION, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00)
        return structure
