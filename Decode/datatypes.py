
"""
    return 32 bit signed integer using first 4 bytes in the input array and remove 4 bytes from array
    :array : input byte array
"""
def get_signed_int(array):
    output = array[0] | (array[1] << 8) | (array[2] << 16) | (array[3] << 24)
    array[0:4] = []
    return twos_comp(output, 32)


"""
    return 32 bit unsigned integer using first 4 bytes in the input array and remove 4 bytes from array
    :array : input byte array
"""
def get_unsigned_int_32(array):
    output = array[0] | (array[1] << 8) | (array[2] << 16) | (array[3] << 24)
    array[0:4] = []
    return output


"""
    return 16 bit signed integer using first 2 bytes in the input array and remove 2 bytes from array
    :array : input byte array
"""


def get_signed_short(array):
    output = array[0] | (array[1] << 8)
    array[0:2] = []
    return twos_comp(output, 16)


"""
    return 16 bit unsigned integer using first 2 bytes in the input array and remove 2 bytes from array
    :array : input byte array
"""
def get_unsigned_short(array):
    output = array[0] | (array[1] << 8)
    array[0:2] = []
    return output


"""
    return 8 bit signed integer using first byte in the input array and remove first byte from array
    :array : input byte array
"""
def get_signed_byte(array):
    output = array[0]
    array[0:1] = []
    return twos_comp(output, 8)

"""
    return 8 bit unsigned integer using first byte in the input array and remove first byte from array
    :array : input byte array
"""
def get_unsigned_byte(array):
    output = array[0]
    array[0:1] = []
    return output


"""
    this calculates the two's compliment.
    the value and the number of bits the number should consist should be given as inputs
    returns the two's complement value
"""
def twos_comp(val, bits):
    if(val & (1 << (bits - 1))) != 0:
        val = val - (1 << bits)
    return val

