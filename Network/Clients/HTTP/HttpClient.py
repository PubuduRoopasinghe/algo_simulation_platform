from tornado import httpclient, httputil
import configparser
from DatabaseHandler import DatabaseHandler
import time
from Configurations.basestation import BaseStation

class HttpClient:
    def __init__(self, url_, database_name_=None):
        self.url = url_

        if database_name_ is not None:
            self.db_handler = DatabaseHandler(db_name_=database_name_)
            self.db_handler.connect()
        else:
            self.db_handler = None
        self.__database_empty = True

        self.infant_ID = self.get_infant_id()
        # print self.infant_ID
        self.token = ''

        self.get_token()
        self.http_client = httpclient.HTTPClient()

    def update_data(self, data):
        cur_time, json_msg = self.decode_msg(data)

        if cur_time is None:
            return

        if not self.post(json_msg):
            self.db_handler.insert_record([cur_time, json_msg])
            self.__database_empty = False
            return

        if not self.__database_empty:
            db_records = self.db_handler.get_all_records()
            self.db_handler.delete_all_records()

            if not self.upload_old_data(db_records):
                return

            # self.db_handler.delete_all_records()
            self.__database_empty = True

    def post(self, data):
        print self.url, data
        res = True
        try:
            httpRequest = httpclient.HTTPRequest(url=self.url, method='POST',
                                                 headers={'infantID': str(self.infant_ID), 'deviceID': str(0x00),
                                                          "content-type": "application/json"},
                                                 body=data)

            # print httpRequest
            response = self.http_client.fetch(request=httpRequest)
            # print response
        except httpclient.HTTPError as e:
            # HTTPError is raised for non-200 responses; the response
            # can be found in e.response.
            print("HttpClient Error" + str(e))
            res = False

        except Exception as e:
            # Other errors are possible, such as IOError.
            print("HttpClient Error" + str(e))
            res = False
        # self.http_client.close()
        return res

    def decode_msg(self, msg):
        # should return the parameters as an (array, json_str)
        # array = [time, parameters ......]

        return ''

    def upload_old_data(self, data):
        # should be replaced with the method being used to upload the data
        # can be either the websocket or the post request
        # need to return True on sucessfully uploading
        # else need to send the data back to the database using self.db_handler.insert_record()

        return True

    @staticmethod
    def get_infant_id():
        # config = configparser.ConfigParser()
        # config.read('Network/Config/config.ini')
        #
        # if config.has_section('Infant Data'):
        #     return config.get('Infant Data', 'ID')
        #
        # else:
        #     config.add_section('Infant Data')
        #     config.set('Infant Data', 'ID', str(0))
        #
        #     with open('Network/Config/config.ini', 'w+') as configFile:
        #         config.write(configFile)
        #
        #     return 0
        return BaseStation.get_infant_id()

        # return 0
    def get_token(self):
        expired = False

        if expired:
            # get new token some how
            return self.token
        else:
            return self.token
