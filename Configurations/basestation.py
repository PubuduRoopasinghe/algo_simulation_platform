from Utils.util import Util
from ConfigParser import SafeConfigParser


class BaseStation:

    def __init__(self):
        pass

    __config = SafeConfigParser()

    @staticmethod
    def __read():
        BaseStation.__config.read('config.ini')

    @staticmethod
    def __save_configurations():
        with open('config.ini', 'w') as config_file:
            BaseStation.__config.write(config_file)

    @staticmethod
    def set_band_id(address):
        if Util.validate_address(address) or address is '':
            BaseStation.__read()
            if not BaseStation.__config.has_section('BLE'):
                BaseStation.__config.add_section('BLE')
            BaseStation.__config.set('BLE', 'MAC', address)
            BaseStation.__save_configurations()

    @staticmethod
    def get_band_id():
        BaseStation.__read()
        if BaseStation.__config.has_option('BLE', 'MAC'):
            address = BaseStation.__config.get('BLE', 'MAC')
            if Util.validate_address(address):
                return True, address
            else:
                return False, ''
        else:
            return False, ''

    @staticmethod
    def set_birthday(dob):
        BaseStation.__read()
        if not BaseStation.__config.has_section('Infant'):
            BaseStation.__config.add_section('Infant')
        BaseStation.__config.set('Infant', 'DoB', str(dob))
        BaseStation.__save_configurations()

    @staticmethod
    def get_birthday():
        BaseStation.__read()
        if BaseStation.__config.has_option('Infant', 'DoB'):
            return int(BaseStation.__config.get('Infant', 'DoB'))
        else:
            BaseStation.set_birthday(Util.get_time())
            return Util.get_time()

    @staticmethod
    def set_infant_id(infant_id):
        BaseStation.__read()
        if not BaseStation.__config.has_section('Infant'):
            BaseStation.__config.add_section('Infant')
        BaseStation.__config.set('Infant', 'ID', infant_id)
        BaseStation.__save_configurations()

    @staticmethod
    def get_infant_id():
        BaseStation.__read()
        if BaseStation.__config.has_option('Infant', 'ID'):
            return BaseStation.__config.get('Infant', 'ID')
        else:
            return ''

    #
    # @staticmethod
    # def replace_band():
    #     BaseStation.__read()
    #     if BaseStation.__config.has_option('BLE', 'MAC'):
    #         BaseStation.__config.remove_option('BLE', 'MAC')
    #         BaseStation.__save_configurations()