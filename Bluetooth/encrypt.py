# import struct
# from Crypto.Cipher import AES
#
#
# class Encrypt:
#
#     __KEY_FORMAT = '<IIII'
#
#     def __init__(self):
#         pass
#
#     @staticmethod
#     def __get_key(key):
#         temp_key = struct.pack(Encrypt.__KEY_FORMAT, key[0], key[1], key[2], key[3])
#         key_0 = bytearray()
#         key_0.extend(temp_key)
#         return key_0
#
#     @staticmethod
#     def __convert_to_string(data=[]):
#         return ''.join(map(chr, data))
#
#
#     @staticmethod
#     def encrypt(key=[], data=[]):
#         if len(key) == 4:
#             key  = Encrypt.__convert_to_string(Encrypt.__get_key(key))
#             cipher = AES.new(key, AES.MODE_ECB)
#             data = Encrypt.__convert_to_string(data)
#             msg  = cipher.encrypt(data)
#             output = bytearray()
#             output.extend(msg)
#             return output
#         else:
#             return []
#
#     @staticmethod
#     def decrypt(key=[], data=[]):
#         if len(key) == 4:
#             key = Encrypt.__convert_to_string(Encrypt.__get_key(key))
#             cipher = AES.new(key, AES.MODE_ECB)
#             data = Encrypt.__convert_to_string(data)
#             msg = cipher.decrypt(data)
#             output = bytearray()
#             output.extend(msg)
#             return output
#         else:
#             return []