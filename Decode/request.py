import struct

class Request:

    def __init__(self, motion_index, motion_period, optical_index, optical_period):
        self.__motion_index = motion_index
        self.__motion_period = motion_period
        self.__optical_index = optical_index
        self.__optical_period = optical_period

    def get_request(self):
        return struct.pack('<IHIH', self.__motion_index, self.__motion_period, self.__optical_index, self.__optical_period)

    def __str__(self):
        return '[{0},{1},{2},{3}]'.format(self.__motion_index, self.__motion_period, self.__optical_index, self.__optical_period)
