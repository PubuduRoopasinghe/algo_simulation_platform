import bme680
from Utils.util import Util
from Utils.logs import Logs
from multiprocessing import Process
from Notification.Message.error import Error
from Sensors.AirQuality.roomcondition import RoomCondition


class AirQuality(Process):

    __I2C_REATTEMPT_PERIOD = 15
    __BURN_IN_TIME         = 60

    def __init__(self, queue_audit, period):
        super(AirQuality, self).__init__()
        self.__queue_audit = queue_audit
        self.__sensor      = None
        self.__period      = period
        self.__log         = Logs('AirQuality')
        self.__notified    = False

    def __sensor_init(self):

        try:
            self.__sensor = bme680.BME680(bme680.I2C_ADDR_PRIMARY)
        except IOError:
            try:
                self.__sensor = bme680.BME680(bme680.I2C_ADDR_SECONDARY)
            except IOError:
                if not self.__notified:
                    self.__queue_audit.put(Error('AirQuality','Sensors not found(I2C Error)'))
                    self.__notified = True
                self.__sensor = None
                return False

        self.__sensor.set_humidity_oversample(bme680.OS_2X)
        self.__sensor.set_pressure_oversample(bme680.OS_4X)
        self.__sensor.set_temperature_oversample(bme680.OS_8X)
        self.__sensor.set_filter(bme680.FILTER_SIZE_3)
        self.__sensor.set_gas_status(bme680.ENABLE_GAS_MEAS)

        self.__sensor.set_gas_heater_temperature(320)
        self.__sensor.set_gas_heater_duration(150)
        self.__sensor.select_gas_heater_profile(0)
        return True

    def __send_to_auditor(self,room_condition):
        self.__queue_audit.put(room_condition)

    def run(self):
        burn_in_data = []

        while self.__sensor is None:
            if self.__sensor_init():
                break
            else:
                Util.sleep(AirQuality.__I2C_REATTEMPT_PERIOD)

        # Collect gas resistance burn-in values, then use the average
        # of the last 50 values to set the upper limit for calculating
        # gas_baseline.
        start_time   = Util.get_time()
        self.__log.i('BME680','Collecting gas resistance burn-in data for 1 minutes')

        while Util.time_elapsed(start_time) < AirQuality.__BURN_IN_TIME:
            if self.__sensor.get_sensor_data() and self.__sensor.data.heat_stable:
                gas = self.__sensor.data.gas_resistance
                burn_in_data.append(gas)
                Util.sleep(1.0)

        gas_baseline = sum(burn_in_data[-10:]) / 10.0

        # Set the humidity baseline to 40%, an optimal indoor humidity.
        humidity_baseline = 40.0

        # This sets the balance between humidity and gas reading in the calculation of air_quality_score (25:75, humidity:gas)
        humidity_weighting = 0.25

        # print('Gas baseline: {0} Ohms, humidity baseline: {1:.2f} %RH\n'.format(gas_baseline,hum_baseline))

        while True:
            if self.__sensor.get_sensor_data() and self.__sensor.data.heat_stable:
                gas = self.__sensor.data.gas_resistance
                gas_offset = gas_baseline - gas

                humidity = self.__sensor.data.humidity
                hum_offset = humidity - humidity_baseline

                # Calculate hum_score as the distance from the hum_baseline.
                if hum_offset > 0:
                    humidity_score  = (100 - humidity_baseline - hum_offset)
                    humidity_score /= (100 - humidity_baseline)
                    humidity_score *= (humidity_weighting * 100)

                else:
                    humidity_score  = (humidity_baseline + hum_offset)
                    humidity_score /= humidity_baseline
                    humidity_score *= (humidity_weighting * 100)

                # Calculate gas_score as the distance from the gas_baseline.
                if gas_offset > 0:
                    gas_score = (gas / gas_baseline)
                    gas_score*= (100 - (humidity_weighting * 100))
                else:
                    gas_score = 100 - (humidity_weighting * 100)

                # Calculate air_quality_score.
                air_quality_score = humidity_score + gas_score

                room_condition = RoomCondition(temperature= self.__sensor.data.temperature,
                                               pressure=self.__sensor.data.pressure,
                                               humidity=self.__sensor.data.humidity,
                                               air_quality=air_quality_score)

                self.__send_to_auditor(room_condition)
                Util.sleep(self.__period)


