# from bluepy.btle import Scanner, DefaultDelegate
#
#
# class ScanDelegate(DefaultDelegate):
#
#     def __init__(self):
#         DefaultDelegate.__init__(self)
#
#     def handle_discovery(self, dev, is_new, is_new_data):
#         pass
#
#
# class Scan:
#
#     __DEVICE_NAME_TOKEN = 9
#     __DEVICE_NAME       = 'babyio'
#     __MANUFACTURE_TOKEN = 255
#     __SENSOR_STATE      = 31
#     __CHARGING_STATE    = 3
#     __INDIVIDUAL_SENSOR_STATE = 1
#     __SENSORS = ['Battery monitor ', 'Temperature Sensor', 'Optical Sensor', 'Motion Sensor', 'Flash Memory']
#
#     def __init__(self,period,log):
#         self.__period = period
#         self.__devices = None
#         self.__scanner = None
#         self.__log     = log
#
#     def scan_devices(self):
#         self.__scanner = Scanner().withDelegate(ScanDelegate())
#         self.__devices = self.__scanner.scan(self.__period)
#         return self.__devices
#
#     def filter(self,devices):
#         filtered_list = []
#         index = 1
#         for device in devices:
#             included = False
#             if device.getValueText(Scan.__DEVICE_NAME_TOKEN) == Scan.__DEVICE_NAME:
#                 manufacturer_data = device.getValueText(Scan.__MANUFACTURE_TOKEN)
#                 charge_state = not(int(manufacturer_data[1], 16) == 0)
#                 sensor_state = int(manufacturer_data[4:6], 16)
#
#                 self.__log.v('\t [{0}] {1}:\n\t\t--> sensor __states   : {2}\n\t\t--> charging __states : {3}\n'.format(
#                                                                                                             index,
#                                                                                                             str(device.addr),
#                                                                                                             sensor_state,
#                                                                                                             charge_state))
#                 index += 1
#                 if charge_state and self.__verify_sensor(sensor_state):
#                     for selected in filtered_list:
#                         if str(device.addr) == str(selected.addr):
#                             included = True
#                             break
#                     if not included:
#                         filtered_list.append(device)
#         filtered_list.sort(key=lambda x:x.rssi)
#         return filtered_list
#
#     def __verify_sensor(self,sensor_state):
#         if sensor_state != Scan.__SENSOR_STATE:
#             self.__log.v('\t\t-->following sensor are malfunctioning')
#             for i in range(len(Scan.__SENSORS)):
#                 state = (1 << i)
#                 if state & sensor_state == 0:
#                     self.__log.v('\t\t\t--> {0}'.format(Scan.__SENSORS[i]))
#         return sensor_state == Scan.__SENSOR_STATE
#
#     def print_devices(self,devices):
#         for device in devices:
#             self.__log.v('----------------------------------------------------------------------------------------------')
#             self.__log.v(' Device       : {0} ({1}dB)'.format(str(device.addr), device.rssi))
#             self.__log.v(' RSSI         : {0} dB'.format(device.rssi))
#             self.__log.v(' Scan Response: {0}'.format(device.getScanData()))
#             self.__log.v('----------------------------------------------------------------------------------------------')
