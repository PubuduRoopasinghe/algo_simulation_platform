from Decode.datatypes import *

class BLE_Conf:

    def __init__(self,data):
        self.tx_power = get_unsigned_byte(data)
        self.adv_period= get_unsigned_short(data)

